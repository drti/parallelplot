
# parallelPlot

![parallelPlot snapshot](./doc/snapshots/PCP.png)

## Overview

`parallelPlot` is an R package providing an interactive parallel coordinate chart, written in R and javascript, based on [htmlwidgets](https://github.com/ramnathv/htmlwidgets) and [d3.js](https://d3js.org/).

In a Shiny app, it can be updated programmatically and some actions can trigger reactions - see example [test/testingApp/Shiny-ScatterPlotMatrix-ParallelPlot-CsvFile.R](https://ifpen-gitlab.appcollaboratif.fr/detocs/scatterplotmatrix/-/blob/master/test/testingApp/Shiny-ScatterPlotMatrix-ParallelPlot-CsvFile.R) (deployed in [shinyapps.io](https://detocs.shinyapps.io/ScatterPlotMatrix-ParallelPlot-Link-DataFromFile/)).

For example, it can interact with a [scatterPlotMatrix](https://cran.r-project.org/package=scatterPlotMatrix).

![link scatterPlotMatrix-parallelPlot snapshot](./doc/snapshots/linkPCP_SPM.gif)

## Context

This `parallelPlot` R package was initially developped as part of the [Lagun](https://gitlab.com/drti/lagun) project conducted by Safran Tech and IFPEN, to augment the existing parallel coordinate chart packages with the required functionalities (categorical variables, histogram distributions, manual cutoff definitions, ... more information [here](./doc/features.md)). The limit use case for Lagun was 1200 columns and 5000 rows, with the possibility of up to 10000 rows.

## Install

- Requirements: R (>= 3.5.1) available from [CRAN](https://cran.r-project.org/), git
- Clone the current repository: `git clone https://gitlab.com/drti/parallelPlot.git`
- Install parallelPlot: `R -e "install.packages('parallelPlot/htmlwidget/', repos = NULL, type = 'source')"`

To generate `htmlwidget` files from source files:

``` sh
cd parallelplot
npm install
npm run build
```

See [doc/devGuide.md](./doc/devGuide.md) for more information.

## List of contributors

- David Chazalviel (TECH'advantage, funded by Safran Tech)
- Benoit Lehman (TECH'advantage, funded by Safran Tech)
- Sébastien Da Veiga (Safran Tech)