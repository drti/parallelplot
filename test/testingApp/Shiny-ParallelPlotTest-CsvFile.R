library(shiny)
library(shinyjs)
library(shinyWidgets)
library(parallelPlot)

# load 'htmlwidgets' (useful for 'Save As Html" action)
library(htmlwidgets)

###############################
#  Define client user interface
###############################

ui <- fluidPage(
  useShinyjs(),  # Set up shinyjs
  # Populate with some widgets
  titlePanel("ParallelPlot - Data from file"),
  wellPanel(
    fluidRow(
      column(
        3,
        fileInput("csvFile", "Choose CSV File (with header & separator = ',' & decimal = '.')", accept = c(
          "text/csv",
          "text/comma-separated-values,text/plain",
          ".csv")
        )
      ),
      column(
        3,
        pickerInput(inputId = "categorical", label = "Categorical", choices = c(), multiple = TRUE)
      ),
      column(
        2,
        style = "margin-top: 25px;", 
        actionButton("plotButton", "Plot CSV data")
      ),
      column(
        2,
        style = "margin-top: 25px;", 
        actionButton("downloadButton", "Download Widget")
      ),
      column(
        2,
        downloadButton("associatedDownloadButton", "Download Widget", style = "visibility: hidden;")
      )
    )
  ),
  parallelPlotOutput("parallelPlot")
)

#####################
# Define server logic
#####################

server <- function(input, output, session) {

  csvData <- eventReactive(input$csvFile, {
      read.csv2(input$csvFile$datapath, header = TRUE, sep = ",")
  })

  dataForDownload <- NULL
  ppForDownload <- NULL

  observeEvent(csvData(), {
    req(csvData())
    choices <- colnames(csvData())
    selected <- names(Filter(function(col) length(levels(as.factor(col))) < 10, csvData()))
    updatePickerInput(session = session, inputId = "categorical", choices = choices, selected = selected)
  })

  # If 'csvFile' has been chosen ...
  output$parallelPlot <- renderParallelPlot({
    input$plotButton
    isolate({
      data <- as.data.frame(lapply(names(csvData()), function(colName) {
      	if (colName %in% input$categorical) {
      	  return(as.factor(csvData()[[colName]])) 
      	}
      	else {
      	  return(csvData()[[colName]])
      	}
      }))
      colnames(data) <- names(csvData())
      dataForDownload <<- data

      parallelPlot(
        data,
        controlWidgets = TRUE,
        width = "100%"
      )
    })
  })

  # If 'downloadButton' has been clicked, send a 'getPlotConfig' message
  observeEvent(input$downloadButton, {
    print("observeEvent input$downloadButton")
    parallelPlot::getPlotConfig("parallelPlot", "ConfigForDownload")
  })
  # When the 'ConfigForDownload' reactive input is changed,
  # create a parallelPlot and save it by emulating a click on 'associatedDownloadButton'
  observeEvent(input$ConfigForDownload, {
    print("observeEvent input$ConfigForDownload")
    ppForDownload <<- parallelPlot(
      data = dataForDownload,
      categorical = input$ConfigForDownload$categorical,
      categoriesRep = input$ConfigForDownload$categoriesRep,
      arrangeMethod = input$ConfigForDownload$arrangeMethod,
      inputColumns = input$ConfigForDownload$inputColumns,
      keptColumns = input$ConfigForDownload$keptColumns,
      histoVisibility = input$ConfigForDownload$histoVisibility,
      invertedAxes = input$ConfigForDownload$invertedAxes,
      cutoffs = input$ConfigForDownload$cutoffs,
      refRowIndex = input$ConfigForDownload$refRowIndex,
      refColumnDim = input$ConfigForDownload$refColumnDim,
      rotateTitle = input$ConfigForDownload$rotateTitle,
      columnLabels = input$ConfigForDownload$columnLabels,
      continuousCS = input$ConfigForDownload$continuousCS,
      categoricalCS = input$ConfigForDownload$categoricalCS,
      controlWidgets = NULL,
      cssRules = input$ConfigForDownload$cssRules,
      sliderPosition = input$ConfigForDownload$sliderPosition
    )
    shinyjs::runjs("document.getElementById('associatedDownloadButton').click();")
  })

  # if a click on 'associatedDownloadButton' occurred, save 'ppForDownload' widget (previously created)
  output$associatedDownloadButton <- downloadHandler(
    filename = function() {
      paste("parallelPlot-", Sys.Date(), ".html", sep = "")
    },
    content = function(tmpContentFile) {
      print("downloadHandler-saveWidget")
      htmlwidgets::saveWidget(ppForDownload, tmpContentFile)
    }
  )

}

##########################
# Create Shiny app objects
##########################
shinyApp(ui, server)
