###############################
#  Define client user interface
###############################

testingModule.ui <- function(id) { # nolint
  ns <- shiny::NS(id)

  tagList(
    # Populate with some widgets
    br(),
    bsCollapse(
      bsCollapsePanel(
        "Arguments",
        style = "primary",
        fluidRow(
          column(
            2,
            tags$div(
              em("'categorical' argument"),
              selectInput(
                ns("categoricalColumnsSelect"),
                "Categorical Columns:",
                choices = list("1/3" = 3, "1/4" = 4),
                selected = "1/3"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'categoriesRep' argument"),
              selectInput(
                ns("categoriesRepSelect"),
                "Categories Representation:",
                choices = list("EquallySpacedLines", "EquallySizedBoxes"),
                selected = "EquallySpacedLines"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'arrangeMethod' argument"),
              selectInput(
                ns("arrangeMethodSelect"),
                "Arrange Method in Category Boxes:",
                choices = list("fromLeft", "fromRight", "fromBoth", "fromNone"),
                selected = "fromRight"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'refColumnDim' argument"),
              selectInput(
                ns("refColSelect"),
                "Reference column:",
                choices = list("None" = 1, "First" = 2, "Second" = 3),
                selected = "None"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'refRowIndex' argument"),
              selectInput(
                ns("refRowSelect"),
                "Reference row:",
                choices = list("None" = 1, "First" = 2, "Last" = 3),
                selected = "None"
              )
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'invertedAxes' argument"),
              selectInput(
                ns("invAxesSelect"),
                "Inverted Axes:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3, "None" = 4),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'histoVisibility' argument"),
              selectInput(
                ns("histSelect"),
                "Histo Visibility:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3, "None" = 4),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'continuousCS' argument"),
              selectInput(
                ns("continuousCsSelect"),
                "Continuous Color Scale:",
                choices = list(
                  "Viridis" = "Viridis", "Inferno" = "Inferno", "Magma" = "Magma", "Plasma" = "Plasma", "Warm" = "Warm",
                  "Cool" = "Cool", "Rainbow" = "Rainbow", "CubehelixDefault" = "CubehelixDefault", "Blues" = "Blues",
                  "Greens" = "Greens", "Greys" = "Greys", "Oranges" = "Oranges", "Purples" = "Purples", "Reds" = "Reds",
                  "BuGn" = "BuGn", "BuPu" = "BuPu", "GnBu" = "GnBu", "OrRd" = "OrRd", "PuBuGn" = "PuBuGn",
                  "PuBu" = "PuBu", "PuRd" = "PuRd", "RdBu" = "RdBu", "RdPu" = "RdPu", "YlGnBu" = "YlGnBu",
                  "YlGn" = "YlGn", "YlOrBr" = "YlOrBr", "YlOrRd" = "YlOrRd"
                ),
                selected = "Viridis"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'categoricalCS' argument"),
              selectInput(
                ns("categoricalCsSelect"),
                "Categorical Color Scale:",
                choices = list(
                  "Category10" = "Category10", "Accent" = "Accent", "Dark2" = "Dark2",
                  "Paired" = "Paired", "Set1" = "Set1"
                ),
                selected = "Category10"
              )
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'cutoffs' argument"),
              sliderInput(ns("brushSlider"), "Brush for second column:", min = 0, max = 100, value = c(25, 75))
            )
          ),
          column(
            2,
            tags$div(
              em("'sliderPosition' argument"),
              checkboxInput(ns("sliderPositionCB"), "Use Custom Slider Position", FALSE)
            )
          ),
          column(
            2,
            tags$div(
              em("'keptColumns' argument"),
              selectInput(
                ns("colSelect"),
                "Available Columns:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'inputColumns' argument"),
              selectInput(
                ns("inputColumnsSelect"),
                "Input Columns:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3),
                selected = "All"
              )
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'cssRules' arguments"),
              checkboxInput(ns("customStyleCB"), "Use Custom Style", FALSE)
            )
          ),
          column(
            2,
            tags$div(
              em("'rotateTitle' argument"),
              checkboxInput(ns("rotateTitleCB"), "Rotate Titles", FALSE)
            )
          ),
          column(
            2,
            tags$div(
              em("'columnLabels' argument"),
              checkboxInput(ns("columnLabelsCB"), "Give labels to use in place of column names", FALSE)
            )
          ),
          column(
            2,
            tags$div(
              em("'editionMode' argument"),
              selectInput(
                ns("editionModeSelect"),
                "Edition mode:",
                choices = list("Off" = "EditionOff", "On Drag" = "EditionOnDrag", "On Drag End" = "EditionOnDragEnd"),
                selected = "EditionOnDragEnd"
              )
            )
          )
        )
      )
    ),
    fluidRow(
      column(
        2,
        numericInput(ns("sampleSize"), "Sample Size:", "1000")
      ),
      column(
        2,
        numericInput(ns("columnCount"), "Column Count:", "12")
      ),
      column(
        2,
        actionButton(ns("buildAction"), "Generate Sampling & Build Parallel Plot", style = "margin-top: 25px;")
      )
    ),
    bsCollapse(
      bsCollapsePanel(
        "Functions",
        style = "primary",
        fluidRow(
          column(
            2,
            tags$div(
              em("'setArrangeMethod'"),
              selectInput(
                ns("arrangeMethodSelect2"),
                "Arrange Method in Category Boxes:",
                choices = list("fromLeft", "fromRight", "fromBoth", "fromNone"),
                selected = "fromRight"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setCategoriesRep'"),
              selectInput(
                ns("categoriesRepSelect2"),
                "Categories Representation:",
                choices = list("EquallySpacedLines", "EquallySizedBoxes"),
                selected = "EquallySpacedLines"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setRefColumnDim'"),
              selectInput(
                ns("refColSelect2"),
                "Reference column:",
                choices = list("None" = 1, "First" = 2, "Second" = 3),
                selected = "None"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setContinuousColorScale'"),
              selectInput(
                ns("continuousCsSelect2"),
                "Continuous Color Scale:",
                choices = list(
                  "Viridis" = "Viridis", "Inferno" = "Inferno", "Magma" = "Magma", "Plasma" = "Plasma", "Warm" = "Warm",
                  "Cool" = "Cool", "Rainbow" = "Rainbow", "CubehelixDefault" = "CubehelixDefault", "Blues" = "Blues",
                  "Greens" = "Greens", "Greys" = "Greys", "Oranges" = "Oranges", "Purples" = "Purples", "Reds" = "Reds",
                  "BuGn" = "BuGn", "BuPu" = "BuPu", "GnBu" = "GnBu", "OrRd" = "OrRd", "PuBuGn" = "PuBuGn",
                  "PuBu" = "PuBu", "PuRd" = "PuRd", "RdBu" = "RdBu", "RdPu" = "RdPu", "YlGnBu" = "YlGnBu",
                  "YlGn" = "YlGn", "YlOrBr" = "YlOrBr", "YlOrRd" = "YlOrRd"
                ),
                selected = "Viridis"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setCategoricalColorScale'"),
              selectInput(
                ns("categoricalCsSelect2"),
                "Categorical Color Scale:",
                choices = list(
                  "Category10" = "Category10", "Accent" = "Accent", "Dark2" = "Dark2",
                  "Paired" = "Paired", "Set1" = "Set1"
                ),
                selected = "Category10"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'getPlotConfig/saveWidget'"),
              actionButton(ns("downloadButton"), "Save As Html"),
              downloadButton(ns("associatedDownloadButton"), "Download Widget", style = "visibility: hidden;")
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'setKeptColumns'"),
              selectInput(
                ns("colSelect2"),
                "Available Columns:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3, "Add First Four" = 4),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setHistoVisibility'"),
              selectInput(
                ns("histSelect2"),
                "Histo Visibility:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3, "None" = 4, "Add First Four" = 5),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setInvertedAxes'"),
              selectInput(
                ns("invAxesSelect2"),
                "Inverted Axes:",
                choices = list("All" = 1, "Odd" = 2, "Even" = 3, "None" = 4, "Add First Four" = 5),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'highlightRow'"),
              actionButton(ns("highlightRowAction"), "Highlight Last Row")
            )
          ),
          column(
            2,
            tags$div(
              em("'highlightRow'"),
              actionButton(ns("clearHlRowAction"), "Remove Highlighting")
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'setCutoffs'"),
              sliderInput(ns("brushSlider2"), "Brush for second column:", min = 0, max = 100, value = c(25, 75))
            )
          ),
          column(
            2,
            tags$div(
              em("'setCutoffs'"),
              selectInput(
                ns("cutoffSelect"),
                "Cutoff for first column:",
                choices = list("All" = "All", "None" = "None"),
                selected = "All"
              )
            )
          ),
          column(
            2,
            tags$div(
              em("'setCutoffs'"),
              actionButton(ns("clearCutoffsAction"), "Remove All Cutoffs")
            )
          )
        ),
        fluidRow(
          column(
            2,
            tags$div(
              em("'getValue(\"Cutoffs\")'"),
              actionButton(ns("getCutoffsAction"), "Retrieve Cutoffs")
            )
          ),
          column(
            2,
            tags$div(
              em("'getValue(\"SelectedTraces\")'"),
              actionButton(ns("getSelectedTracesAction"), "Retrieve Selected Lines")
            )
          ),
          column(
            2,
            tags$div(
              em("'getValue(\"ReferenceColumn\")'"),
              actionButton(ns("getRefColumnAction"), "Retrieve reference column")
            )
          )
        )
      )
    ),
    textOutput(ns("updatedCutoff")),
    textOutput(ns("selectedTraces")),
    br(),
    parallelPlotOutput(ns("parPlot"))
  )
}

#####################
# Define server logic
#####################

testingModule.server <- function(input, output, session) { # nolint
  ns <- session$ns

  allItem <- function(names) {
    sapply(seq_len(length(names)), function(i) TRUE)
  }
  oddItem <- function(names) {
    sapply(seq_len(length(names)), function(i) (i %% 2) == 1)
  }
  evenItem <- function(names) {
    sapply(seq_len(length(names)), function(i) i %% 2 == 0)
  }
  noneItem <- function(names) {
    sapply(seq_len(length(names)), function(i) FALSE)
  }
  colSelectFnList <- c(allItem, oddItem, evenItem)
  histoSelectFnList <- c(allItem, oddItem, evenItem, noneItem)
  invAxesSelectFnList <- c(allItem, oddItem, evenItem, noneItem)
  inputColumnsSelectFnList <- c(allItem, oddItem, evenItem)

  noRefItem <- function(n) {
    NULL
  }
  firstItem <- function(n) {
    1
  }
  secondItem <- function(n) {
    2
  }
  lastItem <- function(n) {
    n
  }
  refRowFnList <- c(noRefItem, firstItem, lastItem)
  refColFnList <- c(noRefItem, firstItem, secondItem)

  # Create Reactive expression which generates fake data based on 'input$buildAction' changes
  fakeData <- eventReactive(input$buildAction, {
    columnNames <- sapply(1:input$columnCount, function(i) paste("Col", i, sep = ""))
    columnLabels <- sapply(1:input$columnCount, function(i) paste("Long-name<br>for<br>column", i, sep = " "))
    categoricalColumnsRatio <- as.numeric(shiny::isolate(input$categoricalColumnsSelect))
    categorical <- sapply(1:input$columnCount, function(i) {
      if (i %% categoricalColumnsRatio == 1) {
        catId <- (i + categoricalColumnsRatio - 1) / categoricalColumnsRatio
        prefix <- paste("Cat", catId, "-", sep = "")
        return(paste(prefix, c(1:(catId * 2)), sep = ""))
      }
      return(NULL)
    })
    mat <- sapply(1:input$columnCount, function(i) {
      if (i %% categoricalColumnsRatio == 1) {
        return(sample(categorical[[i]], size = input$sampleSize, replace = T))
      }
      if (i %% 2 == 0) {
        return(10^ (i %% 24) * rnorm(input$sampleSize, 35, 5))
      }
      return(10^- (i %% 24) * runif(input$sampleSize, -i, i))
    })
    cutoffs <- sapply(1:input$columnCount, function(i) {
      if (i == 2) {
        colMin <- min(as.numeric(as.character(mat[, 2])))
        colMax <- max(as.numeric(as.character(mat[, 2])))
        scaledInterval <- shiny::isolate(input$brushSlider)
        naturalInterval <- sapply(scaledInterval, function(v) colMin + (colMax - colMin) * v / 100)
        return(list(naturalInterval))
      }
      return(NULL)
    })

    toreturn <- list()
    toreturn$data <- data.frame(mat)
    names(toreturn$data) <- columnNames
    toreturn$categorical <- categorical
    toreturn$cutoffs <- cutoffs
    toreturn$columnLabels <- columnLabels
    toreturn
  })

  dataForDownload <- NULL
  ppForDownload <- NULL

  # If 'buildAction' has been clicked ...
  output$parPlot <- renderParallelPlot({
    args <- fakeData()
    dimNames <- colnames(args$data)

    inputColumnsIndex <- as.numeric(shiny::isolate(input$inputColumnsSelect))
    inputColumns <- as.logical(inputColumnsSelectFnList[[inputColumnsIndex]](dimNames))

    colFnIndex <- as.numeric(shiny::isolate(input$colSelect))
    keptColumns <- as.logical(colSelectFnList[[colFnIndex]](dimNames))

    histFnIndex <- as.numeric(shiny::isolate(input$histSelect))
    histoVisibility <- as.logical(histoSelectFnList[[histFnIndex]](dimNames))

    invAxesFnIndex <- as.numeric(shiny::isolate(input$invAxesSelect))
    invertedAxes <- as.logical(invAxesSelectFnList[[invAxesFnIndex]](dimNames))

    rowFnIndex <- as.numeric(shiny::isolate(input$refRowSelect))
    sampleSize <- as.numeric(shiny::isolate(input$sampleSize))
    refRowIndex <- refRowFnList[[rowFnIndex]](sampleSize)

    refColFnIndex <- as.numeric(shiny::isolate(input$refColSelect))
    columnCount <- as.numeric(shiny::isolate(input$columnCount))
    refColumnIndex <- refColFnList[[refColFnIndex]](columnCount)

    cssRules <-
      if (shiny::isolate(input$customStyleCB))
        list(
          "svg" = "background: #C2C2C2"
        )
      else
        NULL

    sliderPosition <-
      if (shiny::isolate(input$sliderPositionCB))
        list(
          dimCount = 8,
          startingDimIndex = 3
        )
      else
        NULL

    dataForDownload <<- args$data
    parallelPlot(
      data = dataForDownload,
      categorical = args$categorical,
      categoriesRep = as.character(shiny::isolate(input$categoriesRepSelect)),
      arrangeMethod = as.character(shiny::isolate(input$arrangeMethodSelect)),
      inputColumns = inputColumns,
      keptColumns = keptColumns,
      histoVisibility = histoVisibility,
      invertedAxes = invertedAxes,
      cutoffs = args$cutoffs,
      refRowIndex = refRowIndex,
      refColumnDim = if (is.null(refColumnIndex)) NULL else dimNames[refColumnIndex],
      rotateTitle = shiny::isolate(input$rotateTitleCB),
      columnLabels = if (shiny::isolate(input$columnLabelsCB)) args$columnLabels else NULL,
      continuousCS = as.character(shiny::isolate(input$continuousCsSelect)),
      categoricalCS = as.character(shiny::isolate(input$categoricalCsSelect)),
      eventInputId = ns("myPlotEvent"),
      editionMode = as.character(shiny::isolate(input$editionModeSelect)),
      controlWidgets = NULL,
      cssRules = cssRules,
      sliderPosition = sliderPosition
    )
  })

  # If 'arrangeMethodSelect2' has been changed ...
  observeEvent(input$arrangeMethodSelect2, {
    parallelPlot::setArrangeMethod(ns("parPlot"), input$arrangeMethodSelect2)
  })

  # If 'categoriesRepSelect2' has been changed ...
  observeEvent(input$categoriesRepSelect2, {
    parallelPlot::setCategoriesRep(ns("parPlot"), input$categoriesRepSelect2)
  })

  # If 'continuousCsSelect2' has been changed ...
  observeEvent(input$continuousCsSelect2, {
    parallelPlot::setContinuousColorScale(ns("parPlot"), input$continuousCsSelect2)
  })

  # If 'categoricalCsSelect2' has been changed ...
  observeEvent(input$categoricalCsSelect2, {
    parallelPlot::setCategoricalColorScale(ns("parPlot"), input$categoricalCsSelect2)
  })

  # If 'histSelect2' has been changed ...
  observeEvent(input$histSelect2, {
    args <- fakeData()
    dimNames <- colnames(args$data)
    histFnIndex <- as.numeric(shiny::isolate(input$histSelect2))
    if (histFnIndex == 5) {
      histoVisibility <- list()
      for (i in 1:4) {
        histoVisibility[dimNames[i]] <- list(TRUE)
      }
      parallelPlot::setHistoVisibility(ns("parPlot"), histoVisibility)
    }
    else {
      histoVisibility <- as.logical(histoSelectFnList[[histFnIndex]](dimNames))
      parallelPlot::setHistoVisibility(ns("parPlot"), histoVisibility)
    }
  })

  # If 'invAxesSelect2' has been changed ...
  observeEvent(input$invAxesSelect2, {
    args <- fakeData()
    dimNames <- colnames(args$data)
    invAxesFnIndex <- as.numeric(shiny::isolate(input$invAxesSelect2))
    if (invAxesFnIndex == 5) {
      invertedAxes <- list()
      for (i in 1:4) {
        invertedAxes[dimNames[i]] <- list(TRUE)
      }
      parallelPlot::setInvertedAxes(ns("parPlot"), invertedAxes)
    }
    else {
      invertedAxes <- as.logical(invAxesSelectFnList[[invAxesFnIndex]](dimNames))
      parallelPlot::setInvertedAxes(ns("parPlot"), invertedAxes)
    }
  })

  # If 'highlightRowAction' has been clicked ...
  observeEvent(input$highlightRowAction, {
    lastRowIndex <- nrow(fakeData()$data)
    parallelPlot::highlightRow(ns("parPlot"), lastRowIndex)
  })
  
  # If 'clearHlRowAction' has been clicked ...
  observeEvent(input$clearHlRowAction, {
    parallelPlot::highlightRow(ns("parPlot"), NULL)
  })
  
  # If 'brushSlider2' has been changed ...
  observeEvent(input$brushSlider2, {
    cutoffs <- list()
    args <- fakeData()
    dimNames <- colnames(args$data)

    colMin <- min(as.numeric(as.character(args$data[, 2])))
    colMax <- max(as.numeric(as.character(args$data[, 2])))
    scaledInterval <- shiny::isolate(input$brushSlider2)
    naturalInterval <- sapply(scaledInterval, function(v) colMin + (colMax - colMin) * v / 100)
    cutoffs[dimNames[2]] <- list(list(naturalInterval))

    parallelPlot::setCutoffs(ns("parPlot"), cutoffs)
  })

  # If 'cutoffSelect' has been changed ...
  observeEvent(input$cutoffSelect, {
    cutoffs <- list()
    args <- fakeData()
    dimNames <- colnames(args$data)

    if (input$cutoffSelect == "None") {
      cutoffs[dimNames[1]] <- list(list())
    }
    else {
      cutoffs[dimNames[1]] <- list(NULL)
    }
    parallelPlot::setCutoffs(ns("parPlot"), cutoffs)
  })

  # If 'clearCutoffsAction' has been clicked ...
  observeEvent(input$clearCutoffsAction, {
    parallelPlot::setCutoffs(ns("parPlot"), NULL)
  })

  # If 'colSelect2' has been changed ...
  observeEvent(input$colSelect2, {
    args <- fakeData()
    dimNames <- colnames(args$data)
    colFnIndex <- as.numeric(shiny::isolate(input$colSelect2))
    if (colFnIndex == 4) {
      keptColumns <- list()
      for (i in 1:4) {
        keptColumns[dimNames[i]] <- list(TRUE)
      }
      parallelPlot::setKeptColumns(ns("parPlot"), keptColumns)
    }
    else {
      keptColumns <- as.logical(colSelectFnList[[colFnIndex]](dimNames))
      parallelPlot::setKeptColumns(ns("parPlot"), keptColumns)
    }
  })

  # If 'refColSelect2' has been changed ...
  observeEvent(input$refColSelect2, {
    args <- fakeData()
    dimNames <- colnames(args$data)
    columnCount <- as.numeric(input$columnCount)
    refColFnIndex <- as.numeric(input$refColSelect2)
    refColumnIndex <- refColFnList[[refColFnIndex]](columnCount)
    refColumnDim = if (is.null(refColumnIndex)) NULL else dimNames[refColumnIndex]
    parallelPlot::setRefColumnDim(ns("parPlot"), refColumnDim)
  })

  # If 'downloadButton' has been clicked, send a 'getPlotConfig' message
  observeEvent(input$downloadButton, {
    print("observeEvent input$downloadButton")
    parallelPlot::getPlotConfig(ns("parPlot"), ns("ConfigForDownload"))
  })
  # When the 'ConfigForDownload' reactive input is changed,
  # create a parallelPlot and save it by emulating a click on 'associatedDownloadButton'.
  observeEvent(input$ConfigForDownload, {
    print("observeEvent input$ConfigForDownload")
    ppForDownload <<- parallelPlot(
      data = dataForDownload,
      categorical = input$ConfigForDownload$categorical,
      categoriesRep = input$ConfigForDownload$categoriesRep,
      arrangeMethod = input$ConfigForDownload$arrangeMethod,
      inputColumns = input$ConfigForDownload$inputColumns,
      keptColumns = input$ConfigForDownload$keptColumns,
      histoVisibility = input$ConfigForDownload$histoVisibility,
      invertedAxes = input$ConfigForDownload$invertedAxes,
      cutoffs = input$ConfigForDownload$cutoffs,
      refRowIndex = input$ConfigForDownload$refRowIndex,
      refColumnDim = input$ConfigForDownload$refColumnDim,
      rotateTitle = input$ConfigForDownload$rotateTitle,
      columnLabels = input$ConfigForDownload$columnLabels,
      continuousCS = input$ConfigForDownload$continuousCS,
      categoricalCS = input$ConfigForDownload$categoricalCS,
      controlWidgets = NULL,
      cssRules = input$ConfigForDownload$cssRules,
      sliderPosition = input$ConfigForDownload$sliderPosition
    )
    shinyjs::runjs(paste0("document.getElementById('", ns("associatedDownloadButton"), "').click();"))
  })

  # if a click on 'associatedDownloadButton' occurred, save 'ppForDownload' widget (previously created)
  output$associatedDownloadButton <- downloadHandler(
    filename = function() {
      paste("parallelPlot-", Sys.Date(), ".html", sep = "")
    },
    content = function(tmpContentFile) {
      print("downloadHandler-saveWidget")
      htmlwidgets::saveWidget(ppForDownload, tmpContentFile)
    }
  )

  # If 'getCutoffsAction' has been clicked ...
  observeEvent(input$getCutoffsAction, {
    attributeType <- "Cutoffs"
    parallelPlot::getValue(ns("parPlot"), attributeType, ns("MyCutoffs"))
  })
  # When the 'MyCutoffs' reactive input is changed, open a dialog to display it
  observeEvent(input$MyCutoffs, {
    showModal(modalDialog(
      title = "Cutoffs",
      shiny:::toJSON(input$MyCutoffs)
    ))
  })

  # If 'getSelectedTracesAction' has been clicked ...
  observeEvent(input$getSelectedTracesAction, {
    attributeType <- "SelectedTraces"
    parallelPlot::getValue(ns("parPlot"), attributeType, ns("MySelectedTraces"))
  })
  # When the 'MySelectedTraces' reactive input is changed, open a dialog to display it
  observeEvent(input$MySelectedTraces, {
    showModal(modalDialog(
      title = "Selected Lines",
      toString(input$MySelectedTraces)
    ))
  })

  # If 'getRefColumnAction' has been clicked ...
  observeEvent(input$getRefColumnAction, {
    attributeType <- "ReferenceColumn"
    parallelPlot::getValue(ns("parPlot"), attributeType, ns("MyReferenceColumn"))
  })
  # When the 'MyReferenceColumn' reactive input is changed, open a dialog to display it
  observeEvent(input$MyReferenceColumn, {
    showModal(modalDialog(
      title = "Reference column (used to determine the color to attribute to a row)",
      # TODO: Find how to manage 'null' value
      if (input$MyReferenceColumn == "NULL") {
        "No Reference Column"
      }
      else {
        toString(input$MyReferenceColumn)
      }
    ))
  })

  # If 'myPlotEvent' has been changed ...
  observeEvent(input$myPlotEvent, {
    if (!is.null(input$myPlotEvent) && input$myPlotEvent$type == "rowClicked") {
      shiny::showNotification("a row has been clicked")
    }
  })

  # When the 'myPlotEvent' reactive input is changed, if type is "cutoffChange", update 'updatedCutoff' text field
  cutoffContent <- "Updated cutoff:"
  output$updatedCutoff <- renderText({
    if (!is.null(input$myPlotEvent) && input$myPlotEvent$type == "cutoffChange") {
      updatedDim <- input$myPlotEvent$value$updatedDim
      cutoffs <- input$myPlotEvent$value$cutoffs
      if (cutoffs[updatedDim] == "NULL") {
        cutoffContent <<- paste("Updated cutoff:", updatedDim, "none")
      }
      else {
        cutoffContent <<- paste("Updated cutoff:", updatedDim, paste(cutoffs[updatedDim], collapse = ";"))
      }
    }
    cutoffContent
  })

  # When the 'myPlotEvent' reactive input is changed, if type is "cutoffChange", update 'selectedTraces' text field
  tracesContent <- "Selected lines:"
  output$selectedTraces <- renderText({
    if (!is.null(input$myPlotEvent) && input$myPlotEvent$type == "cutoffChange") {
      selectedTraces <- input$myPlotEvent$value$selectedTraces
      if (length(selectedTraces) > 10) {
        stringRep <- paste(toString(selectedTraces[c(1, 2, 3, 4)]),
                           "...",
                           toString(selectedTraces[c(-3, -2, -1, 0) + length(selectedTraces)]))
        tracesContent <<- paste("Selected lines:", stringRep, "(", length(selectedTraces), " lines )")
      }
      else {
        tracesContent <<- paste("Selected lines:", toString(selectedTraces), "(", length(selectedTraces), " lines )")
      }
    }
    tracesContent
  })

  # When the 'myPlotEvent' reactive input is changed, if type is "pointChange", send a message 'changeRow' to the plot
  observeEvent(input$myPlotEvent, {
    if (!is.null(input$myPlotEvent) && input$myPlotEvent$type == "pointChange") {
      dim <- input$myPlotEvent$value$dim
      rowIndex <- as.numeric(input$myPlotEvent$value$rowIndex)

      # Build 'fakeChangedRow' (which contents some fake changes)
      fakeChangedRow <- list()
      args <- fakeData()
      dimNames <- colnames(args$data)
      colIndex <- match(dim, dimNames)
      matrixCol <- as.matrix(args$data[colIndex])
      if (is.null(args$categorical[[colIndex]])) {
        newValue <- as.numeric(input$myPlotEvent$value$newValue)
        oldValue <- as.numeric(matrixCol[rowIndex])
        alpha <- (newValue - oldValue) / (as.numeric(max(matrixCol)) - as.numeric(min(matrixCol)))
      }
      else {
        categories <- sort(unique(matrixCol))
        newCatIndex <- match(input$myPlotEvent$value$newValue, categories, 0)
        oldCatIndex <- match(matrixCol[rowIndex], categories, 0)
        alpha <- (newCatIndex - oldCatIndex) / length(categories)
      }
      for (i in seq_len(length(dimNames))) {
        curCol <- as.matrix(args$data[i])
        curDim <- dimNames[i]
        if (curDim == dim) {
          fakeChangedRow[dim] <- input$myPlotEvent$value$newValue
        }
        else {
          if (is.null(args$categorical[[i]])) {
            curValue <- as.numeric(curCol[rowIndex])
            fakeChangedRow[curDim] <- curValue + alpha * (as.numeric(max(curCol)) - as.numeric(min(curCol)))
          }
          else {
            curValue <- curCol[rowIndex]
            categories <- sort(unique(curCol))
            catIndex <- match(curValue, categories, 0)
            newCatIndex <- catIndex + alpha * (length(categories) - 1)
            if (newCatIndex >= 1 && newCatIndex <= length(categories)) {
              fakeChangedRow[curDim] <- categories[newCatIndex]
            }
          }
        }
      }

      parallelPlot::changeRow(ns("parPlot"), rowIndex, fakeChangedRow)
    }
  })
}
