import { JSDOM } from "jsdom";
import { throws, equal, deepEqual, ok } from "assert";
import { stderr } from "test-console";

// *********************
// Properties definition
// *********************

async function initMtcarsJsdom() {
    return initJsdom("test/mocha/jsdom-mtcars.html");
}

async function initIrisJsdom() {
    return initJsdom("test/mocha/jsdom-iris.html");
}

async function initJsdom(htmlFile) {
    const jsdom = await JSDOM.fromFile(htmlFile, {
        resources: "usable",
        runScripts: "dangerously"
    });
    await new Promise(resolve =>
        jsdom.window.addEventListener("load", resolve)
    );
    return jsdom;
}

// ****************
// Test Definitions
// ****************

// eslint-disable-next-line max-lines-per-function
describe("arguments", function () {
    this.timeout(0);

    describe("id", function () {
        describe("#wrongBindTo", args_id_wrongBindTo);
    });

    describe("data", function () {
        describe("#rowOrientedData", args_data_validData);
        describe("#notRowOrientedData", args_data_notRowOriented);
        describe("#dataWithoutLine", args_data_noLines);
    });

    describe("categorical", function () {
        describe("#acceptValidArray", args_categorical_validArray);
        describe("#notCategoricalArray", args_categorical_notAnArray);
        describe("#wrongCategoricalLength", args_categorical_wrongArrayLength);
    });

    describe("categoriesRep", function () {
        describe("#acceptValidValue", args_categoriesRep_validValue);
        describe("#fixInvalidValue", args_categoriesRep_fixInvalidValue);
    });

    describe("arrangeMethod", function () {
        describe("#acceptValidValue", args_arrangeMethod_validValue);
        describe("#fixInvalidValue", args_arrangeMethod_fixInvalidValue);
    });

    describe("inputColumns", function () {
        describe("#acceptValidArray", args_inputColumns_validArray);
        describe("#invalidArrayType", args_inputColumns_invalidArrayType);
        describe("#wrongArrayLength", args_inputColumns_wrongArrayLength);
    });

    describe("keptColumns", function () {
        describe("#acceptValidArray", args_keptColumns_validArray);
        // describe("#invalidArrayType", args_keptColumns_invalidArrayType);
        // describe("#wrongArrayLength", args_keptColumns_wrongArrayLength);
    });

    describe("histoVisibility", function () {
        describe("#acceptValidArray", args_histo_validArray);
        // describe("#invalidArrayType", args_histo_invalidArrayType);
        // describe("#wrongArrayLength", args_histo_wrongArrayLength);
    });

    // 'invertedAxes' argument

    describe("cutoffs", function () {
        describe("#acceptValidArray", args_cutoffs_validArray);
        // describe("#invalidArrayType", args_cutoffs_invalidArrayType);
        // describe("#wrongArrayLength", args_cutoffs_wrongArrayLength);
        // describe("#invalidContinuousType", args_cutoffs_invalidContinuousType);
        // describe("#noNumericalIntervals", args_cutoffs_noNumericalIntervals);
        // describe("#NotTwoTaluesIntervals", args_cutoffs_NotTwoTaluesIntervals);
        // describe("#invalidCategoricalType", args_cutoffs_invalidCategoricalType);
        // describe("#warnForUnknownCategories", args_cutoffs_warnForUnknownCategories);
    });

    describe("refRowIndex", function () {
        describe("#acceptValidRowIndex", args_refRowIndex_validValue);
        describe("#notNumericRefRowIndex", args_refRowIndex_notNumeric);
        describe("#outOfRangeRowIndex", args_refRowIndex_outOfRange);
    });

    describe("refColumnDim", function () {
        describe("#acceptValidColumnDim", args_refColumnDim_validValue);
        describe("#unknownColumnDim", args_refColumnDim_unknownValue);
    });

    describe("rotateTitle", function () {
        describe("#acceptTrueValue", args_rotateTitle_validTrue);
        describe("#acceptFalseValue", args_rotateTitle_validFalse);
    });

    describe("columnLabels", function () {
        describe("#acceptValidArray", args_columnLabels_validArray);
        describe("#invalidArrayType", args_columnLabels_invalidArrayType);
        // describe("#wrongArrayLength", args_columnLabels_wrongArrayLength);
    });

    describe("continuousCS", function () {
        describe("#acceptValidValue", args_continuousCS_validValue);
        describe("#fixInvalidValue", args_continuousCS_fixInvalidValue);
    });

    describe("categoricalCS", function () {
        describe("#acceptValidValue", args_categoricalCS_validValue);
        describe("#fixInvalidValue", args_categoricalCS_fixInvalidValue);
    });

    // describe("editionMode", function () {
    //     describe("#acceptValidValue", args_editionMode_validValue);
    // });

    // 'cssRules' argument
    // 'sliderPosition' argument

});

describe("API", function () {
    this.timeout(0);
    
    // 'setArrangeMethod'

    // 'setCategoriesRep'

    describe("setRefColumnDim", function () {
        describe("#acceptValidColumnDim", api_refColumnDim_validValue);
        describe("#unknownColumnDim", api_refColumnDim_unknownValue);
    });

    describe("setContinuousColorScale", function () {
        describe("#acceptValidValue", api_continuousCS_validValue);
        describe("#fixInvalidValue", api_continuousCS_fixInvalidValue);
    });

    describe("setCategoricalColorScale", function () {
        describe("#acceptValidValue", api_categoricalCS_validValue);
        describe("#fixInvalidValue", api_categoricalCS_fixInvalidValue);
    });

    describe("setHistoVisibility", function () {
        describe("#acceptValidArray", api_histo_validArray);
        // describe("#invalidArrayType", api_histo_invalidArrayType);
        // describe("#wrongArrayLength", api_histo_wrongArrayLength);
    });

    // 'setInvertedAxes'

    describe("setCutoffs", function () {
        describe("#acceptValidArray", api_cutoffs_validArray);
        // describe("#invalidArrayType", api_cutoffs_invalidArrayType);
        // describe("#wrongArrayLength", api_cutoffs_wrongArrayLength);
        // describe("#invalidContinuousType", api_cutoffs_invalidContinuousType);
        // describe("#noNumericalIntervals", api_cutoffs_noNumericalIntervals);
        // describe("#NotTwoTaluesIntervals", api_cutoffs_NotTwoTaluesIntervals);
        // describe("#invalidCategoricalType", api_cutoffs_invalidCategoricalType);
        // describe("#warnForUnknownCategories", api_cutoffs_warnForUnknownCategories);
    });

    // describe("setKeptColumns", function () {
        // describe("#acceptValidArray", api_keptColumns_validArray);
        // describe("#invalidArrayType", api_keptColumns_invalidArrayType);
        // describe("#wrongArrayLength", api_keptColumns_wrongArrayLength);
    // });

    // describe("getValue", function () {
    //     describe("#acceptValidArray", validHistoArray);
    // });

    // describe("highlightRow", function () {
    //     describe("#acceptValidArray", validHistoArray);
    // });

    // describe("changeRow", function () {
    //     describe("#acceptValidArray", validHistoArray);
    // });

    // 'getPlotConfig'

});

// *****************
// Arguments section
// *****************

// *************
// 'id' argument
// *************

function args_id_wrongBindTo() {
    it("should refuse 'id' being not in dom and throw an error", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("wrongId", 1000, 500);
        `);

        const config = {
            data: []
        }
        throws(
            () => window.parallelPlot.generate(config), 
            /Error: 'bindto' dom element not found:#wrongId/
        );
    });
}

// ***************
// 'data' argument
// ***************

function args_data_validData() {
    it("should accept a D3 friendly (row-oriented) data", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const config = {
            data: window.mtcars
        }
        generate(window.parallelPlot, config, []);

        equal(
            window.parallelPlot.sampleData.length, 
            window.mtcars.length,
            "'parallelPlot.sampleData' length should be equal to 'mtcars' row count"
        );
    });
}

function args_data_notRowOriented() {
    it("should refuse invalid value and throw an error", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const config = {
            data: "invalid data"
        }
        throws(
            () => window.parallelPlot.generate(config), 
            /Error: given dataset is not a D3 friendly \(row-oriented\) data/
        );
    });
}

function args_data_noLines() {
    it("should refuse data without line and throw an error", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const config = {
            data: []
        }
        throws(
            () => window.parallelPlot.generate(config), 
            /Error: given dataset contains no line/
        );
    });
}

// **********************
// 'categorical' argument
// **********************

function args_categorical_validArray() {
    it("should accept a valid list for categorical", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const config = {
            data: window.mtcars,
            categorical: categorical
        }
        generate(window.parallelPlot, config, []);

        equal(
            window.parallelPlot.dimensions.length, 
            window.mtcars.columns.length - 1, // 'model' column is removed
            "'parallelPlot.dimensions' length should be equal to 'mtcars' column count minus one"
        );
        for (let i = 0; i < window.parallelPlot.dimensions.length; i++) {
            const dimCategorical = window.parallelPlot.columns[window.parallelPlot.dimensions[i]].categorical;
            equal(
                dimCategorical ? dimCategorical.categories : null, 
                categorical[i + 1], // 'model' column is removed
                `Categorical ${i} doesn't contain the expected categories`
            );
        }

        const reducer = (accumulator, currentCategories) => accumulator + (currentCategories && currentCategories.length);
        equal(
            window.d3.select(window.document).selectAll(".category rect").size(),
            categorical.reduce(reducer),
            "The number of category rectangle is not the expected one"
            );
    });
}

function args_categorical_notAnArray() {
    it("should fix invalid elements and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = "invalid";
        const config = {
            data: window.mtcars,
            categorical: categorical
        }
        generate(window.parallelPlot, config, [ "'categorical' must be an array\n" ]);

        equal(
            window.parallelPlot.dimensions.length, 
            window.mtcars.columns.length - 1, // 'model' column is removed
            "'parallelPlot.dimensions' length should be equal to 'mtcars' column count minus one"
        );

        for (let i = 0; i < window.parallelPlot.dimensions.length; i++) {
            equal(
                window.parallelPlot.columns[window.parallelPlot.dimensions[i]].categorical, 
                null,
                `Categorical ${i} is not 'null' as expected`
            );
        }
    });
}

function args_categorical_wrongArrayLength() {
    it("should fix invalid length and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const config = {
            data: window.mtcars,
            categorical: categorical
        }
        generate(window.parallelPlot, config, [ "Length of 'categorical' must be equal to the number of columns of 'data'\n" ]);

        equal(
            window.parallelPlot.dimensions.length, 
            window.mtcars.columns.length - 1, // 'model' column is removed
            "'parallelPlot.dimensions' length should be equal to 'mtcars' column count minus one"
        );

        for (let i = 0; i < window.parallelPlot.dimensions.length; i++) {
            equal(
                window.parallelPlot.columns[window.parallelPlot.dimensions[i]].categorical, 
                null,
                `Categorical ${i} is not 'null' as expected`
            );
        }
    });
}

// **********************
// 'categoriesRep' argument
// **********************

function args_categoriesRep_validValue() {
    it("should accept a valid value for categoriesRep", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const config = {
            data: window.mtcars,
            categorical: categorical,
            categoriesRep: "EquallySizedBoxes"
        }
        generate(window.parallelPlot, config, []);
    });
}

function args_categoriesRep_fixInvalidValue() {
    it("should fix invalid 'categoriesRep' and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const categoriesRep = "invalid";
        const config = {
            data: window.mtcars,
            categorical: categorical,
            categoriesRep: categoriesRep
        }
        generate(window.parallelPlot, config, [`Unknown categoriesRep: ${categoriesRep}\n` ]);
    });
}

// **********************
// 'arrangeMethod' argument
// **********************

function args_arrangeMethod_validValue() {
    it("should accept a valid value for arrangeMethod", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const config = {
            data: window.mtcars,
            categorical: categorical,
            arrangeMethod: "fromRight"
        }
        generate(window.parallelPlot, config, []);
    });
}

function args_arrangeMethod_fixInvalidValue() {
    it("should fix invalid 'arrangeMethod' and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const arrangeMethod = "invalid";
        const config = {
            data: window.mtcars,
            categorical: categorical,
            arrangeMethod: arrangeMethod
        }
        generate(window.parallelPlot, config, [`Unknown arrangeMethod: ${arrangeMethod}\n` ]);
    });
}

// ***********************
// 'inputColumns' argument
// ***********************

function args_inputColumns_validArray() {
    it("should accept a valid list for inputColumns", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const inputColumns = [true, false, true, true, false, false, true, false, true, true, true, true];
        const config = {
            data: window.mtcars,
            inputColumns: inputColumns
        }
        generate(window.parallelPlot, config, []);

        const dimension = window.d3.select(".parallelPlot")
            .selectAll("g.dimension");
        equal(
            dimension.size(),
            inputColumns.length - 1, // 'model' column is removed
            "The number of 'dimension' groups is not as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return window.d3.select(element).classed("input");
            }),
            inputColumns.slice(1), // 'model' column is removed
            "Some 'dimension' groups doesn't have an 'input' class as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return !window.d3.select(element).classed("output");
            }),
            inputColumns.slice(1), // 'model' column is removed
            "Some 'dimension' groups doesn't have an 'output' class as expected"
        );
    });
}

function args_inputColumns_invalidArrayType() {
    it("should fix invalid array type and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const inputColumns = "invalid";
        const config = {
            data: window.mtcars,
            inputColumns: inputColumns
        }
        generate(window.parallelPlot, config, ["'inputColumns' must be an array\n"]);

        const dimension = window.d3.select(".parallelPlot")
            .selectAll("g.dimension");
        equal(
            dimension.size(),
            window.parallelPlot.dimensions.length,
            "The number of 'dimension' groups is not as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return window.d3.select(element).classed("input");
            }),
            window.parallelPlot.dimensions.map(_d => true),
            "Some 'dimension' groups doesn't have an 'input' class as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return !window.d3.select(element).classed("output");
            }),
            window.parallelPlot.dimensions.map(_d => true),
            "Some 'dimension' groups doesn't have an 'output' class as expected"
        );
    });
}

function args_inputColumns_wrongArrayLength() {
    it("should fix invalid array type and print a message", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const inputColumns = [true, false];
        const config = {
            data: window.mtcars,
            inputColumns: inputColumns
        }
        generate(window.parallelPlot, config, ["Length of 'inputColumns' must be equal to the number of columns of 'data'\n"]);

        const dimension = window.d3.select(".parallelPlot")
            .selectAll("g.dimension");
        equal(
            dimension.size(),
            window.parallelPlot.dimensions.length,
            "The number of 'dimension' groups is not as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return window.d3.select(element).classed("input");
            }),
            window.parallelPlot.dimensions.map(_d => true),
            "Some 'dimension' groups doesn't have an 'input' class as expected"
        );

        deepEqual(
            dimension.nodes().map(function (element) {
                return !window.d3.select(element).classed("output");
            }),
            window.parallelPlot.dimensions.map(_d => true),
            "Some 'dimension' groups doesn't have an 'output' class as expected"
        );
    });
}

// **********************
// 'keptColumns' argument
// **********************

function args_keptColumns_validArray() {
    it("should accept a valid list for keptColumns", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const keptColumns = [true, false, true, true, false];
        const config = {
            data: window.iris,
            keptColumns: keptColumns
        }
        generate(window.parallelPlot, config, []);

        const keptDimensions = window.iris.columns.filter((_dim, i) => keptColumns[i]);

        const axisLabel = window.d3.select(".parallelPlot")
            .selectAll("g.dimension text.axisLabel");
        equal(
            axisLabel.size(),
            keptDimensions.length,
            "The number of 'axisLabel' texts is not as expected"
        );

        deepEqual(
            axisLabel.nodes().map(function (element) {
                return window.d3.select(element).text();
            }),
            keptDimensions,
            "Some 'dimension' groups are not labeled as expected"
        );
    });
}

// ********************
// 'histogram' argument
// ********************

function args_histo_validArray() {
    it("should accept a valid list for histogram", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const histoVisibility = categorical.map(_cats => true);
        const config = {
            data: window.mtcars,
            categorical: categorical,
            histoVisibility: histoVisibility
        }
        generate(window.parallelPlot, config, []);

        ["histogram", "histogramSelected"].forEach(histoClass => {
            const histoGroupCount = categorical.reduce(
                (accumulator, curCategorical) => accumulator + (curCategorical === null ? 1 : curCategorical.length),
                -1
            );
            
            equal(
                window.d3.select(window.document).selectAll(`g.${histoClass}`).size(),
                histoGroupCount,
                `The number of '${histoClass}' groups is not as expected`
            );
            
            const histoCatRectCount = categorical.reduce(
                (accumulator, curCategorical) => accumulator + (curCategorical === null ? 0 : curCategorical.length),
                0
            );
              
            equal(
                window.d3.select(window.document).selectAll(`g.category .${histoClass} rect`).size(),
                histoCatRectCount,
                `The number of '${histoClass}' rects for categoricals is not as expected`
            );
        });
    });
}

// ******************
// 'cutoffs' argument
// ******************

function args_cutoffs_validArray() {
    it("should accept a valid list for cutoffs", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const cutoffs = [[[6, 7]], null, null, null, ["Virginica", "Setosa"]]
        const config = {
            data: window.iris,
            categorical: categorical,
            cutoffs: cutoffs
        }
        generate(window.parallelPlot, config, []);

        equal(
            window.d3.select(window.document).selectAll("g.foreground path").size(),
            window.iris.length,
            "The number of 'foreground' paths is not equal to 'iris' row count"
        );
        
        const keptCount = window.iris.filter(row => 
            row["sepal.length"] <= 7 && 
            row["sepal.length"] >= 6 &&
            // eslint-disable-next-line dot-notation
            ["Virginica", "Setosa"].includes(row["variety"])
        ).length;

        const invisibleCount = window.d3.select(window.document).selectAll("g.foreground path")
            .filter(function() { return window.d3.select(this).style("display") === "none"; })
            .size();

        equal(
            invisibleCount,
            window.iris.length - keptCount,
            "The number of invisible 'foreground' paths is not as expected"
        );
    });
}

// **********************
// 'refRowIndex' argument
// **********************

function args_refRowIndex_validValue() {
    it("should accept a valid 'refRowIndex'", async function () {
        const refRowIndex = 0;

        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            refRowIndex: refRowIndex
        }
        generate(window.parallelPlot, config, []);

        const d = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === refRowIndex)
            .attr("d");
        const segments = d.split("L");
        const [minY, maxY] = window.d3.extent(segments, seg => seg.split(",")[1]);

        equal(
            minY, 
            maxY,
            "The 'foreground' path corresponding to the 'refRowIndex' doesn't look horizontal"
        );
    });
}

function args_refRowIndex_notNumeric() {
    it("should fix invalid 'refRowIndex' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refRowIndex = "invalid";
        const config = {
            data: window.iris,
            refRowIndex: refRowIndex
        }
        generate(window.parallelPlot, config, [ "'refRowIndex' must be of integer type\n" ]);

        equal(
            window.parallelPlot.refRowIndex, 
            null,
            "'parallelPlot.refRowIndex' is not 'null' as expected"
        );
    });
}

function args_refRowIndex_outOfRange() {
    it("should fix out of range 'refRowIndex' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refRowIndex = -1;
        const config = {
            data: window.iris,
            refRowIndex: refRowIndex
        }
        generate(window.parallelPlot, config, [ `refRowIndex: ${refRowIndex} must be a valid row index, it must be in range: [1, ${window.iris.length - 1}]\n` ]);

        equal(
            window.parallelPlot.refRowIndex, 
            null,
            "'parallelPlot.refRowIndex' is not 'null' as expected"
        );
    });
}

// ***********************
// 'refColumnDim' argument
// ***********************

// eslint-disable-next-line max-lines-per-function
function args_refColumnDim_validValue() {
    // eslint-disable-next-line max-lines-per-function
    it("should accept a valid 'refColumnDim'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const refColumnDim = window.iris.columns[4]; // "variety"
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim
        }
        generate(window.parallelPlot, config, []);

        equal(
            window.d3.select(".parallelPlot")
                .selectAll("g.dimension").size(),
            categorical.length,
            "The number of 'dimension' groups is not as expected"
        );

        const catRect = window.d3.select(".parallelPlot")
            .selectAll("g.dimension")
            .filter(dim => dim === refColumnDim)
            .selectAll(".category rect");

        equal(
            catRect.size(), 
            categorical[4].length,
            "The number of 'category' rect for the 'refColumnDim' is not as expected"
        );
    
        catRect.each(function(_d, i) {
            equal(
                window.d3.select(this).attr("fill"), 
                window.d3.schemeCategory10[i],
                `'Category' rect ${i} is not of the expected color`
            );

            equal(
                window.d3.select(window.document)
                    .selectAll("g.foreground path")
                    .filter(function(_dim) { 
                        return window.d3.select(this).attr("stroke") === window.d3.schemeCategory10[i];
                    }).size(),
                window.iris.filter(row => 
                    // eslint-disable-next-line dot-notation
                    row[refColumnDim] === categorical[4][i]
                ).length,
                `The number of 'foreground' path having a stroke of color ${i} is not as expected`
            );
        });
    });
}

function args_refColumnDim_unknownValue() {
    it("should fix unknown dimension value and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const refColumnDim = "unknown";
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim
        }

        generate(window.parallelPlot, config, [ `Unknown 'refColumnDim': ${refColumnDim}\n` ]);

        equal(
            window.parallelPlot.refColumnDim, 
            null,
            "'parallelPlot.refColumnDim' is not 'null' as expected"
        );
    });
}

// **********************
// 'rotateTitle' argument
// **********************

function args_rotateTitle_validTrue() {
    it("should accept 'true' value for rotateTitle", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            rotateTitle: true
        }

        generate(window.parallelPlot, config, []);

        const axisLabel = window.d3.select(".parallelPlot")
            .selectAll("g.dimension text.axisLabel");
        equal(
            axisLabel.size(),
            window.iris.columns.length,
            "The number of 'axisLabel' texts is not as expected"
        );

        axisLabel.each(function(dim) {
            ok(
                window.d3.select(this).attr("transform").includes("rotate"), 
                `Axis '${dim}' is not inclined as expected`
            );
        });
    });
}

function args_rotateTitle_validFalse() {
    it("should accept 'false' value for rotateTitle", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            rotateTitle: false
        }

        generate(window.parallelPlot, config, []);

        const axisLabel = window.d3.select(".parallelPlot")
            .selectAll("g.dimension text.axisLabel");
        equal(
            axisLabel.size(),
            window.iris.columns.length,
            "The number of 'axisLabel' texts is not as expected"
        );

        axisLabel.each(function(dim) {
            const transform = window.d3.select(this).attr("transform");
            ok(
                transform === null || !transform.includes("rotate"), 
                `Axis '${dim}' is not horizontal as expected`
            );
        });
    });
}

// **********************
// 'columnLabels' argument
// **********************

function args_columnLabels_validArray() {
    it("should accept a valid list for columnLabels", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        const columnLabels = window.iris.columns.map((_dim, i) => "Col" + i);
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            columnLabels: columnLabels
        }

        generate(window.parallelPlot, config, []);

        const axisLabel = window.d3.select(".parallelPlot")
            .selectAll("g.dimension text.axisLabel");
        equal(
            axisLabel.size(),
            window.iris.columns.length,
            "The number of 'axisLabel' texts is not as expected"
        );

        deepEqual(
            axisLabel.nodes().map(function (element) {
                return window.d3.select(element).text();
            }),
            columnLabels,
            "Some 'dimension' groups are not labeled as expected"
        );
    });
}

function args_columnLabels_invalidArrayType() {
    it("should fix invalid elements and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        const columnLabels = "invalid";
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            columnLabels: columnLabels
        }

        generate(window.parallelPlot, config, [ "'columnLabels' must be an array\n" ]);

        const axisLabel = window.d3.select(".parallelPlot")
            .selectAll("g.dimension text.axisLabel");
        equal(
            axisLabel.size(),
            window.iris.columns.length,
            "The number of 'axisLabel' texts is not as expected"
        );

        deepEqual(
            axisLabel.nodes().map(function (element) {
                return window.d3.select(element).text();
            }),
            window.iris.columns,
            "Some 'dimension' groups are not labeled as expected"
        );
    });
}

// **********************
// 'continuousCS' argument
// **********************

function args_continuousCS_validValue() {
    it("should accept a valid 'continuousCS'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refColumnDim = window.iris.columns[0]; // "sepal.length"
        const continuousCsId = window.parallelPlot.constructor.CONTINUOUS_CS_IDS[6]; // "Rainbow"

        const config = {
            data: window.iris,
            refColumnDim: refColumnDim,
            continuousCS: continuousCsId
        }
        generate(window.parallelPlot, config, []);

        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            continuousRowColor(window, config, continuousCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

function continuousRowColor(window, config, continuousCsId, checkedRowIndex) {
    const continuousCs = window.parallelPlot.constructor.CONTINUOUS_CS[continuousCsId]
    const refColExtent = window.d3.extent(config.data, function(row) {
        return +row[config.refColumnDim];
    });

    // Build a 'scaleLinear' to retrieve a 'nice' domain
    const numbin = Math.ceil(2.5 * Math.pow(config.data.length, 0.25));
    const yScale = window.d3.scaleLinear().domain(refColExtent).nice(numbin);

    const colorScale = window.d3.scaleSequential(continuousCs).domain(yScale.domain());
    const checkedRowValue = +config.data[checkedRowIndex][config.refColumnDim];

    return colorScale(checkedRowValue);
}

function args_continuousCS_fixInvalidValue() {
    it("should fix invalid 'continuousCS' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refColumnDim = window.iris.columns[0]; // "sepal.length"
        const continuousCsId = "invalid";

        const config = {
            data: window.iris,
            refColumnDim: refColumnDim,
            continuousCS: continuousCsId
        }
        generate(window.parallelPlot, config, [ `Unknown continuous color scale: ${continuousCsId}\n` ]);

        const defaultContinuousCsId = window.parallelPlot.constructor.CONTINUOUS_CS_IDS[0]; // "Viridis"
        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            continuousRowColor(window, config, defaultContinuousCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

// **********************
// 'categoricalCS' argument
// **********************

function args_categoricalCS_validValue() {
    it("should accept a valid 'categoricalCS'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refColumnDim = window.iris.columns[4]; // "variety"
        const categoricalCsId = window.parallelPlot.constructor.CATEGORIAL_CS_IDS[4]; // "Set1"

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim,
            categoricalCS: categoricalCsId
        }
        generate(window.parallelPlot, config, []);

        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            categoricalRowColor(window, config, categoricalCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

function categoricalRowColor(window, config, categoricalCsId, checkedRowIndex) {
    const refColumnIndex = config.data.columns.indexOf(config.refColumnDim);
    const categoricalCs = window.parallelPlot.constructor.CATEGORIAL_CS[categoricalCsId]
    const checkedRowValue = +config.data[checkedRowIndex][config.refColumnDim];
    const colorScale = categoricalCs.domain([0, config.categorical[refColumnIndex].length - 1])

    return colorScale(checkedRowValue);
}

function args_categoricalCS_fixInvalidValue() {
    it("should fix invalid 'categoricalCS' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const refColumnDim = window.iris.columns[4]; // "variety"
        const categoricalCsId = "invalid";

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim,
            categoricalCS: categoricalCsId
        }
        generate(window.parallelPlot, config, [ `Unknown categorical color scale: ${categoricalCsId}\n` ]);

        const defaultCategoricalCsId = window.parallelPlot.constructor.CATEGORIAL_CS_IDS[0]; // "Category10"
        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            categoricalRowColor(window, config, defaultCategoricalCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

// ***********
// API section
// ***********

// *****************
// 'setRefColumnDim'
// *****************

// eslint-disable-next-line max-lines-per-function
function api_refColumnDim_validValue() {
    // eslint-disable-next-line max-lines-per-function
    it("should accept a valid 'refColumnDim'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const refColumnDim = window.iris.columns[4]; // "variety"
        const config = {
            data: window.iris,
            categorical: categorical
        }
        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setRefColumnDim(refColumnDim);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, []);

        equal(
            window.d3.select(".parallelPlot")
                .selectAll("g.dimension").size(),
            categorical.length,
            "The number of 'dimension' groups is not as expected"
        );

        const catRect = window.d3.select(".parallelPlot")
            .selectAll("g.dimension")
            .filter(dim => dim === refColumnDim)
            .selectAll(".category rect");

        equal(
            catRect.size(), 
            categorical[4].length,
            "The number of 'category' rect for the 'refColumnDim' is not as expected"
        );
    
        catRect.each(function(_d, i) {
            equal(
                window.d3.color(window.d3.select(this).attr("fill")).formatHex(),
                window.d3.color(window.d3.schemeCategory10[i]).formatHex(),
                // window.d3.select(this).attr("fill"), 
                // window.d3.schemeCategory10[i],
                `'Category' rect ${i} is not of the expected color`
            );

            equal(
                window.d3.select(window.document)
                    .selectAll("g.foreground path")
                    .filter(function(_dim) { 
                        return window.d3.color(window.d3.select(this).attr("stroke")).formatHex() === window.d3.color(window.d3.schemeCategory10[i]).formatHex();
                        // return window.d3.select(this).attr("stroke") === window.d3.schemeCategory10[i];
                    }).size(),
                window.iris.filter(row => 
                    // eslint-disable-next-line dot-notation
                    row[refColumnDim] === categorical[4][i]
                ).length,
                `The number of 'foreground' path having a stroke of color ${i} is not as expected`
            );
        });
    });
}

function api_refColumnDim_unknownValue() {
    it("should fix unknown dimension value and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const refColumnDim = "unknown";
        const config = {
            data: window.iris,
            categorical: categorical
        }

        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setRefColumnDim(refColumnDim);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, [ `Unknown ref comumn dim: ${refColumnDim}\n` ]);
        equal(
            window.parallelPlot.refColumnDim, 
            null,
            "'parallelPlot.refColumnDim' is not 'null' as expected"
        );
    });
}

// ***********************
// setContinuousColorScale
// ***********************

function api_continuousCS_validValue() {
    it("should accept a valid string for 'continuousCS'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const refColumnDim = window.iris.columns[0]; // "sepal.length"
        const continuousCsId = window.parallelPlot.constructor.CONTINUOUS_CS_IDS[6]; // "Rainbow"

        const config = {
            data: window.iris,
            refColumnDim: refColumnDim
        }
        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setContinuousColorScale(continuousCsId);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, []);

        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            continuousRowColor(window, config, continuousCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

function api_continuousCS_fixInvalidValue() {
    it("should ignore invalid 'continuousCS' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const refColumnDim = window.iris.columns[0]; // "sepal.length"
        const continuousCsId = "invalid";

        const config = {
            data: window.iris,
            refColumnDim: refColumnDim
        }
        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setContinuousColorScale(continuousCsId);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, [ `Unknown continuous color scale: ${continuousCsId}\n` ]);

        const defaultContinuousCsId = window.parallelPlot.constructor.CONTINUOUS_CS_IDS[0]; // "Viridis"
        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            continuousRowColor(window, config, defaultContinuousCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

// ***********************
// setCategoricalColorScale
// ***********************

function api_categoricalCS_validValue() {
    it("should accept a valid 'categoricalCS'", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const refColumnDim = window.iris.columns[4]; // "variety"
        const categoricalCsId = window.parallelPlot.constructor.CATEGORIAL_CS_IDS[4]; // "Set1"

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim
        }
        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setCategoricalColorScale(categoricalCsId);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, []);

        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            categoricalRowColor(window, config, categoricalCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

function api_categoricalCS_fixInvalidValue() {
    it("should fix invalid 'categoricalCS' and print a message", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);
        window.parallelPlot.changeColorDuration = 0; // is to combine with 'timerFlush' call

        const refColumnDim = window.iris.columns[4]; // "variety"
        const categoricalCsId = "invalid";

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical,
            refColumnDim: refColumnDim
        }
        generate(window.parallelPlot, config, []);

        const inspect = stderr.inspect();
        window.parallelPlot.setCategoricalColorScale(categoricalCsId);
        window.d3.timerFlush(); // 'changeColorDuration' must be to 0
        inspect.restore();
        deepEqual(inspect.output, [ `Unknown categorical color scale: ${categoricalCsId}\n` ]);

        const defaultCategoricalCsId = window.parallelPlot.constructor.CATEGORIAL_CS_IDS[0]; // "Category10"
        const checkedRowIndex = 0;

        const stroke = window.d3.select(".parallelPlot")
            .selectAll(".foreground path")
            .filter((_r, i) => i === checkedRowIndex)
            .attr("stroke");

        colorAssertion(
            window,
            stroke,
            categoricalRowColor(window, config, defaultCategoricalCsId, checkedRowIndex),
            `The path color corresponding to row ${checkedRowIndex} is not as expected`
        );
    });
}

// ******************
// setHistoVisibility
// ******************

function api_histo_validArray() {
    it("should accept a valid list for histogram", async function () {
        const jsdom = await initMtcarsJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, null, [4, 6, 8], null, null, null, null, null, [0, 1], [0, 1], [3, 4, 5], [1, 2, 3, 4, 5, 6, 7, 8]];
        const config = {
            data: window.mtcars,
            categorical: categorical
        }
        generate(window.parallelPlot, config, []);

        const histoVisibility = categorical.map(_cats => true);
        window.parallelPlot.setHistoVisibility(histoVisibility);

        ["histogram", "histogramSelected"].forEach(histoClass => {
            const histoGroupCount = categorical.reduce(
                (accumulator, curCategorical) => accumulator + (curCategorical === null ? 1 : curCategorical.length),
                -1
            );
            
            equal(
                window.d3.select(window.document).selectAll(`g.${histoClass}`).size(),
                histoGroupCount,
                `The number of '${histoClass}' groups is not as expected`
            );
            
            const histoCatRectCount = categorical.reduce(
                (accumulator, curCategorical) => accumulator + (curCategorical === null ? 0 : curCategorical.length),
                0
            );
              
            equal(
                window.d3.select(window.document).selectAll(`g.category .${histoClass} rect`).size(),
                histoCatRectCount,
                `The number of '${histoClass}' rects for categoricals is not as expected`
            );
        });
    });
}

// **********
// setCutoffs
// **********

function api_cutoffs_validArray() {
    it("should accept a valid list for cutoffs", async function () {
        const jsdom = await initIrisJsdom();
        const window = jsdom.window;
        window.eval(`
            parallelPlot = new ParallelPlot("myPP", 1000, 500);
        `);

        const categorical = [null, null, null, null, ["Virginica", "Versicolor", "Setosa"]];
        const config = {
            data: window.iris,
            categorical: categorical
        }
        generate(window.parallelPlot, config, []);

        const cutoffs = [[[6, 7]], null, null, null, ["Virginica", "Setosa"]]
        window.parallelPlot.setCutoffs(cutoffs);

        equal(
            window.d3.select(window.document).selectAll("g.foreground path").size(),
            window.iris.length,
            "The number of 'foreground' paths is not equal to 'iris' row count"
        );
        
        const keptCount = window.iris.filter(row => 
            row["sepal.length"] <= 7 && 
            row["sepal.length"] >= 6 &&
            // eslint-disable-next-line dot-notation
            ["Virginica", "Setosa"].includes(row["variety"])
        ).length;

        const invisibleCount = window.d3.select(window.document).selectAll("g.foreground path")
            .filter(function() { return window.d3.select(this).style("display") === "none"; })
            .size();

        equal(
            invisibleCount,
            window.iris.length - keptCount,
            "The number of invisible 'foreground' paths is not as expected"
        );
    });
}

// ****************
// common functions
// ****************

function generate(parallelPlot, config, expectedOutput) {
    const inspect = stderr.inspect();
    parallelPlot.generate(config);
    inspect.restore();
    deepEqual(inspect.output, expectedOutput);
}

function colorAssertion(window, actual, expected, message) {
    equal(
        window.d3.color(actual).formatHex(),
        window.d3.color(expected).formatHex(),
        message
    );
}