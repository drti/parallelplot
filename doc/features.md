# Features

## Mouse interactions

- Hovering over a line => Highlight and display information

![`hovering`](./snapshots/hovering.gif)

- single click on a column header => Assign a color to each line depending on its position on the column

![`coloring`](./snapshots/coloring.gif)

- double click on a column header => Reverse the orientation of the axis (if continuous variable)
- horizontal drag and drop of a column header => Change the order in which columns are displayed
- vertical drag and drop of a category => Change the order in which categories are displayed

![`ordering`](./snapshots/ordering.gif)

- single click on a category => Filter (or draw) the lines that go through this category
- single click between two categories => Remove filtering (or drawing) for all categories

![`filtering-cat-values`](./snapshots/filtering-cat-values.gif)

- vertical drag and drop along an axis => Define an interval outside of which lines must be filtered
- single click on an axis => Remove all filtering intervals

![`filtering-cont-values`](./snapshots/filtering-cont-values.gif)

- drag and drop along the top slider => Define a range, specifying which columns interval is visible (the columns used for coloring or filtering remain visible)

![`slider`](./snapshots/slider.gif)

## Basic usage (dataset uses `factor` type)

``` R
library(parallelPlot)
parallelPlot(iris)
# 'species' column is of factor type and has box representation for its categories
```

![basic usage (with factor)](./snapshots/basic-with-factor.png)

## `refColumnDim` argument (referenced column is categorical)

``` R
library(parallelPlot)
parallelPlot(iris, refColumnDim = "Species")
# Each trace has a color depending of its 'species' value
```

![`refColumnDim` with categorical column](./snapshots/refColumnDim-Cat.png)

## `categoricalCS` argument

``` R
library(parallelPlot)
parallelPlot(iris, refColumnDim = "Species", categoricalCS = "Set1")
# Colors used for categories are not the same as previously (supported values: Category10, Accent, Dark2, Paired, Set1)
```

![`categoricalCS`](./snapshots/categoricalCS.png)

## `refColumnDim` argument (referenced column is continuous)

``` R
library(parallelPlot)
parallelPlot(iris, refColumnDim = "Sepal.Length")
# Each trace has a color depending of its 'Sepal.Length' value
```

![`refColumnDim` with continuous column](./snapshots/refColumnDim-Cont.png)

## `continuousCS` argument

``` R
library(parallelPlot)
parallelPlot(iris, refColumnDim = "Sepal.Length", continuousCS = "YlOrRd")
# Colors used for points are not the same as previously (supported values: `Viridis`, `Inferno`, `Magma`, `Plasma`, `Warm`, `Cool`, `Rainbow`, `CubehelixDefault`, `Blues`,`Greens`, `Greys`, `Oranges`, `Purples`, `Reds`, `BuGn`, `BuPu`, `GnBu`, `OrRd`, `PuBuGn`,`PuBu`, `PuRd`, `RdBu`, `RdPu`, `YlGnBu`, `YlGn`, `YlOrBr`, `YlOrRd`)
```

![`continuousCS`](./snapshots/continuousCS.png)

## Basic usage (dataset doesn't use `factor` type)

``` R
library(parallelPlot)
parallelPlot(mtcars)
# Several columns are of numerical type but should be of factor type (for example `cyl`)
```

![basic usage (without factor)](./snapshots/basic-without-factor.png)

## `categorical` argument

``` R
library(parallelPlot)
categorical <- list(cyl = c(4, 6, 8), vs = c(0, 1), am = c(0, 1), gear = 3:5, carb = 1:8)
parallelPlot(mtcars, categorical = categorical, refColumnDim = "cyl")
# `cyl` and four last columns have a box representation for categories
```

![`categorical`](./snapshots/categorical.png)

## `categoriesRep` argument

``` R
library(parallelPlot)
categorical <- list(cyl = c(4, 6, 8), vs = c(0, 1), am = c(0, 1), gear = 3:5, carb = 1:8)
parallelPlot(mtcars, categorical = categorical, refColumnDim = "cyl", categoriesRep = "EquallySizedBoxes")
# Within a category column, the height assigned to each category can either be equal for each category (`EquallySizedBoxes`) or calculated to reflect the proportion of lines passing through each category (`EquallySpacedLines`).
```

![`categoriesRep` with `EquallySizedBoxes` value](./snapshots/categoriesRep-EquallySizedBoxes.png)

## `arrangeMethod` argument

``` R
library(parallelPlot)
categorical <- list(cyl = c(4, 6, 8), vs = c(0, 1), am = c(0, 1), gear = 3:5, carb = 1:8)
parallelPlot(mtcars, categorical = categorical, refColumnDim = "cyl", arrangeMethod = "fromLeft")
# Within a category box, the position of lines is calculated to minimize crossings on the left of the box. `arrangeMethod` can also be set to `fromRight` to minimize crossings on the right of the box (default behavior). `fromBoth` allows to merge the two behaviors (see next example). To turn this ordering off (for example for performance reasons), `arrangeMethod` can also be set to `fromNone`.
```

![`arrangeMethod` with 'fromLeft' value](./snapshots/arrangeMethod-fromLeft.png)

## `arrangeMethod` argument (using `fromBoth`)

``` R
library(parallelPlot)
categorical <- list(cyl = c(4, 6, 8), vs = c(0, 1), am = c(0, 1), gear = 3:5, carb = 1:8)
parallelPlot(mtcars, categorical = categorical, refColumnDim = "cyl", arrangeMethod = "fromBoth")
# Within a category box, lines have an incoming point and an outgoing point; these points are ordered to minimize crossings on the left and on the right of the box.
```

![`arrangeMethod` with 'fromBoth' value](./snapshots/arrangeMethod-fromBoth.png)

## `inputColumns` argument

``` R
library(parallelPlot)
categorical <- list(cyl = c(4, 6, 8), vs = c(0, 1), am = c(0, 1), gear = 3:5, carb = 1:8)
inputColumns <- c("mpg", "disp", "drat", "qsec", "am", "gear", "carb")
parallelPlot(mtcars, categorical = categorical, inputColumns = inputColumns, refColumnDim = "cyl")
# The column name is blue for outputs and green for inputs (in shiny mode, inputs can be edited)
```

![`inputColumns`](./snapshots/inputColumns.png)

## `histoVisibility` argument

``` R
library(parallelPlot)
histoVisibility <- rep(TRUE, ncol(iris))
parallelPlot(iris, histoVisibility = histoVisibility)
# An histogram is displayed for each column
```

![`histoVisibility`](./snapshots/histoVisibility.png)

## `invertedAxes` argument

``` R
library(parallelPlot)
invertedAxes <- c("Sepal.Width")
parallelPlot(iris, invertedAxes = invertedAxes)
# Axe of second column is inverted (a sign '↓' is added at the beginning of the column header)
```

![`invertedAxes`](./snapshots/invertedAxes.png)

## `cutoffs` argument

``` R
library(parallelPlot)
histoVisibility <- names(iris) # same as `rep(TRUE, ncol(iris))`
cutoffs <- list(Sepal.Length = list(c(6, 7)), Species = c("virginica", "setosa"))
parallelPlot(iris, histoVisibility = histoVisibility, cutoffs = cutoffs)
# Lines which are not kept by cutoffs are shaded; an histogram for each column is displayed considering only kept lines
```

![`cutoffs`](./snapshots/cutoffs.png)

## `refRowIndex` argument

``` R
library(parallelPlot)
parallelPlot(iris, refRowIndex = 1)
# Axes are shifted vertically in such a way that first trace of the dataset looks horizontal
```

![`refRowIndex`](./snapshots/refRowIndex.png)

## `rotateTitle` argument

``` R
library(parallelPlot)
parallelPlot(iris, refColumnDim = "Species", rotateTitle = TRUE)
# Column names are rotated (can be useful for long column names)
```

![`rotateTitle`](./snapshots/rotateTitle.png)

## `columnLabels` argument

``` R
library(parallelPlot)
columnLabels <- gsub("\\.", "<br>", colnames(iris))
parallelPlot(iris, refColumnDim = "Species", columnLabels = columnLabels)
# Given names are displayed in place of column names found in dataset; <br> is used to insert line breaks
```

![`columnLabels`](./snapshots/columnLabels.png)

## `cssRules` argument

``` R
library(parallelPlot)
parallelPlot(iris, cssRules = list(
    "svg" = "background: #C2C2C2", # Set background of plot to grey
    ".axisLabel" = c("fill: red", "font-size: 1.8em"), # Set title of axes red and greater
    ".tick text" = "font-size: 1.8em", # Set text of axes ticks greater
    ".plotGroup path" = "opacity: 0.25", # Make lines less opaque
    ".xValue" = "color: orange", # Set color for x values in tooltip
    ".xName" = "display: table" # Trick to have x values on a new line
))
```

![`cssRules`](./snapshots/cssRules.png)

Apply CSS to the plot. CSS is a simple way to describe how elements on a web page should be displayed (position, color, size, etc.). 
See the page [Styling 'parallelPlot'](styling-parallelPlot.md) for more details

## `sliderPosition` argument

``` R
library(parallelPlot)
parallelPlot(iris, sliderPosition = list(
  dimCount = 3, # Number of columns to show
  startingDimIndex = 2 # Index of first shown column
))
```

Visible columns starts at second column and three columns are represented.

![`sliderPosition`](./snapshots/sliderPosition.png)

