# Installation of developpement tools

## `node.js` (>= v20.9.0)
- retrieve the `node.js` installer and run it (see instructions in web site <https://nodejs.org>) => will also allow to use `npm` , the package manager of `node.js`;
- in a command terminal, `node -v` must return the version (for example "v20.9.0").

Optionally, you have to configure `npm` to indicate the proxies via the commands:
- `npm config set proxy http://irproxy:8082`
- `npm config set https-proxy http://irproxy:8082`