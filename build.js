import { build } from "esbuild";

build({
  entryPoints: ["src/Typescript/ppIndex.ts"],
  bundle: true,
  minify: false,
  outfile: "dist/pp.esm.js",
  platform: "neutral",
  format: "esm",
});

build({
  entryPoints: ["src/Typescript/ppIndex.ts"],
  bundle: true,
  minify: false,
  outfile: "dist/pp.cjs.js",
  platform: "neutral",
  format: "cjs",
});

build({
  entryPoints: ["src/Typescript/ppIndex.ts"],
  bundle: true,
  minify: false,
  outfile: "dist/pp.iife.js",
  platform: "browser",
  format: "iife"
});

build({
  entryPoints: ["src/Typescript/ppHtmlwidget.ts"],
  bundle: true,
  minify: false,
  external: ["Shiny", "HTMLWidgets"],
  outfile: "htmlwidget/inst/htmlwidgets/parallelPlot.js",
  platform: "browser",
  format: "iife"
});