import * as d3 from "d3";
import { Column } from "./column";
import { CatCutoff, ContCutoff, ParallelPlot } from "./parallelPlot";

    export class Categorical {
        static readonly TOTAL_SPACE_BETWEEN = 30;
        
        column: Column;

        categories: (number | string)[];

        histoScale: d3.ScaleLinear<number, number>;
        
        /** for each category, how many rows are to spread */
        rowCountByCat: number[] = [];

        /** A d3 stack generator used to compute 'boundsByCat' */
        stackGenerator: d3.Stack<number, number[], number>;

        /** for each category, an array with lower and upper bounds of its 'rect' (scaled values in axis range) */
        boundsByCat: number[][] = [];

        /** 
         * Y-offset to apply for each trace point of this column when 'fromNone' arrange method is used. 
         */
        fromNoneOffsets: null | number[] = null;

        /** Y-offset to apply for each trace point of this column when 'fromLeft' arrange method is used. */
        fromLeftOffsets: null | number[] = null;

        /** 
         * Y-offset to apply:
         * - for each trace point of this column when 'fromRight' arrange method is used;
         * - for each outgoing trace point of this column when 'fromBoth' arrange method is used.
         */
        fromRightOffsets: null | number[] = null;

        /** 
         * Y-offset to apply for each incoming trace point of this column when 'fromBoth' arrange method is used. 
         */
        fromBothInOffsets: null | number[] = null;

        dragCat: string | number | null = null;

        initialDragCategories: (number | string)[] | null = null;

        keptCatIndexes: Set<number> | null = null;

        initDone: boolean;

        constructor(column: Column, categories: (number | string)[]) {
            this.column = column;
            this.categories = categories;
            this.histoScale = d3.scaleLinear();
            this.stackGenerator = d3.stack<number, number[], number>()
                .keys(d3.range(this.categories.length))
                .value((d, key) => {
                    return d[key];
                });
            this.initDone = false;
        }

        private checkInitDone() {
            if (!this.initDone) {
                this.initRowCountByCat();
                this.initBoundsByCat();
                this.initDone = true;
            }
        }

        public unvalidateInit() {
            this.fromNoneOffsets = null;
            this.fromLeftOffsets = null;
            this.fromRightOffsets = null;
            this.fromBothInOffsets = null;
            this.initDone = false;
        }

        private initRowCountByCat() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            this.rowCountByCat = this.categories.map(() => 0) as number[];
            parallelPlot.sampleData.forEach(row => {
                const rowCount = this.rowCountByCat[row[columnDim]];
                if (typeof rowCount !== "undefined") {
                    this.rowCountByCat[row[columnDim]] = rowCount + 1;
                }
            });
        }

        // eslint-disable-next-line max-lines-per-function
        private buildOffsetsFor(arrangeMethod: string) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            let orderedRowIndexes;
            if (arrangeMethod === ParallelPlot.ARRANGE_FROM_LEFT) {
                orderedRowIndexes = this.buildFromLeftRowIndexes();
            }
            else if (arrangeMethod === ParallelPlot.ARRANGE_FROM_RIGHT) {
                orderedRowIndexes = this.buildFromRightRowIndexes();
            }
            else if (arrangeMethod === ParallelPlot.ARRANGE_FROM_BOTH) {
                orderedRowIndexes = this.buildFromLeftRowIndexes(true);
            }
            else {
                orderedRowIndexes = d3.range(parallelPlot.sampleData.length);
            }

            const rowPositionByCat = this.categories.map(() => 0) as number[];
            // rowPositionsByRow: for each row, what is its position in the category box
            let rowPositionsByRow: number[] = [];
            orderedRowIndexes.forEach(orderedRowIndex => {
                const row = parallelPlot.sampleData[orderedRowIndex];
                const rowPosition = rowPositionByCat[row[columnDim]];
                if (typeof rowPosition === "undefined") {
                    rowPositionsByRow.push(NaN);
                }
                else {
                    rowPositionByCat[row[columnDim]] = rowPosition + 1;
                    rowPositionsByRow.push(rowPosition);
                }
            });

            const permutedPositions = new Array(rowPositionsByRow.length);
            orderedRowIndexes.forEach((orderedRowIndex, i) => {
                permutedPositions[orderedRowIndex] = rowPositionsByRow[i];
            });
            rowPositionsByRow = permutedPositions;

            const offsets: number[] = [];
            parallelPlot.sampleData.forEach((row, i) => {
                const rowCount = this.rowCountByCat[row[columnDim]];
                if (typeof rowCount === "undefined") {
                    offsets.push(0);
                }
                else {
                    let spreaderScale = parallelPlot.catSpreaderMap.get(rowCount);
                    if (typeof spreaderScale === "undefined") {
                        spreaderScale = d3.scalePoint<number>()
                            .domain(d3.range(rowCount)) // costly => introduce 'catSpreaderMap'
                            .padding(0.8);
                        parallelPlot.catSpreaderMap.set(rowCount, spreaderScale);
                    }
                    const bounds = this.boundsByCat[row[columnDim]];
                    const halfLength = Math.abs(bounds[1] - bounds[0]) / 2;
                    spreaderScale.range([-halfLength, halfLength]);
        
                    const offset = spreaderScale(rowPositionsByRow[i]);
                    if (typeof offset === "undefined") {
                        offsets.push(0);
                    }
                    else {
                        offsets.push(offset);
                    }
                }
            });
            return offsets;
        }

        private getFromNoneOffsets() {
            if (this.fromNoneOffsets === null) {
                this.fromNoneOffsets = this.buildOffsetsFor(ParallelPlot.ARRANGE_FROM_NONE);
            }
            return this.fromNoneOffsets as number[];
        }

        private getFromLeftOffsets() {
            if (this.fromLeftOffsets === null) {
                this.fromLeftOffsets = this.buildOffsetsFor(ParallelPlot.ARRANGE_FROM_LEFT);
            }
            return this.fromLeftOffsets as number[];
        }

        private getFromRightOffsets() {
            if (this.fromRightOffsets === null) {
                this.fromRightOffsets = this.buildOffsetsFor(ParallelPlot.ARRANGE_FROM_RIGHT);
            }
            return this.fromRightOffsets as number[];
        }

        private getFromBothInOffsets() {
            if (this.fromBothInOffsets === null) {
                this.fromBothInOffsets = this.buildOffsetsFor(ParallelPlot.ARRANGE_FROM_BOTH);
            }
            return this.fromBothInOffsets as number[];
        }

        private inOffsets() {
            const arrangeMethod = this.column.parallelPlot.arrangeMethod;
            if (arrangeMethod === ParallelPlot.ARRANGE_FROM_LEFT) {
                return this.getFromLeftOffsets();
            }
            if (arrangeMethod === ParallelPlot.ARRANGE_FROM_RIGHT) {
                return this.getFromRightOffsets();
            }
            if (arrangeMethod === ParallelPlot.ARRANGE_FROM_NONE) {
                return this.getFromNoneOffsets();
            }
            return this.getFromBothInOffsets();
        }

        private outOffsets() {
            const arrangeMethod = this.column.parallelPlot.arrangeMethod;
            if (arrangeMethod === ParallelPlot.ARRANGE_FROM_BOTH) {
                return this.getFromRightOffsets();
            }
            return this.inOffsets();
        }

        private buildFromLeftRowIndexes(bothMethod?: boolean) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            const columnDimIndex = parallelPlot.dimensions.indexOf(columnDim);

            const fromLeftRowIndexes = d3.range(parallelPlot.sampleData.length);
            fromLeftRowIndexes.sort((rowIndex1, rowIndex2) => {
                let diff = 0;
                let usedDimIndex = columnDimIndex - 1;
                let usedColumnName = "";
                while (diff === 0 && usedDimIndex >= 0) {
                    usedColumnName = parallelPlot.dimensions[usedDimIndex];
                    diff = parallelPlot.sampleData[rowIndex2][usedColumnName] - parallelPlot.sampleData[rowIndex1][usedColumnName];
                    const usedColumn = parallelPlot.columns[usedColumnName];
                    if (bothMethod && diff === 0 && usedColumn.categorical) {
                        const previousOutOffsets = usedColumn.categorical.outOffsets();
                        diff = previousOutOffsets[rowIndex1] - previousOutOffsets[rowIndex2];
                    }
                    usedDimIndex = usedDimIndex - 1;
                }

                if (diff === 0) {
                    usedDimIndex = columnDimIndex + 1;
                    while (diff === 0 && usedDimIndex <= parallelPlot.dimensions.length - 1) {
                        usedColumnName = parallelPlot.dimensions[usedDimIndex];
                        diff = parallelPlot.sampleData[rowIndex2][usedColumnName] - parallelPlot.sampleData[rowIndex1][usedColumnName];
                        usedDimIndex = usedDimIndex + 1;
                    }
                }

                if (diff !== 0) {
                    const column = parallelPlot.columns[usedColumnName];
                    if (column.continuous && column.continuous.invertedAxe) {
                        diff = -diff;
                    }
                }

                return (diff === 0) ? rowIndex1 - rowIndex2 : diff;
            });
            return fromLeftRowIndexes;
        }

        private buildFromRightRowIndexes() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            const columnDimIndex = parallelPlot.dimensions.indexOf(columnDim);

            const fromRightRowIndexes = d3.range(parallelPlot.sampleData.length);
            fromRightRowIndexes.sort((rowIndex1, rowIndex2) => {
                let diff = 0;
                let usedDimIndex = columnDimIndex + 1;
                let usedColumnName = "";
                while (diff === 0 && usedDimIndex <= parallelPlot.dimensions.length - 1) {
                    usedColumnName = parallelPlot.dimensions[usedDimIndex];
                    diff = parallelPlot.sampleData[rowIndex2][usedColumnName] - parallelPlot.sampleData[rowIndex1][usedColumnName];
                    usedDimIndex = usedDimIndex + 1;
                }

                if (diff === 0) {
                    usedDimIndex = columnDimIndex - 1;
                    while (diff === 0 && usedDimIndex >= 0) {
                        usedColumnName = parallelPlot.dimensions[usedDimIndex];
                        diff = parallelPlot.sampleData[rowIndex2][usedColumnName] - parallelPlot.sampleData[rowIndex1][usedColumnName];
                        usedDimIndex = usedDimIndex - 1;
                    }
                }

                if (diff !== 0) {
                    const column = parallelPlot.columns[usedColumnName];
                    if (column.continuous && column.continuous.invertedAxe) {
                        diff = -diff;
                    }
                }

                return (diff === 0) ? rowIndex1 - rowIndex2 : diff;
            });
            return fromRightRowIndexes;
        }

        private initBoundsByCat() {
            const parallelPlot = this.column.parallelPlot;
            const yScale = d3.scaleLinear()
                .domain([0, parallelPlot.sampleData.length])
                .range([parallelPlot.axeHeight - Categorical.TOTAL_SPACE_BETWEEN, 0]);


            let visibleCatCount;
            if (parallelPlot.catEquallySpacedLines) {
                this.boundsByCat = this.stackGenerator([this.rowCountByCat])
                    .map(series => series[0].map(yScale));
                visibleCatCount = this.rowCountByCat.filter(rowCount => rowCount !== 0).length;
            }
            else {
                const sameCountByCat = [this.rowCountByCat.map(() => parallelPlot.sampleData.length / this.rowCountByCat.length)];
                this.boundsByCat = this.stackGenerator(sameCountByCat)
                    .map(series => series[0].map(yScale));
                visibleCatCount = this.categories.length;
            }

            if (visibleCatCount > 1) {
                const spaceBetween = Categorical.TOTAL_SPACE_BETWEEN / (visibleCatCount - 1);
                let cumulatedSpaceBetween = spaceBetween;
                for (let i = this.boundsByCat.length - 2; i >= 0; i--) {
                    this.boundsByCat[i][0] += cumulatedSpaceBetween;
                    this.boundsByCat[i][1] += cumulatedSpaceBetween;
                    if (!parallelPlot.catEquallySpacedLines || this.rowCountByCat[i] !== 0) {
                        cumulatedSpaceBetween += spaceBetween;
                    }
                }
            }
        }
        
        private yValue(catIndex: number | { valueOf(): number }): number {
            this.checkInitDone();
            const bounds = this.boundsByCat[catIndex.valueOf()];
            return (bounds[0] + bounds[1]) / 2;
        }

        public invertYValue(yValue: number): number {
            this.checkInitDone();

            for (let i = 0; i < this.boundsByCat.length; i++) {
                if (
                    this.boundsByCat[i][0] < this.boundsByCat[i][1]
                    && this.boundsByCat[i][0] < yValue
                    && yValue < this.boundsByCat[i][1]
                ) {
                    return i;
                }
                if (
                    this.boundsByCat[i][1] < this.boundsByCat[i][0] 
                    && this.boundsByCat[i][1] < yValue
                    && yValue < this.boundsByCat[i][0]
                ) {
                    return i;
                }
            }
            return -1;
        }

        public yTraceValueIn(rowIndex: number): number {
            this.checkInitDone();
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const value = parallelPlot.sampleData[rowIndex][columnDim];
            return this.yValue(value) + this.inOffsets()[rowIndex];
        }

        public yTraceValueOut(rowIndex: number): number {
            this.checkInitDone();
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const value = parallelPlot.sampleData[rowIndex][columnDim];
            return this.yValue(value) + this.outOffsets()[rowIndex];
        }

        public format(value: number) {
            if (value >= 0 && value < this.categories.length) {
                return Number.isInteger(value.valueOf()) ? this.categories[value.valueOf()].toString() : "";
            }
            console.warn(value, " is not valid, it should be between 0 and ", this.categories.length);
            return "";
        }

        public catWithoutTraces() {
            return this.categories.filter((_cat, i) => this.rowCountByCat[i] === 0);
        }

        // eslint-disable-next-line max-lines-per-function
        public refreshBoxesRep() {
            this.initBoundsByCat();

            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);

            categoricalGroup
                .selectAll(".category")
                .style("display", (_cat, i) => {
                    return parallelPlot.catEquallySpacedLines && this.rowCountByCat[i] === 0
                        ? "none"
                        : null;
                })
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", (_cat, i) => {
                    return "translate(0," + this.yValue(i) + ")";
                });
                
            categoricalGroup
                .selectAll(".category .box")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("y", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return -Math.abs(bounds[1] - bounds[0]) / 2;
                })
                .attr("height", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return Math.abs(bounds[1] - bounds[0]);
                });

            categoricalGroup
                .selectAll(".category .barMainHisto")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("y", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return -Math.abs(bounds[1] - bounds[0]) / 4;
                })
                .attr("height", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return Math.abs(bounds[1] - bounds[0]) / 2;
                });

            categoricalGroup
                .selectAll(".category .barSecondHisto")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("y", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return -Math.abs(bounds[1] - bounds[0]) / 4;
                })
                .attr("height", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return Math.abs(bounds[1] - bounds[0]) / 2;
                });
        }

        // eslint-disable-next-line max-lines-per-function
        public drawCategories() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);

            categoricalGroup.append("rect")
                .attr("class", "catOverlay")
                .attr("x", -ParallelPlot.catClusterWidth)
                .attr("height", parallelPlot.axeHeight)
                .attr("width", 2 * ParallelPlot.catClusterWidth)
                .attr("opacity", 0)
                .attr("cursor", "crosshair")
                .on("click", () => {
                    this.toggleCategories();
                    this.applyCategoricalCutoffs();
                    parallelPlot.applyColumnCutoffs(columnDim, false);
                })

            categoricalGroup
                .selectAll(".category")
                .data(this.categories)
                .join(
                    // eslint-disable-next-line max-lines-per-function
                    enter => { 
                        const catGroup = enter.append("g")
                            .attr("class", "category tick")
                            .attr("transform", (_cat, i) => {
                                return "translate(0," + this.yValue(i) + ")";
                            })
                            .style("display", (_cat, i) => {
                                return parallelPlot.catEquallySpacedLines && this.rowCountByCat[i] === 0
                                    ? "none"
                                    : null;
                            });
                        catGroup.append("rect")
                            .attr("class", "box")
                            .classed("active", (_cat, i) => {
                                return this.isKept(i);
                            })
                            .classed("inactive", (_cat, i) => {
                                return !this.isKept(i);
                            })
                            .attr("x", -ParallelPlot.catClusterWidth / 2)
                            .attr("y", (_cat, i) => {
                                const bounds = this.boundsByCat[i];
                                return -Math.abs(bounds[1] - bounds[0]) / 2;
                            })
                            .attr("height", (_cat, i) => {
                                const bounds = this.boundsByCat[i];
                                return Math.abs(bounds[1] - bounds[0]);
                            })
                            .attr("width", ParallelPlot.catClusterWidth)
                            .attr("fill-opacity", 0.3)
                            .call(d3.drag<SVGRectElement, string | number>()
                                // Click Distance : 5 pixel arround .. to begin a drag .. or to have a "click" if under
                                .clickDistance(5)
                                // @ts-expect-error: 'this.parentNode' can be null => generate an error
                                .container(function() { return this.parentNode.parentNode; })
                                .on("start", (_event, cat: string | number) => {
                                    this.dragStart(cat);
                                })
                                .on("drag", (event) => {
                                    this.drag(event.y);
                                })
                                .on("end", () => {
                                    this.dragEnd();
                                })
                                )
                            // "onclick" must be called after 'drag' ... if not the click event is removed
                            .on("click", (_event, cat) => {
                                const catIndex = this.categories.indexOf(cat) as number;
                                this.toggleCategory(catIndex);
                                this.applyCategoricalCutoffs();
                                parallelPlot.applyColumnCutoffs(columnDim, false);
                            })
                            .attr("shape-rendering", "crispEdges");
                        catGroup.append("text")
                            .text((_cat, i) => {
                                return this.categories[i].toString();
                            })
                            .attr("text-anchor", "end")
                            .attr("x", -ParallelPlot.catClusterWidth);
                        return catGroup;
                    },
                    update => update,
                    exit => exit.remove()
                )
                .select("rect")
                .attr("fill", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return this.column.dim === parallelPlot.refColumnDim
                        ? parallelPlot.valueColor(catIndex)
                        : "black";
                });
        }

        public dragStart(draggedCat: string | number) {
            this.dragCat = draggedCat;
            this.initialDragCategories = this.categories.slice();
        }

        public drag(y: number) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            let position: number = y;
            const dragCat = this.dragCat as (string | number);

            const indexInitialPosition = this.categories.indexOf(dragCat);

            if (indexInitialPosition > 0) {
                const bottomCat = this.categories[indexInitialPosition - 1];
                const bottomY = this.yValue(indexInitialPosition - 1);

                if (bottomY && position > bottomY) {
                    if (this.canSwitchDimension(bottomCat, dragCat)) {
                        this.switchDimension(bottomCat, dragCat);
                    }
                    else {
                        position = bottomY;
                    }
                }
            }

            if (indexInitialPosition < this.categories.length - 1) {
                const topCat = this.categories[indexInitialPosition + 1];
                const topY = this.yValue(indexInitialPosition + 1);

                if (topY && position < topY) {
                    if (this.canSwitchDimension(dragCat, topCat)) {
                        this.switchDimension(dragCat, topCat);
                    } else {
                        position = topY;
                    }
                }
            }

            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);

            categoricalGroup.selectAll(".category")
                .filter(cat => cat === this.dragCat)
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", function() {
                    return "translate(0," + position + ")";
                });
        }

        private canSwitchDimension(cat1: string | number, cat2: string | number): boolean {
            const indexCat1 = this.categories.indexOf(cat1);
            const indexCat2 = this.categories.indexOf(cat2);

            if (indexCat1 === null || indexCat2 === null) {
                return false;
            }

            if ((indexCat1 as number) + 1 !== (indexCat2 as number)) {
                return false;
            }

            return true;
        }

        private switchDimension(bottomCat: string | number, topCat: string | number) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const bottomCatIndex = this.categories.indexOf(bottomCat);
            const topCatIndex = this.categories.indexOf(topCat);

            if (
                bottomCatIndex === -1 ||
                topCatIndex === -1
            ) {
                return;
            }

            if (bottomCatIndex + 1 !== topCatIndex) {
                return;
            }

            const beforeDragCategories = this.categories.slice();
            this.categories[bottomCatIndex] = topCat;
            this.categories[topCatIndex] = bottomCat;

            // update 'this.rowCountByCat' and 'this.boundsByCat'
            const oldCatMapping = this.categories.map(cat => beforeDragCategories!.indexOf(cat));
            this.rowCountByCat = oldCatMapping.map(oldCatIndex => this.rowCountByCat[oldCatIndex]);
            this.initBoundsByCat();

            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);

            categoricalGroup.selectAll<SVGRectElement, string | number>(".category")
                .filter(cat =>
                    (cat === topCat || cat === bottomCat) &&
                    cat !== this.dragCat
                )
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return "translate(0," + this.yValue(catIndex) + ")";
                });
        }

        public dragEnd() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);

            categoricalGroup.selectAll<SVGRectElement, string | number>(".category")
                .filter(cat => cat === this.dragCat)
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return "translate(0," + this.yValue(catIndex) + ")";
                });

            if (this.initialDragCategories) {
                // update 'parallelPlot.sampleData'
                const newCatMapping = this.initialDragCategories.map(cat => this.categories.indexOf(cat));
                parallelPlot.sampleData.forEach(function(row) {
                    const catValue = row[columnDim];
                    const newCatValue = newCatMapping[catValue];
                    row[columnDim] = isNaN(newCatValue) ? NaN : newCatValue;
                });

                // update 'this.keptCatIndexes'
                if (this.keptCatIndexes !== null) {
                    this.keptCatIndexes = new Set(
                        [...this.keptCatIndexes].map(catValue => newCatMapping[catValue])
                    );
                }

                // update traces
                parallelPlot.refreshTracesPaths();
            }

            this.dragCat = null;
        }

        public applyCategoricalCutoffs() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            const categoricalGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim);
            categoricalGroup.selectAll<SVGRectElement, string | number>(".category rect")
                .classed("active", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return this.isKept(catIndex);
                })
                .classed("inactive", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return !this.isKept(catIndex);
                });
        }

        private selectedRowCountByCat(
            selected: Record<string, number>[]
        ) {
            const columnDim = this.column.dim;
            const rowCountByCat = this.categories.map(() => 0) as number[];
            selected.forEach(row => {
                const rowCount = rowCountByCat[row[columnDim]];
                if (typeof rowCount !== "undefined") {
                    rowCountByCat[row[columnDim]] = rowCount + 1;
                }
            });
            return rowCountByCat;
        }

        // eslint-disable-next-line max-lines-per-function
        public drawMainHistogram() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const categoryGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim)
                .selectAll<SVGGElement, string | number>(".category");

            categoryGroup.select(".histogram").remove();
        
            if (!this.column.histoVisible) {
                return;
            }
        
            const histogramGroup = categoryGroup.append("g")
                .attr("class", "histogram")
                .attr("opacity", "0.5")
                .style("display", function() {
                    return parallelPlot.selectedRows.size > parallelPlot.sampleData.length / 10.0
                        ? null
                        : "none";
                });

            const columnWidth = parallelPlot.xScaleVisibleDimension(parallelPlot.visibleDimensions[0]);
            if (typeof columnWidth === "undefined") {
                console.error("Dim '", parallelPlot.visibleDimensions[0], "' not found");
                return;
            }
            const maxBinLength = d3.max(this.rowCountByCat);
            if (typeof maxBinLength === "undefined") {
                console.error("maxBinLength not found");
                return;
            }
            // range is 70% percent of the original x() size between 2 dimensions
            this.histoScale
                .range([0, columnWidth * 0.7])
                .domain([0, maxBinLength]);
        
            histogramGroup
                .append("rect")
                .attr("class", "barMainHisto")
                .attr("pointer-events", "none")
                .attr("x", ParallelPlot.catClusterWidth / 2 + 1)
                .attr("y", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return -Math.abs(bounds[1] - bounds[0]) / 4;
                })
                .attr("width", cat => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return this.histoScale(this.rowCountByCat[catIndex]);
                })
                .attr("height", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return Math.abs(bounds[1] - bounds[0]) / 2;
                })
                .style("fill", ParallelPlot.mainHistoColor)
                .style("stroke", "white");
        }
        
        // eslint-disable-next-line max-lines-per-function
        public drawSelectedHistogram(
            selected: Record<string, number>[]
        ) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const categoryGroup = d3.selectAll(parallelPlot.bindto + " .catGroup")
                .filter(dim => dim === columnDim)
                .selectAll<SVGGElement, string | number>(".category");

            categoryGroup.select(".histogramSelected").remove();
        
            if (!this.column.histoVisible) {
                return;
            }
        
            const histogramGroup = categoryGroup.append("g")
                .attr("class", "histogramSelected")
                .attr("opacity", "0.4");

            const selectedRowCountByCat = this.selectedRowCountByCat(selected);
            let selectedHistoScale: d3.ScaleLinear<number, number>;
            if (selected.length > parallelPlot.sampleData.length / 10.0) {
                selectedHistoScale = this.histoScale;
            }
            else {
                const columnWidth = parallelPlot.xScaleVisibleDimension(
                    parallelPlot.visibleDimensions[0]
                );
                if (typeof columnWidth === "undefined") {
                    console.error("Dim '", parallelPlot.visibleDimensions[0], "' not found");
                    return;
                }
                const maxBinLength = d3.max(selectedRowCountByCat);
                if (typeof maxBinLength === "undefined") {
                    console.error("maxBinLength not found");
                    return;
                }
                selectedHistoScale = d3.scaleLinear()
                    .range([0, columnWidth * 0.7])
                    .domain([0, maxBinLength]);
            }

            histogramGroup
                .append("rect")
                .attr("class", "barSecondHisto")
                .attr("pointer-events", "none")
                .attr("x", ParallelPlot.catClusterWidth / 2 + 1)
                .attr("y", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return -Math.abs(bounds[1] - bounds[0]) / 4;
                })
                .attr("height", (_cat, i) => {
                    const bounds = this.boundsByCat[i];
                    return Math.abs(bounds[1] - bounds[0]) / 2;
                })
                .attr("width", (cat) => {
                    const catIndex = this.categories.indexOf(cat) as number;
                    return selectedHistoScale(selectedRowCountByCat[catIndex]);
                })
                .style("fill", ParallelPlot.secondHistoColor)
                .style("stroke", function() {
                    return "white";
                });
        }

        public toggleCategory(catIndex: number) {
            if (this.column.categorical) {
                const categories = this.column.categorical.categories;
                if (this.keptCatIndexes === null) {
                    this.keptCatIndexes = new Set(d3.range(categories.length));
                    this.keptCatIndexes.delete(catIndex);
                }
                else if (this.keptCatIndexes.has(catIndex)) {
                    this.keptCatIndexes.delete(catIndex);
                }
                else {
                    this.keptCatIndexes.add(catIndex);
                    if (this.keptCatIndexes.size === categories.length) {
                        this.keptCatIndexes = null;
                    }
                }
            }
            else {
                console.error("categories is null but 'toggleCategory' is called.");
            }
        }

        public toggleCategories() {
            if (this.column.categorical === null) {
                console.error("categories is null but 'toggleCategories' is called.");
            }
            else {
                if (this.keptCatIndexes === null) {
                    this.keptCatIndexes = new Set();
                }
                else {
                    this.keptCatIndexes = null;
                }
            }
        }

        public getCutoffs(): CatCutoff[] | null {
            const categories = this.categories;
            if (this.keptCatIndexes === null) {
                return null;
            }
            return [...this.keptCatIndexes].map(catIndex => categories[catIndex]);
        }

        public setCutoffs(cutoffs: ContCutoff[] | CatCutoff[] | null | undefined) {
            if (cutoffs) {
                const columnDim = this.column.dim;
                const categories = this.categories;
                if (cutoffs.length !== 0 && typeof cutoffs[0] !== "string" && typeof cutoffs[0] !== "number") {
                    console.error("Wrong categorical cutoffs are provided:", cutoffs);
                }
                else {
                    const catCutoffs = cutoffs as CatCutoff[];
                    const indexes = catCutoffs
                        .map(catCo => {
                            const catIndex = categories.indexOf(catCo);
                            if (catIndex === -1) {
                                console.error(catCo + " is not a category of " + columnDim);
                            }
                            return catIndex;
                        })
                        .filter(index => index !== -1);
                    this.keptCatIndexes = new Set(indexes);
                }
            }
            else {
                this.keptCatIndexes = null;
            }
        }

        public hasFilters() {
            return this.keptCatIndexes !== null;
        }

        public isKept(value: number) {
            if (this.keptCatIndexes !== null) {
                return this.keptCatIndexes.has(value);
            }
            return true;
        }

    }