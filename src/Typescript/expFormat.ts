import * as d3 from "d3";

    export class ExpFormat {

        static readonly NONBREAKING_SPACE = String.fromCharCode(0xA0);

        static readonly EXP_FORMATS: Record<string, string> = {
            "y": "-24",
            "z": "-21",
            "a": "-18",
            "f": "-15",
            "p": "-12",
            "n": "-9",
            "µ": "-6",
            "m": "-3",
            "k": "3",
            "M": "6",
            "G": "9",
            "T": "12",
            "P": "15",
            "E": "18",
            "Z": "21",
            "Y": "24"
        }

        static readonly f2s = d3.format("~s");

        static readonly f3f = d3.format("~r");

        static sToExp(siValue: string) {
            const siStr = /[yzafpnµmkMGTPEZY]/.exec(siValue)
            if (siStr !== null) {
                return siValue.replace(siStr[0], ExpFormat.NONBREAKING_SPACE + "E" + ExpFormat.EXP_FORMATS[siStr[0]]);
            }
            return siValue;
        }

        static format(value: number | { valueOf(): number; }) {
            if (value.valueOf() > 1e3 || value.valueOf() < -1e3 || (value.valueOf() < 1e-3 && value.valueOf() > -1e-3)) {
                return ExpFormat.sToExp(ExpFormat.f2s(value));
            }
            return ExpFormat.f3f(value);
        }

    }