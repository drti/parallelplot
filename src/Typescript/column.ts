import { Categorical } from "./categorical";
import { Continuous } from "./continuous";
import { ExpFormat } from "./expFormat";
import { CatCutoff, ContCutoff, ParallelPlot, Row } from "./parallelPlot";

    export class Column {
        static readonly INPUT: string = "Input";

        static readonly OUTPUT: string = "Output";

        parallelPlot: ParallelPlot;

        colIndex: number;

        dim: string;

        label: string;

        continuous: Continuous | null;

        categorical: Categorical | null;

        histoVisible: boolean;
        
        ioType: string;

        constructor(
            dim: string,
            colIndex: number,
            parallelPlot: ParallelPlot,
            label: string,
            categories: (number | string)[] | null | undefined,
            cutoffs: ContCutoff[] | CatCutoff[] | null | undefined,
            histoVisibility: boolean,
            invertedAxe: boolean,
            ioType: string
        ) {
            this.colIndex = colIndex;
            this.dim = dim;
            this.label = label;
            this.parallelPlot = parallelPlot;

            if (categories) {
                this.continuous = null;
                this.categorical = new Categorical(this, categories);
            }
            else {
                this.categorical = null;
                this.continuous = new Continuous(this, invertedAxe);
            }

            this.histoVisible = histoVisibility;
            this.setCutoffs(cutoffs);
            this.ioType = ioType;
        }

        public unvalid() {
            if (this.continuous) {
                this.continuous.initDone = false;
            }
            if (this.categorical) {
                this.categorical.initDone = false;
            }
        }

        public setInvertedAxe(invertedAxe: boolean) {
            if (this.continuous) {
                return this.continuous.setInvertedAxe(invertedAxe);
            }
            return false;
        }

        public yTraceValue(rowIndex: number): number {
            if (this.continuous) {
                return this.continuous.yTraceValue(rowIndex);
            }
            if (this.categorical) {
                return this.categorical.yTraceValueIn(rowIndex);
            }
            throw new Error("yTraceValue() failed")
        }

        public formatedRowValue(row: Row) {
            return this.formatedValue(row[this.dim]);
        }

        private formatedValue(value: number) {
            if (this.continuous) {
                return ExpFormat.format(value);
            }
            if (this.categorical) {
                return this.categorical.format(value);
            }
            throw new Error("formatedValue() failed")
        }

        public labelText() {
            return this.label.replace(/<br>/gi, " ");
        }

        public isInput(): boolean {
            return this.ioType === Column.INPUT;
        }
        public isOutput(): boolean {
            return this.ioType === Column.OUTPUT;
        }

        public drawMainHistogram() {
            if (this.continuous) {
                this.continuous.drawMainHistogram();
            }
            if (this.categorical) {
                this.categorical.drawMainHistogram();
            }
        }
        
        public drawSelectedHistogram(
            selected: Record<string, number>[]
        ) {
            if (this.continuous) {
                this.continuous.drawSelectedHistogram(selected);
            }
            if (this.categorical) {
                this.categorical.drawSelectedHistogram(selected);
            }
        }

        public getCutoffs(): ContCutoff[] | CatCutoff[] | null {
            if (this.continuous) {
                return this.continuous.getCutoffs();
            }
            if (this.categorical) {
                return this.categorical.getCutoffs();
            }
            throw new Error("getCutoffs() failed")
        }

        public setCutoffs(cutoffs: ContCutoff[] | CatCutoff[] | null | undefined) {
            if (this.categorical) {
                this.categorical.setCutoffs(cutoffs);
            }
            if (this.continuous) {
                this.continuous.setCutoffs(cutoffs);
            }
        }

        public hasFilters() {
            if (this.continuous) {
                return this.continuous.hasFilters();
            }
            if (this.categorical) {
                return this.categorical.hasFilters();
            }
            throw new Error("hasFilters() failed")
        }

        public isKept(value: number) {
            if (this.continuous) {
                return this.continuous.isKept(value);
            }
            if (this.categorical) {
                return this.categorical.isKept(value);
            }
            throw new Error("isKept() failed")
        }
    }