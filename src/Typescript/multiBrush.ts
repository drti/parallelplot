import * as d3 from "d3";
import { BrushSelection, ContCutoff, ParallelPlot } from "./parallelPlot";

export type BrushDef = {
    id: number;
    brush: d3.BrushBehavior<unknown>;
    initialCutoff: ContCutoff | undefined;
};

    export class MultiBrush {
        // Keep the actual d3-brush functions and their IDs in a list
        brushDefList: BrushDef[] = [];

        colIndex: number;

        plot: ParallelPlot;

        dim: string;

        constructor(colIndex: number, plot: ParallelPlot, dim: string) {
            this.colIndex = colIndex;
            this.plot = plot;
            this.dim = dim;

            // Add new empty BrushDef
            this.addNewBrushDef();
            this.applyDataJoin();
        }

        static multiBrushClass(colIndex: number) {
            return "multibrush_col" + colIndex;
        }

        static brushClass(colIndex: number, brushDef: BrushDef) {
            return "brush" + brushDef.id + "_col" + colIndex;
        }

        brushClass(brushDef: BrushDef) {
            return MultiBrush.brushClass(this.colIndex, brushDef);
        }

        private addNewBrushDef(initialCutoff?: ContCutoff) {
            const brush = d3.brushY()
                .handleSize(4)
                // Set brushable area
                .extent([
                    [-5, 0],
                    [20, this.plot.axeHeight]
                ])
                // When the brush moves (such as on mousemove), update cutoffs of parallelPlot
                .on("brush", () => { this.updatePlotCutoffs(true); })
                // At the end of a brush gesture (such as on mouseup), update cutoffs of parallelPlot and 'brushDefList'
                .on("end", () => {
                    this.updatePlotCutoffs(false);
                    this.updateBrushDefList();
                });

            const newBrushDef: BrushDef = {
                id: this.brushDefList.length,
                brush: brush,
                initialCutoff: initialCutoff
            };

            this.brushDefList.push(newBrushDef);

            return newBrushDef;
        }

        private applyDataJoin() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisMB = this;

            const brushGroup = d3.select(this.plot.bindto + " ." + MultiBrush.multiBrushClass(this.colIndex))
                .selectAll<SVGGElement, BrushDef>(".brush")
                .data(this.brushDefList, brushDef => brushDef.id.toString());

            // Set up a new BrushBehavior for each entering Brush
            brushGroup.enter().insert("g", ".brush")
                .attr("class", (brushDef: BrushDef) => {
                    return ["brush", this.brushClass(brushDef)].join(" ");
                })
                .each(function(brushDef: BrushDef) {
                    d3.select(this).call(brushDef.brush);

                    // if entering Brush has an initialCutoff, modify BrushBehavior selection
                    if (brushDef.initialCutoff) {
                        if (!thisMB.plot.columns[thisMB.dim].continuous) {
                            throw new Error("'initialCutoff' failed for:" + thisMB.dim);
                        }
                        const brushSelection = 
                            brushDef.initialCutoff.map(
                                thisMB.plot.columns[thisMB.dim].continuous!.y()
                            )
                            .sort((a, b) => a - b) as [number, number];

                        // Modify the brush selection
                        d3.select(this).call(d3.brushY().move, brushSelection);
                    }
                });

            brushGroup.each(function(brushDef: BrushDef) {
                d3.select(this)
                    .selectAll(".overlay")
                    .style("pointer-events", function() {
                        return (
                            brushDef.id === thisMB.brushDefList.length - 1
                            && brushDef.brush !== undefined
                        ) 
                        ? "all"
                        : "none";
                    })
                    .on("click", function() {
                        thisMB.removeBrushes();
                    });
            });

            brushGroup.exit().remove();
        }

        private removeBrushes() {
            const brushSelections: BrushSelection[] = [];
            this.plot.setContCutoff(brushSelections, this.dim, false);

            // Remove all brushes
            this.brushDefList = [];
            this.applyDataJoin();

            // Add new empty BrushDef
            this.addNewBrushDef();
            this.applyDataJoin();
        }

        initFrom(cutoffs: ContCutoff[] | null) {
            // Remove all Brushes
            this.brushDefList = [];
            this.applyDataJoin();

            if (cutoffs !== null) {
                // Add a new BrushDef for each given cutoffs
                cutoffs.forEach((cutoff: ContCutoff) => {
                    this.addNewBrushDef(cutoff);
                    this.applyDataJoin();
                });
            }

            // Add new empty BrushDef
            this.addNewBrushDef();
            this.applyDataJoin();
        }

        private updatePlotCutoffs(adjusting: boolean) {
            const brushSelections: BrushSelection[] = [];

            this.brushDefList.forEach(brushDef => {
                const brushGroup = d3.select<SVGGElement, BrushDef>(this.plot.bindto + " ." + this.brushClass(brushDef));
                const brushSelection = d3.brushSelection(
                    brushGroup.node() as SVGGElement
                ) as [number, number];

                if (brushSelection !== null) {
                    brushSelections.push(brushSelection.sort((a, b) => a - b));
                }
            });

            this.plot.setContCutoff(brushSelections, this.dim, adjusting);
        }

        private updateBrushDefList() {
            // If our latest brush has a selection, that means we need to add a new empty BrushDef
            const lastBrushDef = this.brushDefList[this.brushDefList.length - 1];
            const lastBrushGroup = 
                d3.select<SVGGElement, BrushDef>(this.plot.bindto + " ." + this.brushClass(lastBrushDef));
            const lastBrushSelection = d3.brushSelection(
                lastBrushGroup.node() as SVGGElement
            );

            if (lastBrushSelection && lastBrushSelection[0] !== lastBrushSelection[1]) {
                this.addNewBrushDef();
            }

            // Always draw brushes
            this.applyDataJoin();
        }
    }