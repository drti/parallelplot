import * as d3 from "d3";
import { ParallelPlot } from "./parallelPlot";

    export class BrushSlider {

        parallelPlot: ParallelPlot;

        dimIndexScale: d3.ScalePoint<number> = d3.scalePoint<number>();

        dimIndexScaleInvertFn: d3.ScaleQuantize<number> = d3.scaleQuantize<number>();

        inSelectionDrag: boolean = false;

        constructor(parallelPlot: ParallelPlot) {
            this.parallelPlot = parallelPlot;
            this.updateDimIndexScale();

            // Create the slider axis
            const axis = d3.select(parallelPlot.bindto + " .slider").append("g")
                .attr("pointer-events", "none")
                .attr("class", "axisGroup")
                // Tick Values set to none to have no overlayed names
                .call(d3.axisBottom(this.dimIndexScale).tickSize(0).tickFormat(() => ""));

            d3.select(parallelPlot.bindto).append("div")
                .attr("class", "sliderTooltip")
                .style("display", "none");

            this.createBrush();

            axis.append("line")
                .attr("class", "locatorLine")
                .attr("x1", 50)
                .attr("y1", -4)
                .attr("x2", 50)
                .attr("y2", 4)
                .style("display", "none")
                .attr("pointer-events", "none");

            // Adapt slider to dimensions
            d3.select<SVGGElement, string>(parallelPlot.bindto + " .brushDim").call(
                d3.brushX<string>().move, 
                [
                    this.dimIndexScale(this.parallelPlot.startingDimIndex) as number, 
                    this.dimIndexScale(this.parallelPlot.startingDimIndex + this.parallelPlot.visibleDimCount - 1) as number
                ]
            );
        }

        private updateDimIndexScale() {
            const size = this.parallelPlot.width - ParallelPlot.margin.left - ParallelPlot.margin.right;
            this.dimIndexScale
                .domain(d3.range(this.parallelPlot.dimensions.length))
                .range([0, size]);

            this.dimIndexScaleInvertFn
                .domain([0, size])
                .range(d3.range(this.parallelPlot.dimensions.length));
        }

        private centerBrush(
            indexCenter: number,
            moveBrush: boolean
        ) {
            const sizeDimVisible = this.parallelPlot.visibleDimensions.length;

            let sizeLeft = Math.round((sizeDimVisible - 1) / 2.0);
            let sizeRight = sizeDimVisible - 1 - sizeLeft;

            if (indexCenter - sizeLeft < 0) {
                sizeRight = sizeRight + (sizeLeft - indexCenter);
                sizeLeft = indexCenter;
            }

            if (indexCenter + sizeRight > this.parallelPlot.dimensions.length - 1) {
                sizeLeft = sizeLeft + (indexCenter + sizeRight - this.parallelPlot.dimensions.length + 1);
                sizeRight = this.parallelPlot.dimensions.length - 1 - indexCenter;
            }

            const begin = indexCenter - sizeLeft;
            const end = indexCenter + sizeRight;

            if (
                begin !== this.parallelPlot.startingDimIndex || 
                end !== this.parallelPlot.startingDimIndex + this.parallelPlot.visibleDimCount - 1
            ) {
                this.updateVisibleDimensions(begin, end);
                this.parallelPlot.buildPlotArea();
                this.parallelPlot.style.applyCssRules();

                d3.select(this.parallelPlot.bindto + " .sliderTooltip").style("display", "none");
            }

            if (moveBrush) {
                // Modify the brush selection
                d3.select<SVGGElement, string>(this.parallelPlot.bindto + " .brushDim").call(
                    d3.brushX<string>().move, 
                    [
                        this.dimIndexScale(this.parallelPlot.startingDimIndex) as number, 
                        this.dimIndexScale(this.parallelPlot.startingDimIndex + this.parallelPlot.visibleDimCount - 1) as number
                    ]
                );
            }
        }

        private mouseDown(mouse: [number, number]) {
            this.centerBrush(this.dimIndexScaleInvertFn(mouse[0]), true);
        }

        private mouseMove(mouse: [number, number]) {
            d3.select(this.parallelPlot.bindto + " .locatorLine")
                .style("display", null)
                .attr("x1", mouse[0])
                .attr("x2", mouse[0]);

            const dimIndex = this.dimIndexScaleInvertFn(mouse[0]);
            d3.select(this.parallelPlot.bindto + " .sliderTooltip")
                .text(this.parallelPlot.columns[this.parallelPlot.dimensions[dimIndex]].label)
                .style("left", mouse[0] - ParallelPlot.margin.left + "px")
                .style("top", ParallelPlot.margin.top / 4.0 - 30 + "px")
                .style("display", null);
        }

        private mouseExit() {
            d3.select(this.parallelPlot.bindto + " .locatorLine").style("display", "none");
            d3.select(this.parallelPlot.bindto + " .sliderTooltip").style("display", "none");
        }
        
        // eslint-disable-next-line max-lines-per-function
        private createBrush() {
            this.inSelectionDrag = false;

            d3.select(this.parallelPlot.bindto + " .slider").append("g")
                .attr("class", "brushDim")
                // Call 'd3.brushX()' to create the SVG elements necessary to display the brush selection and to receive input events for interaction
                .call(this.xBrushBehavior())
                // Listen mouve events of 'overlay' group to center brush (if clicked) or show a tooltip
                .call(g => g.select(this.parallelPlot.bindto + " .overlay")
                        .attr("rx", 5)
                        .attr("ry", 5)
                        .on("mousedown touchstart", (event) => { this.mouseDown(d3.pointer(event)); event.stopPropagation(); })
                        .on("mousemove", (event) => { this.mouseMove(d3.pointer(event)); })
                        .on("mouseout", () => { this.mouseExit(); })
                )
                // Listen mouve events of 'selection' group to update 'drag' flag
                .call(g => g.select(this.parallelPlot.bindto + " .selection")
                        .attr("rx", 5)
                        .attr("ry", 5)
                        .on("mousedown", () => { this.inSelectionDrag = true; })
                        .on("mouseup", () => { this.inSelectionDrag = false; })
                );
        }

        private xBrushBehavior() {
            return d3.brushX()
                .handleSize(4)
                // Set brushable area
                .extent([
                    [0, -5],
                    [this.parallelPlot.width - ParallelPlot.margin.left - ParallelPlot.margin.right, 5]
                ])
                // When the brush moves (such as on mousemove), brush is dragged or a brush bound is moved
                .on("brush", (event) => {
                    const selection = event.selection;
                    if (this.inSelectionDrag) {
                        // if brush is dragged, use 'centerBrush' to keep unchanged the number of selected columns
                        const brushCenter = (selection[0] + selection[1]) / 2.0;
                        const centerIndex = this.dimIndexScaleInvertFn(brushCenter);
                        this.centerBrush(centerIndex, false);
                    } else {
                        const begin = this.dimIndexScaleInvertFn(selection[0]);
                        const end = this.dimIndexScaleInvertFn(selection[1]);
                        if (
                            begin !== this.parallelPlot.startingDimIndex || 
                            end !== this.parallelPlot.startingDimIndex + this.parallelPlot.visibleDimCount - 1
                        ) {
                            this.updateVisibleDimensions(begin, end);
                            this.parallelPlot.buildPlotArea();
                            this.parallelPlot.style.applyCssRules();
                        }
                    }
                })
                // At the end of a brush gesture (such as on mouseup), set 'drag' flag to 'false'
                .on("end", () => {
                    this.inSelectionDrag = false;
                });
        }

        public updateVisibleDimensions(begin: number, end: number) {
            if (begin >= 0 && end >= 0) {
                this.parallelPlot.startingDimIndex = begin;
                this.parallelPlot.visibleDimCount = end - begin + 1;
                this.parallelPlot.updateVisibleDimensions();
            }
        }

    }