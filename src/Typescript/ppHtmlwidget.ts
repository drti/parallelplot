// @ts-nocheck
import { PPConfig, ParallelPlot, SliderPosition } from "./parallelPlot";

// eslint-disable-next-line no-undef
HTMLWidgets.widget({

    name: "parallelPlot",

    type: "output",

    // eslint-disable-next-line max-lines-per-function
    factory: function(el: HTMLElement, width: number, height: number) {
        function js2RIndex(index: number) {
            return (typeof index === "number") ? index + 1 : index;
        }

        function r2JsIndex(index: number) {
            return (typeof index === "number") ? index - 1 : index;
        }

        const parallelPlot = new ParallelPlot(el.id, width, height);

        return {
            // eslint-disable-next-line max-lines-per-function
            renderValue: function(config: PPConfig) {
                // Add a reference to the widget from the HTML element
                document.getElementById(parallelPlot.id())!.widget = this;

                // If htmlwidget is included in Shiny app, listen JavaScript messages sent from Shiny
                if (HTMLWidgets.shinyMode) {
                    ["setArrangeMethod", "setCategoriesRep", "setContinuousColorScale", "setRefColumnDim", "setCategoricalColorScale", "setHistoVisibility", "setInvertedAxes", "setCutoffs", "setKeptColumns", "getValue", "highlightRow", "changeRow", "getPlotConfig"].forEach(func => {
                        Shiny.addCustomMessageHandler("parallelPlot:" + func, function(message) {
                            const elem = document.getElementById(message.id);
                            if (elem) {
                                elem.widget[func](message);
                            }
                        });
                    });

                    // Listen event sent by the parallelPlot
                    if (config.eventInputId !== null) {
                        parallelPlot.on(ParallelPlot.PLOT_EVENT, function (event) {
                            if (event.type === ParallelPlot.EDITION_EVENT
                                || event.type === ParallelPlot.ROW_CLICK_EVENT
                                || event.type === ParallelPlot.HL_ROW_EVENT
                            ) {
                                event.value.rowIndex = js2RIndex(event.value.rowIndex);
                            }
                            if (event.type === ParallelPlot.CUTOFF_EVENT) {
                                event.value.selectedTraces = event.value.selectedTraces.map(js2RIndex);
                            }
                            // Forward 'event' to Shiny through the reactive input 'eventInputId'
                            Shiny.setInputValue(config.eventInputId, event, {priority: "event"});
                        });
                    }
                }

                const controlWidgets = (config.controlWidgets === null) 
                    ? !HTMLWidgets.shinyMode : 
                    config.controlWidgets;

                const sliderPosition: SliderPosition | null = config.sliderPosition
                    ? {}
                    : null;
                if (config.sliderPosition) {
                    if (typeof config.sliderPosition.dimCount === "number") {
                        sliderPosition!.dimCount = config.sliderPosition.dimCount
                    }
                    if (typeof config.sliderPosition.startingDimIndex === "number") {
                        sliderPosition!.startingDimIndex = r2JsIndex(config.sliderPosition.startingDimIndex)
                    }
                }

                parallelPlot.generate({
                    data: HTMLWidgets.dataframeToD3(config.data),
                    rowLabels: config.rowLabels,
                    categorical: config.categorical,
                    categoriesRep: config.categoriesRep,
                    arrangeMethod: config.arrangeMethod,
                    inputColumns: config.inputColumns,
                    keptColumns: config.keptColumns,
                    histoVisibility : config.histoVisibility,
                    invertedAxes : config.invertedAxes,
                    cutoffs: config.cutoffs,
                    refRowIndex : config.refRowIndex ? r2JsIndex(config.refRowIndex) : undefined,
                    refColumnDim : config.refColumnDim,
                    rotateTitle : config.rotateTitle,
                    columnLabels : config.columnLabels,
                    continuousCS : config.continuousCS,
                    categoricalCS : config.categoricalCS,
                    editionMode : config.editionMode,
                    controlWidgets: controlWidgets,
                    cssRules: config.cssRules,
                    sliderPosition: sliderPosition
                });
            }, // End 'renderValue'

            setArrangeMethod: function(params) {
                parallelPlot.setArrangeMethod(params.arrangeMethod);
            },

            setCategoriesRep: function(params) {
                parallelPlot.setCategoriesRep(params.categoriesRep);
            },

            setRefColumnDim: function(params) {
                parallelPlot.setRefColumnDim(params.dim);
            },

            setContinuousColorScale: function(params) {
                parallelPlot.setContinuousColorScale(params.continuousCsId);
            },

            setCategoricalColorScale: function(params) {
                parallelPlot.setCategoricalColorScale(params.categoricalCsId);
            },

            setHistoVisibility: function(params) {
                parallelPlot.setHistoVisibility(params.histoVisibility);
            },

            setInvertedAxes: function(params) {
                parallelPlot.setInvertedAxes(params.invertedAxes);
            },

            setCutoffs: function(params) {
                parallelPlot.setCutoffs(params.cutoffs);
            },

            setKeptColumns: function(params) {
                parallelPlot.setKeptColumns(params.keptColumns);
            },

            getValue: function(params) {
                if (HTMLWidgets.shinyMode) {
                    let value = parallelPlot.getValue(params.attrType);
                    if (value === null) {
                        // TODO: Find how to manage 'null' value
                        value = "NULL";
                    }
                    else {
                        if (params.attrType === ParallelPlot.ST_ATTR_TYPE) {
                            value = (value as number[]).map(js2RIndex);
                        }
                    }
                    Shiny.setInputValue(params.valueInputId, value, {priority: "event"});
                }
            },

            highlightRow: function(params) {
                parallelPlot.highlightRow(r2JsIndex(params.rowIndex), params.newValues);
            },

            changeRow: function(params) {
                if (typeof params.rowIndex === "number") {
                    parallelPlot.changeRow(r2JsIndex(params.rowIndex), params.newValues);
                }
            },

            getPlotConfig: function(params) {
                if (HTMLWidgets.shinyMode) {
                    const plotConfig = parallelPlot.getPlotConfig();
                    if (typeof plotConfig.refRowIndex === "number") {
                        plotConfig.refRowIndex = js2RIndex(plotConfig.refRowIndex);
                    }
                    if (plotConfig.sliderPosition && typeof plotConfig.sliderPosition.startingDimIndex === "number") {
                        plotConfig.sliderPosition.startingDimIndex = js2RIndex(plotConfig.sliderPosition.startingDimIndex);
                    }
                    Shiny.setInputValue(params.configInputId, plotConfig, {priority: "event"});
                }
            },

            resize: function(newWidth: number, newHeight: number) {
                parallelPlot.resize(newWidth, newHeight);
            }
        };
    } // End 'factory'
});
