import { ParallelPlot } from "./parallelPlot";
import * as d3 from "d3";

// @ts-ignore
window.ParallelPlot = ParallelPlot;
window.d3 = d3;

export {d3};
export default ParallelPlot;