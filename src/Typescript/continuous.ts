import * as d3 from "d3";
import { Column } from "./column";
import { ExpFormat } from "./expFormat";
import { MultiBrush } from "./multiBrush";
import { CatCutoff, ContCutoff, ParallelPlot, Row } from "./parallelPlot";

    export class Continuous {
        column: Column;

        yScale: d3.ScaleLinear<number, number>;
        
        histoGenerator: d3.HistogramGeneratorNumber<Row, number>;
        
        histoScale: d3.ScaleLinear<number, number>;
        
        multiBrush: MultiBrush | null;

        invertedAxe: boolean;
        
        continuousMin: number | undefined = undefined;
        
        continuousMax: number | undefined = undefined;
        
        contCutoffs: ContCutoff[] | null = null;

        initDone: boolean;

        constructor(
            column: Column,
            invertedAxe: boolean
        ) {
            this.column = column;
            this.invertedAxe = invertedAxe;
            this.yScale = d3.scaleLinear();
            this.histoGenerator = d3.bin<Row, number>();
            this.histoScale = d3.scaleLinear();
            this.multiBrush = null;
            this.initDone = false;
        }

        private checkInitDone() {
            if (!this.initDone) {
                this.initYScaleAndHisto();
                this.initDone = true;
            }
        }

        private initYScaleAndHisto() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            [this.continuousMin, this.continuousMax] = d3.extent(parallelPlot.sampleData, function(row) {
                return +row[columnDim];
            });
            if (typeof this.continuousMin === "undefined" || typeof this.continuousMax === "undefined") {
                console.trace("d3.extent returns 'undefined values'");
                return;
            }
            this.yScale
                .domain(this.invertedAxe 
                    ? [this.continuousMax, this.continuousMin] 
                    : [this.continuousMin, this.continuousMax]
                )
                .range([parallelPlot.axeHeight, 0])
                .nice(this.numbin());

            this.histoGenerator
                .value(function(row) {
                    return row[columnDim];
                })
                .domain([this.continuousMin, this.continuousMax])
                .thresholds(this.equiDepthThresholds(this.continuousMin, this.continuousMax));
        }

        private equiDepthThresholds(min: number, max: number) {
            const binBounds: number[] = [];
            const depth = (max - min) / this.numbin();
            for (let j = 0; j < this.numbin(); j++) {
                binBounds.push(min + j * depth);
            }
            return binBounds;
        }

        private numbin() {
            const parallelPlot = this.column.parallelPlot;
            return Math.ceil(2.5 * Math.pow(parallelPlot.sampleData.length,0.25));
        }

        public y() : d3.ScaleLinear<number, number> {
            this.checkInitDone();
            return this.yScale;
        }

        public yTraceValue(rowIndex: number): number {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const value = parallelPlot.sampleData[rowIndex][columnDim];
            return this.y()(value);
        }

        public histo() : d3.HistogramGeneratorNumber<Row, number> {
            this.checkInitDone();
            return this.histoGenerator;
        }

        public setInvertedAxe(invertedAxe: boolean) {
            if (this.invertedAxe !== invertedAxe) {
                this.invertedAxe = invertedAxe;

                if (this.initDone && typeof this.continuousMin !== "undefined" && typeof this.continuousMax !== "undefined") {
                    this.yScale
                        .domain(this.invertedAxe 
                            ? [this.continuousMax, this.continuousMin] 
                            : [this.continuousMin, this.continuousMax])
                        .nice(this.numbin());
                }
                return true;
            }
            return false;
        }

        // eslint-disable-next-line max-lines-per-function
        public drawMainHistogram() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const dimensionGroup = d3.selectAll<SVGGElement, string>(parallelPlot.bindto + " .plotGroup .dimension")
                .filter(dim => dim === columnDim);

            dimensionGroup.select(".histogram").remove();

            if (!this.column.histoVisible) {
                return;
            }
        
            const histogramGroup = dimensionGroup.append("g")
                .attr("class", "histogram")
                .attr("opacity", "0.5")
                .style("display", function() {
                    return parallelPlot.selectedRows.size > parallelPlot.sampleData.length / 10.0
                        ? null
                        : "none";
                });

            const columnWidth = parallelPlot.xScaleVisibleDimension(parallelPlot.visibleDimensions[0]);
            if (typeof columnWidth === "undefined") {
                console.error("Dim '", parallelPlot.visibleDimensions[0], "' not found");
                return;
            }
            const bins: d3.Bin<Record<string, number>, number>[] = this.histo()(parallelPlot.sampleData);
            const maxBinLength = d3.max(bins.map(b => b.length));
            if (typeof maxBinLength === "undefined") {
                console.error("maxBinLength not found");
                return;
            }
            // range is 70% percent of the original x() size between 2 dimensions
            this.histoScale
                .range([0, columnWidth * 0.7])
                .domain([0, maxBinLength]);
        
            histogramGroup.selectAll("rect")
                .data(bins)
                .enter().append("rect")
                .attr("class", "barMainHisto")
                .attr("pointer-events", "none")
                .attr("x", 2)
                .attr("transform", (bin) => {
                    if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                        console.error("bin.x1 is undefined");
                        return null;
                    }
                    return (
                        "translate(0," +
                        Math.min(
                            this.y()(bin.x1),
                            this.y()(bin.x0)
                        ) + ")"
                    );
                })
                .attr("width", (bin) => this.histoScale(bin.length))
                .attr("height", (bin) => {
                    if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                        console.error("bin.x0 or bin.x1 are undefined");
                        return null;
                    }
                    return Math.abs(this.y()(bin.x0) - this.y()(bin.x1));
                })
                .style("fill", ParallelPlot.mainHistoColor)
                .style("stroke", "white");
        }
        
        // eslint-disable-next-line max-lines-per-function
        public drawSelectedHistogram(
            selected: Record<string, number>[]
        ) {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const dimensionGroup = d3.selectAll<SVGGElement, string>(parallelPlot.bindto + " .plotGroup .dimension")
                .filter(dim => dim === columnDim);

            dimensionGroup.select(".histogramSelected").remove();
        
            if (!this.column.histoVisible) {
                return;
            }
        
            const histogramGroup = dimensionGroup.append("g")
                .attr("class", "histogramSelected")
                .attr("opacity", "0.4");

            const bins = this.histo()(selected);
            let selectedHistoScale: d3.ScaleLinear<number, number>;
            if (selected.length > parallelPlot.sampleData.length / 10.0) {
                selectedHistoScale = this.histoScale;
            }
            else {
                const columnWidth = parallelPlot.xScaleVisibleDimension(
                    parallelPlot.visibleDimensions[0]
                );
                if (typeof columnWidth === "undefined") {
                    console.error("Dim '", parallelPlot.visibleDimensions[0], "' not found");
                    return;
                }
                const maxBinLength = d3.max(bins.map(b => b.length));
                if (typeof maxBinLength === "undefined") {
                    console.error("maxBinLength not found");
                    return;
                }
                selectedHistoScale = d3.scaleLinear()
                    .range([0, columnWidth * 0.7])
                    .domain([0, maxBinLength]);
            }

            histogramGroup.selectAll("rect")
                .data(bins)
                .enter().append("rect")
                .attr("class", "barSecondHisto")
                .attr("pointer-events", "none")
                .attr("x", 2)
                .attr("transform", (bin: d3.Bin<Record<string, number>, number>) => {
                    if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                        console.error("bin.x1 is undefined");
                        return null;
                    }
                    return (
                        "translate(0," +
                        Math.min(
                            this.y()(bin.x1),
                            this.y()(bin.x0)
                        ) + ")"
                    );
                })
                .attr("height", (bin: d3.Bin<Record<string, number>, number>) => {
                    if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                        console.error("bin.x0 or bin.x1 are undefined");
                        return null;
                    }
                    return Math.abs(this.y()(bin.x0) - this.y()(bin.x1));
                })
                .attr("width", function(bin) {
                    return selectedHistoScale.domain()[1] === 0
                        ? 0
                        : selectedHistoScale(bin.length);
                })
                .style("fill", ParallelPlot.secondHistoColor)
                .style("stroke", function() {
                    return "white";
                });
        }

        public getCutoffs(): ContCutoff[] | null {
            return this.contCutoffs;
        }

        public setCutoffs(cutoffs: ContCutoff[] | CatCutoff[] | null | undefined) {
            if (cutoffs) {
                if (typeof cutoffs[0] === "string" || typeof cutoffs[0] === "number") {
                    console.error("categories is null but categorical cutoffs are provided:", cutoffs);
                }
                else {
                    this.contCutoffs = (cutoffs as ContCutoff[]).map(co => {
                        // reverse order
                        return co.sort(function(a, b) {
                            return b - a;
                        });
                    });
                }
            }
            else {
                this.contCutoffs = null;
            }
        }

        public hasFilters() {
            return this.contCutoffs !== null;
        }

        public isKept(value: number) {
            if (this.contCutoffs !== null) {
                let active: boolean = false;

                this.contCutoffs.forEach(function(contCutoff: ContCutoff) {
                    active =
                        active ||
                        (contCutoff[1] <= value && value <= contCutoff[0]);
                });
                return active;
            }
            return true;
        }

        public initMultiBrush() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;
            if (
                this.multiBrush === null ||
                d3.select(parallelPlot.bindto + " ." + MultiBrush.multiBrushClass(this.column.colIndex))
                    .selectAll(".brush")
                    .size() === 0
            ) {
                this.multiBrush = new MultiBrush(
                    this.column.colIndex,
                    parallelPlot,
                    columnDim
                );
            }
            this.multiBrush.initFrom(this.contCutoffs);
        }

        public drawAxe() {
            const axis = d3.axisLeft(this.y())
                .tickFormat(ExpFormat.format);

            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const axisGroup = d3.selectAll<SVGGElement, string>(parallelPlot.bindto + " .axisGroup")
                .filter(dim => dim === columnDim);

            axisGroup.call(axis.scale(this.y()));
        }
        
        public drawColorScale() {
            const columnDim = this.column.dim;
            const parallelPlot = this.column.parallelPlot;

            const dimensionGroup = d3.selectAll<SVGGElement, string>(parallelPlot.bindto + " .plotGroup .dimension")
                .filter(dim => dim === columnDim);

            dimensionGroup.selectAll(".colorScaleBar")
                .data(d3.range(parallelPlot.axeHeight))
                .enter().append("rect")
                .attr("pointer-events", "none")
                .attr("class", "colorScaleBar")
                .attr("x", -4)
                .attr("y", function(_d: number, i: number) {
                    return i;
                })
                .attr("height", 1)
                .attr("width", 4)
                .attr("opacity", 0.9)
                .style("fill", (pixel: number) => {
                    return parallelPlot.valueColor(
                        this.y().invert(pixel)
                    );
                });
        }

    }