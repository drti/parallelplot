import * as d3 from "d3";
import { ExpFormat } from "./expFormat";
import { BrushDef, MultiBrush } from "./multiBrush";
import { ParallelPlot } from "./parallelPlot";

    export class ColumnHeaders {
        dimensionGroup: d3.Selection<SVGGElement, string, d3.BaseType, unknown>;

        parallelPlot: ParallelPlot;

        dragDimension: string | null = null;

        clickCount: number = 0;

        // eslint-disable-next-line max-lines-per-function
        constructor(parallelPlot: ParallelPlot) {
            this.dimensionGroup = d3.select(parallelPlot.bindto + " .plotGroup").selectAll(".dimension");

            this.parallelPlot = parallelPlot;
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisTextColumns: ColumnHeaders = this;

            this.dimensionGroup.append("text")
                .attr("class", "axisLabel")
                .on("mouseover", function() {
                    d3.select(this).attr("font-weight", "bold");
                })
                .on("mouseout", function() {
                    d3.select(this).attr("font-weight", "normal");
                })

                .call(d3.drag<SVGTextElement, string>()
                    // Click Distance : 5 pixel arround .. to begin a drag .. or to have a "click" if under
                    .clickDistance(5)
                    // @ts-expect-error: 'this.parentNode' can be null => generate an error
                    .container(function() { return this.parentNode.parentNode; })
                    .on("start", (_event, dim: string) => {
                        this.dragDimension = dim;
                    })
                    .on("drag", (event, dim: string) => {
                        this.drag(event.x, dim);
                    })
                    .on("end", () => {
                        this.dragEnd();
                    })
                )

                // ON click must be called after Drag ... if not the click event is removed
                .on("click", function(event, dim) {
                    if (event.defaultPrevented) {
                        return; // dragged
                    }

                    if (thisTextColumns.clickCount === 0) {
                        thisTextColumns.clickCount = 1;
                        setTimeout(function() {
                            if (thisTextColumns.clickCount === 1) {
                                parallelPlot.changeColorMapOnDimension(dim);
                            }
                            if (thisTextColumns.clickCount === 2) {
                                const continuous = parallelPlot.columns[dim].continuous
                                if (continuous) {
                                    if (continuous.setInvertedAxe(!continuous.invertedAxe)) {
                                        thisTextColumns.reverseDomainOnAxis(dim);
                                        const invertedAxes: Record<string, boolean> = {};
                                        invertedAxes[dim] = continuous.invertedAxe;
                                        thisTextColumns.parallelPlot.sendInvertedAxeEvent(invertedAxes);
                                        parallelPlot.refreshTracesPaths();
                                    }
                                }
                            }
                            thisTextColumns.clickCount = 0;
                        }, 350);
                    }
                    else if (thisTextColumns.clickCount === 1) {
                        thisTextColumns.clickCount = 2;
                    }
                });

            this.dimensionGroup = d3.select(parallelPlot.bindto + " .plotGroup").selectAll(".dimension");
            parallelPlot.updateColumnLabels();
        }

        // eslint-disable-next-line max-lines-per-function
        private drag(x: number, draggedDim: string) {
            let position: number = x;
            const parallelPlot = this.parallelPlot;
            const dimensionGroup = this.dimensionGroup;
            const dragDimension = this.dragDimension as string;

            const indexInitialPosition: number = parallelPlot.visibleDimensions.indexOf(dragDimension);

            if (indexInitialPosition > 0) {
                const leftDimension: string =
                    parallelPlot.visibleDimensions[indexInitialPosition - 1];
                const leftX: number | undefined = 
                    parallelPlot.xScaleVisibleDimension(leftDimension);

                if (leftX && position < leftX) {
                    if (this.canSwitchDimension(leftDimension, dragDimension)) {
                        this.switchdimension(leftDimension, dragDimension);
                    }
                    else {
                        position = leftX;
                    }
                }
            }

            if (indexInitialPosition < parallelPlot.visibleDimensions.length - 1) {
                const rightDimension: string =
                    parallelPlot.visibleDimensions[indexInitialPosition + 1];
                const rightX: number | undefined = 
                    parallelPlot.xScaleVisibleDimension(rightDimension);

                if (rightX && position > rightX) {
                    if (this.canSwitchDimension(dragDimension, rightDimension)) {
                        this.switchdimension(dragDimension, rightDimension);
                    } else {
                        position = rightX as number;
                    }
                }
            }

            dimensionGroup.filter(dim => dim === draggedDim)
                .attr("transform", function(dim) {
                    return `translate(${position}, ${parallelPlot.yRefRowOffset(dim)})`;
                });
        }

        private dragEnd() {
            const parallelPlot = this.parallelPlot;
            const dimensionGroup = this.dimensionGroup;
            // Move only the second dimension switched
            dimensionGroup.filter(dim => dim === this.dragDimension)
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", function(d) {
                    const x = parallelPlot.xScaleVisibleDimension(d);
                    const yRefRowOffset = parallelPlot.yRefRowOffset(d);
                    return `translate(${x}, ${yRefRowOffset})`;
                });

            parallelPlot.refreshTracesPaths();

            this.dragDimension = null;
        }

        private canSwitchDimension(dim1: string, dim2: string): boolean {
            const parallelPlot = this.parallelPlot;
            const indexDim1 = parallelPlot.dimensions.indexOf(dim1);
            const indexDim2 = parallelPlot.dimensions.indexOf(dim2);

            if (indexDim1 === null || indexDim2 === null) {
                return false;
            }

            if ((indexDim1 as number) + 1 !== (indexDim2 as number)) {
                return false;
            }

            return true;
        }

        // eslint-disable-next-line max-lines-per-function
        private switchdimension(leftDim: string, rightDim: string) {
            const parallelPlot = this.parallelPlot;
            const dimensionGroup = this.dimensionGroup;

            const leftVisibleIndex = parallelPlot.visibleDimensions.indexOf(leftDim);
            const rightVisibleIndex = parallelPlot.visibleDimensions.indexOf(rightDim);

            const leftSliderIndex = parallelPlot.dimensions.indexOf(leftDim);
            const rightSliderIndex = parallelPlot.dimensions.indexOf(rightDim);

            if (
                leftVisibleIndex === -1 ||
                rightVisibleIndex === -1 ||
                leftSliderIndex === -1 ||
                rightSliderIndex === -1
            ) {
                return;
            }

            // check that they are coherent leftIndex + 1 = rightIndex in both array
            if (leftVisibleIndex + 1 !== rightVisibleIndex) {
                return;
            }

            if (leftSliderIndex + 1 !== rightSliderIndex) {
                return;
            }

            // Switch on global
            parallelPlot.dimensions[leftSliderIndex] = rightDim;
            parallelPlot.dimensions[rightSliderIndex] = leftDim;

            // Switch on Visible
            parallelPlot.visibleDimensions[leftVisibleIndex] = rightDim;
            parallelPlot.visibleDimensions[rightVisibleIndex] = leftDim;

            // Reset of the XScale
            parallelPlot.updateXScale();

            // Move only the second dimension switched
            dimensionGroup.filter(dim =>
                    (dim === rightDim || dim === leftDim) &&
                    dim !== this.dragDimension
                )
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", function(d) {
                    const x = parallelPlot.xScaleVisibleDimension(d);
                    const yRefRowOffset = parallelPlot.yRefRowOffset(d);
                    return `translate(${x}, ${yRefRowOffset})`;
                });
        }

        // eslint-disable-next-line max-lines-per-function
        public reverseDomainOnAxis(revDim: string) {
            const dimensionGroup = this.dimensionGroup;
            const parallelPlot = this.parallelPlot;
            const reversedColumn = parallelPlot.columns[revDim];

            // If not continuous axis, no reverse
            if (reversedColumn.continuous === null) {
                return;
            }

            parallelPlot.updateColumnLabels();
            
            // Have the old scale in order to do some reverse operation
            const [old1, old2] = reversedColumn.continuous.y().domain();
            const oldScale = d3.scaleLinear()
                .range(reversedColumn.continuous.y().range())
                .domain([old2, old1]);

            const axis = d3.axisLeft(reversedColumn.continuous.y())
                .tickFormat(ExpFormat.format);
            dimensionGroup.filter(dim => dim === revDim)
                .each(function() {
                    d3.select(this).selectAll<SVGGElement, string>(".axisGroup")
                        .transition()
                        .ease(d3.easeBackOut)
                        .duration(ParallelPlot.D3_TRANSITION_DURATION)
                        .call(axis);
                });

            // cannot filter the dimension group
            // we need the i argument in order to find the brush
            dimensionGroup.filter(dim => dim === revDim)
                // eslint-disable-next-line max-lines-per-function
                .each(function(dim: string) {
                    // The group can be moved by the reverse if the center lane is defined
                    d3.select(this)
                        .transition()
                        .ease(d3.easeBackOut)
                        .duration(ParallelPlot.D3_TRANSITION_DURATION)
                        .attr("transform", function() {
                            const x = parallelPlot.xScaleVisibleDimension(revDim);
                            const yRefRowOffset = parallelPlot.yRefRowOffset(dim);
                            return `translate(${x}, ${yRefRowOffset})`;
                        });

                    // Reverse Main histo Bins of the group
                    d3.select(this)
                        .selectAll<SVGRectElement, d3.Bin<Record<string, number>, number>>(".barMainHisto")
                        .transition()
                        .ease(d3.easeBackOut)
                        .duration(ParallelPlot.D3_TRANSITION_DURATION)
                        .attr("transform", function(
                            bin: d3.Bin<Record<string, number>, number>
                        ) {
                            if (typeof bin.x0 === "undefined" ||typeof bin.x1 === "undefined") {
                                console.error("bin.x1 is undefined");
                                return null;
                            }
                            return (
                                "translate(0," +
                                Math.min(
                                    reversedColumn.continuous!.y()(bin.x1),
                                    reversedColumn.continuous!.y()(bin.x0)
                                ) + ")"
                            );
                        })
                        .attr("height", function(
                            bin: d3.Bin<Record<string, number>, number>
                        ) {
                            if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                                console.error("bin.x0 or bin.x1 are undefined");
                                return null;
                            }
                            return Math.abs(
                                reversedColumn.continuous!.y()(bin.x0) -
                                reversedColumn.continuous!.y()(bin.x1)
                            );
                        });

                    // Reverse Second Histo Bins
                    d3.select(this)
                        .selectAll<SVGRectElement, d3.Bin<Record<string, number>, number>>(".barSecondHisto")
                        .transition()
                        .ease(d3.easeBackOut)
                        .duration(ParallelPlot.D3_TRANSITION_DURATION)
                        .attr("transform", function(
                            bin: d3.Bin<Record<string, number>, number>
                        ) {
                            if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                                console.error("bin.x1 is undefined");
                                return null;
                            }
                            return (
                                "translate(0," +
                                Math.min(
                                    reversedColumn.continuous!.y()(bin.x1),
                                    reversedColumn.continuous!.y()(bin.x0)
                                ) + ")"
                            );
                        })
                        .attr("height", function(
                            bin: d3.Bin<Record<string, number>, number>
                        ) {
                            if (typeof bin.x0 === "undefined" || typeof bin.x1 === "undefined") {
                                console.error("bin.x0 or bin.x1 are undefined");
                                return null;
                            }
                            return Math.abs(
                                reversedColumn.continuous!.y()(bin.x0) -
                                reversedColumn.continuous!.y()(bin.x1)
                            );
                        });

                    // Reverse the brush
                    const allDimensions = Object.keys(parallelPlot.sampleData[0]);
                    const colIndex = allDimensions.indexOf(dim);
                    d3.select(this)
                        .selectAll("." + MultiBrush.multiBrushClass(colIndex))
                        .selectAll<SVGGElement, BrushDef>(".brush")
                        .each(function() {
                            const brushGroup = d3.select(this);
                            const brushSelection = d3.brushSelection(
                                brushGroup.node() as SVGGElement
                            ) as [number, number];

                            if (brushSelection) {
                                const reversedBrushSelection = [
                                    reversedColumn.continuous!.y()(oldScale.invert(brushSelection[0])),
                                    reversedColumn.continuous!.y()(oldScale.invert(brushSelection[1]))
                                ].sort((a, b) => a - b);

                                // Modify the brush selection
                                brushGroup
                                    .transition()
                                    .ease(d3.easeBackOut)
                                    .duration(ParallelPlot.D3_TRANSITION_DURATION)
                                    .call(d3.brushY().move, reversedBrushSelection); // [selection[1] as number, selection[0] as number])
                            }
                        });
                    // Reverse the color Scale if it is on the same group
                    d3.select(this)
                        .selectAll<SVGRectElement, number>(".colorScaleBar")
                        .transition()
                        .duration(ParallelPlot.D3_TRANSITION_DURATION)
                        .style("fill", function(pixel: number) {
                            return parallelPlot.valueColor(reversedColumn.continuous!.y().invert(pixel));
                        });
            });
        }
        
    }