import * as d3 from "d3";

export type Row = Record<string, number>;

export type ContCutoff = [number, number];

export type CatCutoff = number | string;

export type BrushSelection = [number, number];

type UnnamedCutoffs = (ContCutoff[] | CatCutoff[] | null)[];

type NamedCutoffs = Record<string, ContCutoff[]> | Record<string, CatCutoff[]>;

type Point = [number, number];

export type SliderPosition = {
    dimCount?: number;
    startingDimIndex?: number;
};    

export type PPConfig = {
    data: d3.DSVRowArray<string>;
    rowLabels: string[] | null | undefined;
    categorical: ((string | number)[] | null | undefined)[] | null | undefined;
    categoriesRep: string | null | undefined;
    arrangeMethod: string | null | undefined;
    inputColumns: boolean[] | null | undefined;
    keptColumns: boolean[] | null | undefined;
    histoVisibility: boolean[] | null | undefined;
    invertedAxes: boolean[] | null | undefined;
    cutoffs: (ContCutoff[] | CatCutoff[] | null | undefined)[] | null | undefined;
    refRowIndex: number | null | undefined;
    refColumnDim: string | null | undefined;
    rotateTitle: boolean | null | undefined;
    columnLabels: string[] | null | undefined;
    continuousCS: string | null | undefined;
    categoricalCS: string | null | undefined;
    editionMode: string | null | undefined;
    controlWidgets: boolean | null | undefined;
    cssRules: Record<string, string | string[]> | null | undefined;
    sliderPosition: SliderPosition | null | undefined;
};

import { BrushSlider } from "./brushSlider";
import { Column } from "./column";
import { ColumnHeaders } from "./columnHeaders";
import { MultiBrush } from "./multiBrush";
import { Style } from "./style";

    export class ParallelPlot {
        static readonly mainHistoColor = "#a6f2f2";

        static readonly secondHistoColor = "#169c9c";

        static readonly CONTINUOUS_CS: Record<string, (_t: number) => string> = {
                // From d3-scale.
                Viridis: d3.interpolateViridis,
                Inferno: d3.interpolateInferno,
                Magma: d3.interpolateMagma,
                Plasma: d3.interpolatePlasma,
                Warm: d3.interpolateWarm,
                Cool: d3.interpolateCool,
                Rainbow: d3.interpolateRainbow,
                CubehelixDefault: d3.interpolateCubehelixDefault,
                // From d3-scale-chromatic
                Blues: d3.interpolateBlues,
                Greens: d3.interpolateGreens,
                Greys: d3.interpolateGreys,
                Oranges: d3.interpolateOranges,
                Purples: d3.interpolatePurples,
                Reds: d3.interpolateReds,
                BuGn: d3.interpolateBuGn,
                BuPu: d3.interpolateBuPu,
                GnBu: d3.interpolateGnBu,
                OrRd: d3.interpolateOrRd,
                PuBuGn: d3.interpolatePuBuGn,
                PuBu: d3.interpolatePuBu,
                PuRd: d3.interpolatePuRd,
                RdBu: d3.interpolateRdBu,
                RdPu: d3.interpolateRdPu,
                YlGnBu: d3.interpolateYlGnBu,
                YlGn: d3.interpolateYlGn,
                YlOrBr: d3.interpolateYlOrBr,
                YlOrRd: d3.interpolateYlOrRd
        };

        static readonly CONTINUOUS_CS_IDS: string[] = Object.keys(ParallelPlot.CONTINUOUS_CS);

        static readonly CATEGORIAL_CS: Record<string, d3.ScaleOrdinal<number, string>> = {
            Category10: d3.scaleOrdinal<number, string>(d3.schemeCategory10),
            Accent: d3.scaleOrdinal<number, string>(d3.schemeAccent),
            Dark2: d3.scaleOrdinal<number, string>(d3.schemeDark2),
            Paired: d3.scaleOrdinal<number, string>(d3.schemePaired),
            Set1: d3.scaleOrdinal<number, string>(d3.schemeSet1)
        };

        static readonly CATEGORIAL_CS_IDS: string[] = Object.keys(ParallelPlot.CATEGORIAL_CS);

        static readonly CATEGORIES_REP_LIST = ["EquallySpacedLines", "EquallySizedBoxes"];

        static readonly margin = { top: 100, right: 10, bottom: 10, left: 10 };

        static readonly catClusterWidth = 12;

        static readonly line = d3.line();

        static readonly PLOT_EVENT = "plotEvent";

        static readonly CUTOFF_EVENT = "cutoffChange";

        static readonly INVERTED_AXE_EVENT = "axeOrientationChange";

        static readonly REF_COLUMN_DIM_EVENT = "refColumnDimChange";

        static readonly HL_ROW_EVENT = "hlRowEvent";

        static readonly ROW_CLICK_EVENT = "rowClicked";

        static readonly EDITION_EVENT = "pointChange";

        static readonly CO_ATTR_TYPE = "Cutoffs";

        static readonly ST_ATTR_TYPE = "SelectedTraces";

        static readonly RC_ATTR_TYPE = "ReferenceColumn";

        static readonly EDITION_OFF = "EditionOff";

        static readonly EDITION_ON_DRAG = "EditionOnDrag";

        static readonly EDITION_ON_DRAG_END = "EditionOnDragEnd";

        static readonly EDITION_MODE_IDS: string[] = [ParallelPlot.EDITION_OFF, ParallelPlot.EDITION_ON_DRAG, ParallelPlot.EDITION_ON_DRAG_END, ];

        static readonly MAX_VISIBLE_DIMS = 30;

        static readonly DEFAULT_SLIDER_POSITION = {
            dimCount: 15,
            startingDimIndex: 0
        }
    
        static readonly ARRANGE_FROM_LEFT = "fromLeft";

        static readonly ARRANGE_FROM_RIGHT = "fromRight";

        static readonly ARRANGE_FROM_BOTH = "fromBoth";

        static readonly ARRANGE_FROM_NONE = "fromNone";

        static readonly ARRANGE_METHODS = [ParallelPlot.ARRANGE_FROM_LEFT, ParallelPlot.ARRANGE_FROM_RIGHT, ParallelPlot.ARRANGE_FROM_BOTH, ParallelPlot.ARRANGE_FROM_NONE];

        static readonly D3_TRANSITION_DURATION: number = 500;

        width = 0;

        height = 0;

        changeColorDuration: number = 1000;

        axeHeight: number = 0;

        bindto: string;

        rowLabels: string[] | null | undefined = null;

        sampleData: Row[] = [];

        dimensions: string[] = [];

        defaultVisibleDimCount: number = 0;
    
        visibleDimCount: number = 0;
    
        startingDimIndex: number = 0;
    
        visibleDimensions: string[] = [];

        /** Set Containing the index of uncut rows. */
        selectedRows: Set<number> = new Set();

        continuousCsId: string = ParallelPlot.CONTINUOUS_CS_IDS[0];

        categoricalCsId: string = ParallelPlot.CATEGORIAL_CS_IDS[0];

        refColumnDim: string | null = null;

        colorScale: ((_t: number) => string) | null = null;

        columns: Record<string, Column> = {}; // Column for each dimension

        editedRowIndex: number | null = null;

        editedPointDim: string | null = null;

        editionMode: string = ParallelPlot.EDITION_ON_DRAG_END;

        /**
         * Position of each dimension in X in the drawing domain
         */
        xScaleVisibleDimension: d3.ScalePoint<string> = d3.scalePoint();

        refRowIndex: number | null = null;

        style: Style;

        hlPointIndex: number | null = null;

        dispatch = d3.dispatch<EventTarget>(ParallelPlot.PLOT_EVENT);

        rotateTitle: boolean = false;

        catSpreaderMap: Map<number, d3.ScalePoint<number>> = new Map();

        columnHeaders: ColumnHeaders | null = null;

        catEquallySpacedLines = true;

        arrangeMethod = ParallelPlot.ARRANGE_METHODS[1];

        useControlWidgets = false;

        constructor(id: string, width: number, height: number) {
            this.bindto = "#" + id;
            this.style = new Style(this.bindto);
            this.width = width ? width : 1200;
            this.height = height ? height : 600;
        }

        public id() {
            return this.bindto.substring(1);
        }

        public resize(width: number, height: number) {
            this.width = width ? width : 1200;
            this.height = height ? height : 600;
            
            d3.select(this.bindto + " svg")
                .attr("width", this.width)
                .attr("height", this.height)
                
            this.updateXScale();
            d3.select(this.bindto + " .slider .axisGroup").remove();
            d3.select(this.bindto + " .slider .brushDim").remove();
            new BrushSlider(this);
            this.buildPlotArea();
            this.style.applyCssRules();
        }

        private getHeight() {
            // Try to take into account the height of 'controlWidgets'
            if (this.useControlWidgets) {
                const controlDivNode = d3.select<HTMLElement, unknown>(this.bindto + " .controlDiv").node();
                if (controlDivNode !== null) {
                    const controlDivHeight = controlDivNode.getBoundingClientRect().height;
                    if (this.height > controlDivHeight) {
                        return this.height - controlDivHeight;
                    }
                }
            }
            return this.height;
        }

        private initSliderPosition(config: PPConfig) {
            if (config.sliderPosition) {
                if (typeof config.sliderPosition.dimCount !== "number" 
                    || config.sliderPosition.dimCount > ParallelPlot.MAX_VISIBLE_DIMS
                ) {
                    this.defaultVisibleDimCount = ParallelPlot.DEFAULT_SLIDER_POSITION.dimCount;
                }
                else {
                    this.defaultVisibleDimCount = config.sliderPosition.dimCount;
                }
                if (typeof config.sliderPosition.startingDimIndex === "number") {
                    this.startingDimIndex = config.sliderPosition.startingDimIndex;
                }
                else {
                    this.startingDimIndex = ParallelPlot.DEFAULT_SLIDER_POSITION.startingDimIndex;
                }
            }
            else {
                this.defaultVisibleDimCount = ParallelPlot.DEFAULT_SLIDER_POSITION.dimCount;
                this.startingDimIndex = ParallelPlot.DEFAULT_SLIDER_POSITION.startingDimIndex;
            }

            if (this.dimensions.length < this.defaultVisibleDimCount) {
                this.visibleDimCount = this.dimensions.length;
            }
            else {
                this.visibleDimCount = this.defaultVisibleDimCount;
            }
            
            if (this.startingDimIndex > this.dimensions.length - this.visibleDimCount) {
                this.startingDimIndex = this.dimensions.length - this.visibleDimCount;
            }
        }
    
        // eslint-disable-next-line max-lines-per-function
        public generate(config: PPConfig) {
            if (d3.select(this.bindto).empty()) {
                throw new Error("'bindto' dom element not found:" + this.bindto);
            }

            this.style.cssRules = config.cssRules;
            d3.select(this.bindto).classed("parallelPlot", true);
            this.checkConfig(config);
            this.rowLabels = config.rowLabels;
            this.catEquallySpacedLines = config.categoriesRep === ParallelPlot.CATEGORIES_REP_LIST[0];
            this.arrangeMethod = config.arrangeMethod ? config.arrangeMethod : ParallelPlot.ARRANGE_FROM_RIGHT;
            this.initSampleData(config);

            d3.select(this.bindto).style("position", "relative");

            d3.select(this.bindto).selectAll("div").remove();

            d3.select(this.bindto).append("div")
                .attr("class", "ppDiv")
                .classed("withWidgets", this.useControlWidgets)
                .classed("withoutWidgets", !this.useControlWidgets);

            this.appendControlDiv();

            this.axeHeight =
                this.getHeight() - ParallelPlot.margin.top - ParallelPlot.margin.bottom;

            if (this.refRowIndex !== null) {
                this.axeHeight = this.axeHeight / 2;
            }
            this.rotateTitle = config.rotateTitle 
                ? config.rotateTitle
                : false;

            const allDimensions = Object.keys(this.sampleData[0]);
            allDimensions.forEach((dim, i) => {
                const isInput = Array.isArray(config.inputColumns)
                    ? config.inputColumns[i]
                    : true;
                const label = Array.isArray(config.columnLabels)
                    ? config.columnLabels[i]
                    : dim;
                const categories = Array.isArray(config.categorical)
                    ? config.categorical[i]
                    : null;
                const cutoffs = Array.isArray(config.cutoffs)
                    ? config.cutoffs[i]
                    : null;
                const histoVisibility = Array.isArray(config.histoVisibility)
                    ? config.histoVisibility[i]
                    : false;
                const invertedAxis = Array.isArray(config.invertedAxes)
                    ? config.invertedAxes[i]
                    : false;
                this.columns[dim] = new Column(
                    dim,
                    i,
                    this,
                    label,
                    categories,
                    cutoffs,
                    histoVisibility,
                    invertedAxis,
                    isInput ? Column.INPUT : Column.OUTPUT
                );
            });
            const nanColumns = allDimensions.map(dim => this.sampleData.every(row => isNaN(row[dim])))

            this.dimensions = allDimensions.filter(
                (_dim, i) => !(nanColumns[i] || (Array.isArray(config.keptColumns) && !config.keptColumns[i]))
            );

            if (!config.refColumnDim) {
                this.refColumnDim = null;
            }
            else if (this.dimensions.includes(config.refColumnDim)) {
                this.refColumnDim = config.refColumnDim;
            }
            else {
                console.error("Unknown 'refColumnDim': " + config.refColumnDim);
            }

            this.updateSelectedRows();
            this.initSliderPosition(config);

            if (!config.continuousCS) {
                this.continuousCsId = ParallelPlot.CONTINUOUS_CS_IDS[0];
            }
            else if (ParallelPlot.CONTINUOUS_CS_IDS.includes(config.continuousCS)) {
                this.continuousCsId = config.continuousCS;
            }
            else {
                console.error("Unknown continuous color scale: " + config.continuousCS);
            }

            if (!config.categoricalCS) {
                this.categoricalCsId = ParallelPlot.CATEGORIAL_CS_IDS[0];
            }
            else if (ParallelPlot.CATEGORIAL_CS_IDS.includes(config.categoricalCS)) {
                this.categoricalCsId = config.categoricalCS;
            }
            else {
                console.error("Unknown categorical color scale: " + config.categoricalCS);
            }

            if (!config.editionMode) {
                this.editionMode = ParallelPlot.EDITION_OFF;
            }
            else if (ParallelPlot.EDITION_MODE_IDS.includes(config.editionMode)) {
                this.editionMode = config.editionMode;
            }
            else {
                console.error("Unknown edition mode: " + config.editionMode);
            }

            this.appendPlotSvg();
            this.applyColorMap();
            this.initTraceTooltip();

            this.appendContCsSelect();
            this.appendCatCsSelect();
            this.appendZAxisSelector();
            this.initZAxisUsedCB();
            this.appendCatRepSelect();
            this.appendArrangeMethodSelect();

            this.style.applyCssRules();
        }

        private appendControlDiv() {
            const ppDiv = d3.select(this.bindto + " .ppDiv");
            const controlDiv = ppDiv.append("div").attr("class", "controlDiv");

            const csDiv = controlDiv.append("div")
                .attr("class", "csDiv");
            csDiv.append("div")
                .attr("class", "zAxisUsedDiv")
                .html(`<input type="checkbox" id="${this.id()}_zAxisUsed" name="zAxisUsed" checked> <label for="${this.id()}_zAxisUsed">Use Z Axis</label> <span class="ParamSelect ZAxis"></span>`);

            csDiv.append("div")
                .html("Continuous Color Scale: <span class=\"contCsSelect\"></span>");
            csDiv.append("div")
                .html("Categorical Color Scale: <span class=\"catCsSelect\"></span>");

            const catDiv = controlDiv.append("div")
                .attr("class", "catDiv");

            catDiv.append("div")
                .html("Categories Representation: <span class=\"catRepSelect\"></span>");
            catDiv.append("div")
                .html("Arrange Method in Category Boxes: <span class=\"arrangeMethodSelect\"></span>");
        }

        private appendContCsSelect() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPPlot = this;

            d3.select(this.bindto + " .contCsSelect").append("select")
                .on("change", function() {
                    const contCsKey = ParallelPlot.CONTINUOUS_CS_IDS[this.selectedIndex];
                    thisPPlot.setContinuousColorScale(contCsKey);
                })
                .selectAll("option")
                .data(ParallelPlot.CONTINUOUS_CS_IDS)
                .enter().append("option")
                .text(function (d) { return d; })
                .attr("value", function (d) { return d; });

            const contCsIndex = ParallelPlot.CONTINUOUS_CS_IDS.indexOf(this.continuousCsId);
            d3.select<HTMLSelectElement, string>(this.bindto + " .contCsSelect > select")
                .property("selectedIndex", contCsIndex);
        }

        private appendCatCsSelect() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPPlot = this;

            d3.select(this.bindto + " .catCsSelect").append("select")
                .on("change", function() {
                    const catCsKey = ParallelPlot.CATEGORIAL_CS_IDS[this.selectedIndex];
                    thisPPlot.setCategoricalColorScale(catCsKey);
                })
                .selectAll("option")
                .data(ParallelPlot.CATEGORIAL_CS_IDS)
                .enter().append("option")
                .text(function (d) { return d; })
                .attr("value", function (d) { return d; });

            const catCsIndex = ParallelPlot.CATEGORIAL_CS_IDS.indexOf(this.categoricalCsId);
            d3.select<HTMLSelectElement, string>(this.bindto + " .catCsSelect > select")
                .property("selectedIndex", catCsIndex);
        }

        private appendZAxisSelector() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPPlot = this;

            d3.select(this.bindto + " .ParamSelect.ZAxis")
                .append("select")
                .on("change", function () {
                    thisPPlot.changeColorMapOnDimension(thisPPlot.dimensions[this.selectedIndex]);
                })
                .selectAll("option")
                .data(this.dimensions)
                .enter().append("option")
                .text(function (d) { return d; })
                .attr("value", function (d) { return d; });

            if (this.refColumnDim !== null) {
                const paramIndex = this.dimensions.indexOf(this.refColumnDim);
                d3.select<HTMLSelectElement, string>(this.bindto + " .ParamSelect.ZAxis > select")
                    .property("selectedIndex", paramIndex);
            }
        }

        private initZAxisUsedCB() {
            d3.select(`#${this.id()}_zAxisUsed`)
                .property("checked", this.refColumnDim !== null)
                .on("change", ParallelPlot.prototype.updateZAxisFromGui.bind(this));
        }

        private updateZAxisGui() {
            if (this.refColumnDim !== null) {
                const paramIndex = this.dimensions.indexOf(this.refColumnDim);
                d3.select<HTMLSelectElement, string>(this.bindto + " .ParamSelect.ZAxis > select")
                    .property("selectedIndex", paramIndex);
            }
            d3.select(`#${this.id()}_zAxisUsed`)
                .property("checked", this.refColumnDim !== null);
        }

        private updateZAxisFromGui() {
            if (d3.select(`#${this.id()}_zAxisUsed`).property("checked")) {
                const zAxisSelectNode = d3.select<HTMLSelectElement, unknown>(this.bindto + " .ParamSelect.ZAxis>select").node();
                if (zAxisSelectNode && this.refColumnDim !== this.dimensions[zAxisSelectNode.selectedIndex]) {
                    this.refColumnDim = this.dimensions[zAxisSelectNode.selectedIndex];
                    this.sendRefColumnDimEvent();
                    this.applyColorMap();
                }
            }
            else if (this.refColumnDim !== null) {
                this.refColumnDim = null;
                this.sendRefColumnDimEvent();
                this.applyColorMap();
            }
        }

        private appendCatRepSelect() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPPlot = this;

            d3.select(this.bindto + " .catRepSelect").append("select")
                .on("change", function() {
                    const catRep = ParallelPlot.CATEGORIES_REP_LIST[this.selectedIndex];
                    thisPPlot.setCategoriesRep(catRep);
                })
                .selectAll("option")
                .data(ParallelPlot.CATEGORIES_REP_LIST)
                .enter().append("option")
                .text(function (d) { return d; })
                .attr("value", function (d) { return d; });

            const catRep = this.catEquallySpacedLines ? ParallelPlot.CATEGORIES_REP_LIST[0] : ParallelPlot.CATEGORIES_REP_LIST[1]
            const catRepIndex = ParallelPlot.CATEGORIES_REP_LIST.indexOf(catRep);
            d3.select<HTMLSelectElement, string>(this.bindto + " .catRepSelect > select")
                .property("selectedIndex", catRepIndex);
        }

        private appendArrangeMethodSelect() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPPlot = this;

            d3.select(this.bindto + " .arrangeMethodSelect").append("select")
                .on("change", function() {
                    const arrangeMethod = ParallelPlot.ARRANGE_METHODS[this.selectedIndex];
                    thisPPlot.setArrangeMethod(arrangeMethod);
                })
                .selectAll("option")
                .data(ParallelPlot.ARRANGE_METHODS)
                .enter().append("option")
                .text(function (d) { return d; })
                .attr("value", function (d) { return d; });

            const arrangeMethodIndex = ParallelPlot.ARRANGE_METHODS.indexOf(this.arrangeMethod);
            d3.select<HTMLSelectElement, string>(this.bindto + " .arrangeMethodSelect > select")
                .property("selectedIndex", arrangeMethodIndex);
        }

        // eslint-disable-next-line max-lines-per-function
        private appendPlotSvg() {
            const ppDiv = d3.select(this.bindto + " .ppDiv");
            const svg = ppDiv.append("svg")
                .attr("width", this.width)
                .attr("height", this.getHeight());
            this.addGlow();

            const plotGroup = svg.append("g")
                .attr("class", "plotGroup")
                .attr(
                    "transform",
                    "translate(" +
                        ParallelPlot.margin.left + "," +
                        ParallelPlot.margin.top + ")"
                );

            this.setColorScale();

            svg.append("g")
                .attr("class", "slider")
                .attr(
                    "transform",
                    "translate(" +
                        ParallelPlot.margin.left + "," +
                        ParallelPlot.margin.top / 4.0 + ")"
                );

            // Path of the background
            plotGroup.append("g")
                .attr("class", "background")
                .attr("opacity", "0.1");

            // Path of the foreground
            plotGroup.append("g")
                .attr("class", "foreground")
                .attr("opacity", "0.8");

            plotGroup.append("g")
                .attr("class", "columns");

            this.updateVisibleDimensions();

            new BrushSlider(this);
            this.buildPlotArea();

            plotGroup.append("path")
                .attr("class", "subHlTrace")
                .attr("d", this.path(0) as string)
                .attr("stroke-width", 3)
                .attr("fill", "none")
                .attr("pointer-events", "none")
                .style("display", "none")
                .style("filter", "url(#glow)");

            plotGroup.append("path")
                .attr("class", "hlTrace")
                .attr("d", this.path(0) as string)
                .attr("stroke-width", 2)
                .attr("fill", "none")
                .attr("pointer-events", "none")
                .style("display", "none");

            plotGroup.append("path")
                .attr("class", "subEditedTrace")
                .attr("d", this.path(0) as string)
                .attr("stroke-width", 3)
                .attr("opacity", "0.8")
                .attr("fill", "none")
                .attr("pointer-events", "none")
                .style("display", "none")
                .style("filter", "url(#glow)");

            plotGroup.append("path")
                .attr("class", "editedTrace")
                .attr("d", this.path(0) as string)
                .attr("opacity", "0.8")
                .attr("stroke-width", 2)
                .attr("fill", "none")
                .style("display", "none")
                .on("click", () => {
                    this.editedRowIndex = null;
                    this.drawEditedTrace();
                });

            plotGroup.append("g")
                .attr("class", "editionCircles")
                .style("display", "none");
        }

        private checkConfig(config: PPConfig) {
            this.useControlWidgets = typeof config.controlWidgets === "boolean" ? config.controlWidgets : false;
            ParallelPlot.checkData(config);
            ParallelPlot.checkCategorical(config);
            ParallelPlot.checkCategoriesRep(config);
            ParallelPlot.checkArrangeMethod(config);
            ParallelPlot.checkColumnLabels(config);
            ParallelPlot.checkInputColumns(config);
            this.checkRefRowIndex(config);
        }

        private static checkData(config: PPConfig) {
            if (!Array.isArray(config.data)) {
                throw new Error("given dataset is not a D3 friendly (row-oriented) data");
            }
            if (config.data.length === 0) {
                throw new Error("given dataset contains no line)");
            }

            if (typeof config.data.columns === "undefined") {
                config.data.columns = Object.keys(config.data[0]);
            }
        }

        private static checkCategorical(config: PPConfig) {
            if (config.categorical) {
                if (Array.isArray(config.categorical)) {
                    if (config.categorical.length !== config.data.columns.length) {
                        console.error("Length of 'categorical' must be equal to the number of columns of 'data'");
                        config.categorical = null;
                    }
                }
                else {
                    console.error("'categorical' must be an array");
                    config.categorical = null;
                }
            }
        }

        private static checkCategoriesRep(config: PPConfig) {
            if (!config.categoriesRep) {
                config.categoriesRep = ParallelPlot.CATEGORIES_REP_LIST[0];
            }
            else if (!ParallelPlot.CATEGORIES_REP_LIST.includes(config.categoriesRep)) {
                console.error("Unknown categoriesRep: " + config.categoriesRep);
            }
        }

        private static checkArrangeMethod(config: PPConfig) {
            if (!config.arrangeMethod) {
                config.arrangeMethod = null;
            }
            else if (!ParallelPlot.ARRANGE_METHODS.includes(config.arrangeMethod)) {
                console.error("Unknown arrangeMethod: " + config.arrangeMethod);
                config.arrangeMethod = null;
            }
        }

        private static checkColumnLabels(config: PPConfig) {
            if (config.columnLabels) {
                if (Array.isArray(config.columnLabels)) {
                    if (config.columnLabels.length !== config.data.columns.length) {
                        console.error("Length of 'columnLabels' must be equal to the number of columns of 'data'");
                        config.columnLabels = null;
                    }
                }
                else {
                    console.error("'columnLabels' must be an array");
                    config.columnLabels = null;
                }
            }
        }

        private static checkInputColumns(config: PPConfig) {
            if (config.inputColumns) {
                if (Array.isArray(config.inputColumns)) {
                    if (config.inputColumns.length !== config.data.columns.length) {
                        console.error("Length of 'inputColumns' must be equal to the number of columns of 'data'");
                        config.inputColumns = null;
                    }
                }
                else {
                    console.error("'inputColumns' must be an array");
                    config.inputColumns = null;
                }
            }
        }

        private checkRefRowIndex(config: PPConfig) {
            if (typeof config.refRowIndex === "number") {
                this.refRowIndex = config.refRowIndex;
            }
            else {
                if (config.refRowIndex) {
                    console.error("'refRowIndex' must be of integer type")
                }
                this.refRowIndex = null;
            }

            if (Array.isArray(config.data)) {
                const rowCount = config.data.length;
                if (typeof this.refRowIndex === "number" && (this.refRowIndex < 0 || this.refRowIndex > rowCount)) {
                    console.error(`refRowIndex: ${this.refRowIndex} must be a valid row index, it must be in range: [1, ${ rowCount - 1}]`);
                    this.refRowIndex = null;
                }
            }
        }

        private addGlow() {
            const svg = d3.select(this.bindto + " svg");
            //Container for the gradients
            const defs = svg.append("defs");

            //Filter for the outside glow
            const filter = defs.append("filter").attr("id", "glow");
            filter.append("feGaussianBlur")
                .attr("stdDeviation", "3.5")
                .attr("result", "coloredBlur");
            const feMerge = filter.append("feMerge");
            feMerge.append("feMergeNode").attr("in", "coloredBlur");
            feMerge.append("feMergeNode").attr("in", "SourceGraphic");        
        }

        updateXScale() {
            this.xScaleVisibleDimension
                .domain(this.visibleDimensions)
                .range([0, this.width - ParallelPlot.margin.left - ParallelPlot.margin.right])
                .padding(1);
        }

        on(
            typenames: string,
            // eslint-disable-next-line @typescript-eslint/no-explicit-any
            callback: (_this: object, ..._args: any[]) => void
        ) {
            this.dispatch.on(typenames, callback);
        }

        private initSampleData(config: PPConfig) {
            this.sampleData = [];

            config.data.forEach(r => {
                const curRow: Row = {};
                config.data.columns.forEach((dim, i) => {
                    const categories = Array.isArray(config.categorical)
                        ? config.categorical[i]
                        : null;
                    const cellValue = r[dim];
                    if (typeof cellValue === "undefined") {
                        curRow[dim] = NaN;
                    }
                    else if (categories) {
                        let catIndex = categories.indexOf(cellValue.toString());
                        if (catIndex === -1) {
                            catIndex = categories.indexOf(+cellValue);
                        }
                        curRow[dim] = (catIndex === -1) ? NaN : catIndex;
                    }
                    else {
                        curRow[dim] = +cellValue;
                    }
                });
                this.sampleData.push(curRow);
            });
        }

        public updateVisibleDimensions() {
            // Dimensions with cutoffs are Always Visible
            const withCutoffs: string[] = this.dimensions.filter(
                dim =>
                    this.columns[dim].hasFilters() ||
                    dim === this.refColumnDim
            );

            // Care about slice method which dont take the end item
            const requested = this.dimensions.slice(this.startingDimIndex, this.startingDimIndex + this.visibleDimCount);
            const withCutoffsBefore = withCutoffs.filter(
                dim => this.dimensions.indexOf(dim) < this.startingDimIndex
            );
            const withCutoffsAfter = withCutoffs.filter(
                dim => this.dimensions.indexOf(dim) >= this.startingDimIndex + this.visibleDimCount
            );
            const requestedPlusWithCutoffs = withCutoffsBefore
                .concat(requested)
                .concat(withCutoffsAfter);

            // Have to remove elements in order to have the good lenght (but not elements having cutoffs)
            const withoutCutoffsRequested = requested.filter(
                dim => !withCutoffs.includes(dim)
            );

            const requestedToRemoveBefore = withoutCutoffsRequested.splice(
                0,
                withCutoffsBefore.length
            );
            const requestedToRemoveAfter = withoutCutoffsRequested.splice(
                -withCutoffsAfter.length,
                withCutoffsAfter.length
            );

            this.visibleDimensions = requestedPlusWithCutoffs.filter(
                dim =>
                    !requestedToRemoveBefore.includes(dim) &&
                    !requestedToRemoveAfter.includes(dim)
            );
            this.updateXScale();
        }

        xCatOffset(dim: string) {
            return this.columns[dim].categorical !== null && this.arrangeMethod === ParallelPlot.ARRANGE_FROM_BOTH
            ? ParallelPlot.catClusterWidth / 2
            : 0;
        }

        yRefRowOffset(dim: string) {
            return this.refRowIndex === null
                ? 0
                : this.axeHeight - this.yTraceValue(dim, this.refRowIndex);
        }

        yTraceValue(dim: string, rowIndex: number): number {
            return this.columns[dim].yTraceValue(rowIndex);
        }

        rowColor(row: Row) {
            if (this.refColumnDim === null) {
                return "#03306B";
            }
            else {
                if (this.colorScale === null) {
                    console.error("Cant't retrieve a color for a row (no color scale defined)");
                    return "#03306B";
                }
                return this.colorScale(row[this.refColumnDim]);
            }
        }

        valueColor(value: number) {
            if (this.colorScale === null) {
                console.error("Cant't retrieve a color for a value (no color scale defined)");
                return "#03306B";
            }
            return this.colorScale(value);
        }

        changeColorMapOnDimension(d: string) {
            this.refColumnDim = (d === this.refColumnDim) ? null : d;
            this.sendRefColumnDimEvent();
            this.updateZAxisGui();
            this.applyColorMap();
        }

        setArrangeMethod(arrangeMethod: string) {
            if (ParallelPlot.ARRANGE_METHODS.includes(arrangeMethod)) {
                this.arrangeMethod = arrangeMethod;
                this.refreshTracesPaths();
            }
            else {
                console.error("Unknown arrange method: " + arrangeMethod);
            }
        }

        setCategoriesRep(categoriesRep: string) {
            if (ParallelPlot.CATEGORIES_REP_LIST.includes(categoriesRep)) {
                this.catEquallySpacedLines = categoriesRep === ParallelPlot.CATEGORIES_REP_LIST[0];
                this.refreshTracesPaths();
                this.refreshCategoriesBoxes();
                this.updateColumnLabels();
            }
            else {
                console.error("Unknown categories representation: " + categoriesRep);
            }
        }

        setRefColumnDim(dim: string | null) {
            if (dim === null || this.dimensions.includes(dim)) {
                if (this.refColumnDim !== dim) {
                    this.refColumnDim = dim;
                    this.updateZAxisGui();
                    this.applyColorMap();
                }
            }
            else {
                console.error("Unknown ref comumn dim: " + dim);
            }
        }

        setContinuousColorScale(continuousCsId: string) {
            if (ParallelPlot.CONTINUOUS_CS_IDS.includes(continuousCsId)) {
                this.continuousCsId = continuousCsId;
                this.applyColorMap();
            }
            else {
                console.error("Unknown continuous color scale: " + continuousCsId);
            }
        }

        setCategoricalColorScale(categoricalCsId: string) {
            if (ParallelPlot.CATEGORIAL_CS_IDS.includes(categoricalCsId)) {
                this.categoricalCsId = categoricalCsId;
                this.applyColorMap();
            }
            else {
                console.error("Unknown categorical color scale: " + categoricalCsId);
            }
        }

        setHistoVisibility(histoVisibility: boolean[] | Record<string, boolean>) {
            Object.keys(this.sampleData[0]).forEach((dim: string, i: number) => {
                if (Array.isArray(histoVisibility)) {
                    this.columns[dim].histoVisible = histoVisibility[i];
                }
                else {
                    if (typeof histoVisibility[dim] === "undefined") {
                        return;
                    }
                    this.columns[dim].histoVisible = histoVisibility[dim];
                }
                this.drawMainHistograms();
                this.drawSelectedHistograms();
            });
        }

        setInvertedAxes(invertedAxes: boolean[] | Record<string, boolean>) {
            Object.keys(this.sampleData[0]).forEach((dim: string, i: number) => {
                if (Array.isArray(invertedAxes)) {
                    if (this.columns[dim].setInvertedAxe(invertedAxes[i]) && this.columnHeaders) {
                        this.columnHeaders.reverseDomainOnAxis(dim);
                    }
                }
                else {
                    if (typeof invertedAxes[dim] === "undefined") {
                        return;
                    }
                    if (this.columns[dim].setInvertedAxe(invertedAxes[dim]) && this.columnHeaders) {
                        this.columnHeaders.reverseDomainOnAxis(dim);
                    }
                }
            });

            this.refreshTracesPaths();
        }

        updateColumnLabels() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const parallelPlot = this;
            const dimensionGroup = d3.select(parallelPlot.bindto + " .plotGroup").selectAll<SVGGElement, string>(".dimension");
            const axisLabel = dimensionGroup.select(".axisLabel");
            axisLabel.each(function(dim: string) {
                const self = d3.select(this);
                const invertIndicator = 
                    parallelPlot.columns[dim].continuous && parallelPlot.columns[dim].continuous?.invertedAxe 
                    ? "↓ " 
                    : "";
                const hiddenCatCount = 
                    parallelPlot.catEquallySpacedLines && parallelPlot.columns[dim].categorical
                    ? parallelPlot.columns[dim].categorical?.catWithoutTraces().length
                    : 0;
                const hiddenCatIndicator = 
                    hiddenCatCount === 0
                    ? ""
                    : ` (-${hiddenCatCount})`;
                const labels = (invertIndicator + parallelPlot.columns[dim].label + hiddenCatIndicator).split("<br>");
                self.text(labels[0]);
                for (let i = 1; i < labels.length; i++) {
                    self.append("tspan")
                        .attr("x", 0)
                        .attr("dy","1em")
                        .text(labels[i]);
                }

                if (parallelPlot.rotateTitle) {
                    self.attr("y", 0)
                        .attr("transform", "rotate(-10) translate(-20,"+ -15 * labels.length + ")");
                }
                else {
                    self.style("text-anchor", "middle")
                        .attr("y", -15 * labels.length)
                }
            });
        }

        public refreshCategoriesBoxes() {
            Object.values(this.columns).forEach(column => {
                if (column.categorical) {
                    column.categorical.refreshBoxesRep();
                }
            });
        }

        public unvalidateColumnInit() {
            Object.values(this.columns).forEach(column => {
                if (column.categorical) {
                    column.categorical.unvalidateInit();
                }
            });
        }

        public refreshTracesPaths() {
            this.unvalidateColumnInit();
            this.updateTracesPaths();
            this.updateEditedTrace();
        }

        private updateTracesPaths() {
            this.unvalidateColumnInit();

            d3.select(this.bindto + " .foreground")
                .selectAll("path")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("d", (_d, i) => {
                    return this.path(i);
                });

            // Move BackGround Path
            d3.select(this.bindto + " .background")
                .selectAll("path")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("d", (_d, i) => {
                    return this.path(i);
                });
        }

        private updateEditedTrace() {
            if (this.editedRowIndex === null) {
                return;
            }

            d3.select(this.bindto + " .subEditedTrace")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("d", this.path(this.editedRowIndex) as string);
            d3.select(this.bindto + " .editedTrace")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("d", this.path(this.editedRowIndex) as string);

            d3.selectAll<SVGGElement, string>(this.bindto + " .gEditionCircle")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("transform", dim => {
                    const x = this.xScaleVisibleDimension(dim) as number;
                    const xCatOffset = this.xCatOffset(dim);
                    const yRefRowOffset = this.yRefRowOffset(dim);
                    return `translate(${x + xCatOffset}, ${yRefRowOffset})`;
                })

            d3.select(this.bindto + " .editionCircles")
                .selectAll<SVGCircleElement, string>("circle")
                .transition()
                .ease(d3.easeBackOut)
                .duration(ParallelPlot.D3_TRANSITION_DURATION)
                .attr("cy", dim => {
                    if (this.editedRowIndex === null) {
                        return 0;
                    }
                    return this.yTraceValue(dim, this.editedRowIndex) + this.yRefRowOffset(dim);
                })
        }

        setCutoffs(cutoffs: UnnamedCutoffs | NamedCutoffs | null) {
            Object.keys(this.sampleData[0]).forEach((dim: string, i: number) => {
                if (cutoffs === null) {
                    this.columns[dim].setCutoffs(null);
                }
                else if (Array.isArray(cutoffs)) {
                    this.columns[dim].setCutoffs(cutoffs[i]);
                }
                else {
                    if (typeof cutoffs[dim] === "undefined") {
                        return;
                    }
                    this.columns[dim].setCutoffs(cutoffs[dim]);
                }
                if (this.columns[dim].continuous) {
                    this.columns[dim].continuous!.initMultiBrush();
                }
                if (this.columns[dim].categorical) {
                    this.columns[dim].categorical!.applyCategoricalCutoffs();
                }
            });
            this.updateSelectedRows();
            this.drawSelectedTraces();
            this.drawSelectedHistograms();
        }
        
        private adjustVisibleDimensions() {
            if (this.dimensions.length < this.defaultVisibleDimCount) {
                this.visibleDimCount = this.dimensions.length;
            }
            else {
                this.visibleDimCount = this.defaultVisibleDimCount;
            }
            this.startingDimIndex = 0;
            this.updateVisibleDimensions();
        }
    
        setKeptColumns(keptColumns: boolean[] | Record<string, boolean>) {
            if (Array.isArray(keptColumns)) {
                this.dimensions = Object.keys(this.sampleData[0]).filter(
                    (_dim, i) => keptColumns[i]
                );
            }
            else {
                this.dimensions = Object.keys(this.sampleData[0]).filter((dim: string) => {
                    let toKeep = this.dimensions.includes(dim);
                    if (typeof keptColumns[dim] !== "undefined") {
                        toKeep = keptColumns[dim];
                    }
                    return toKeep;
                });
            }
            this.adjustVisibleDimensions();
            d3.select(this.bindto + " .slider .axisGroup").remove();
            d3.select(this.bindto + " .slider .brushDim").remove();
            new BrushSlider(this);
            this.buildPlotArea();
            this.style.applyCssRules();
        }

        getValue(attrType: string) {
            if (attrType === ParallelPlot.CO_ATTR_TYPE) {
                const coMap = new Map<string, ContCutoff[] | CatCutoff[]>();
                Object.keys(this.sampleData[0])
                    .forEach(dim => {
                        const cutoffs = this.columns[dim].getCutoffs();
                        if (cutoffs !== null) {
                            coMap.set(dim, cutoffs);
                        }
                    });
                const cutoffs: Record<string, ContCutoff[] | CatCutoff[]> = {};
                for(const [dim, co] of coMap) {
                    cutoffs[dim] = co;
                }
                return cutoffs;
            }

            if (attrType === ParallelPlot.ST_ATTR_TYPE) {
                return [...this.selectedRows];
            }
            
            if (attrType === ParallelPlot.RC_ATTR_TYPE) {
                return this.refColumnDim;
            }
            
            throw new Error("'getValue' called with an unknown attrType: " + attrType);
        }

        setColorScale() {
            if (this.refColumnDim !== null) {
                if (this.columns[this.refColumnDim].continuous) {
                    const [yRefMin, yRefMax] = this.columns[this.refColumnDim].continuous!.y().domain();
                    this.colorScale = d3.scaleSequential(ParallelPlot.CONTINUOUS_CS[this.continuousCsId])
                        .domain([yRefMin, yRefMax]);
                }
                if (this.columns[this.refColumnDim].categorical) {
                    const yRefMax = this.columns[this.refColumnDim].categorical!.categories.length;
                    this.colorScale = ParallelPlot.CATEGORIAL_CS[this.categoricalCsId]
                        .domain(d3.range(yRefMax));
                }
            }
        }

        // eslint-disable-next-line max-lines-per-function
        private applyColorMap() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPlot = this;

            this.setColorScale();

            d3.select(this.bindto + " .foreground")
                .selectAll<SVGPathElement, Row>("path")
                .transition()
                .duration(this.changeColorDuration)
                .attr("stroke", row => {
                    return this.rowColor(row);
                });

            d3.select(this.bindto + " .background")
                .selectAll<SVGPathElement, Row>("path")
                .transition()
                .duration(this.changeColorDuration)
                .attr("stroke", row => {
                    return this.rowColor(row);
                });

            d3.select(this.bindto + " .plotGroup")
                .selectAll<SVGGElement, string>(".dimension")
                .each(function(dim) {
                    if (dim === thisPlot.refColumnDim) {
                        d3.select(this).selectAll<SVGRectElement, string | number>(".category rect")
                            .transition()
                            .duration(thisPlot.changeColorDuration)
                            .attr("fill", function(cat) {
                                if (thisPlot.columns[dim].categorical) {
                                    const catIndex = thisPlot.columns[dim].categorical!.categories.indexOf(cat) as number;
                                    return thisPlot.valueColor(catIndex);
                                }
                                throw new Error("'applyColorMap' failed for dim:" + dim);
                            });
                    }
                    else {
                        d3.select(this).selectAll(".category rect")
                            .transition()
                            .duration(thisPlot.changeColorDuration)
                            .attr("fill", "black");
                    }
                });

            if (this.editedRowIndex !== null) {
                const stroke = this.selectedRows.has(this.editedRowIndex) ? this.rowColor(this.sampleData[this.editedRowIndex]) : "#FFFFFF";
                const greyStroke = ParallelPlot.greyStroke(stroke);
                d3.select(this.bindto + " .subEditedTrace")
                    .transition()
                    .duration(this.changeColorDuration)
                    .attr("stroke", greyStroke);
                d3.select(this.bindto + " .editedTrace")
                    .transition()
                    .duration(this.changeColorDuration)
                    .attr("stroke", stroke);
            }

            this.drawContinuousCS();
        }

        public drawContinuousCS() {
            d3.select(this.bindto + " .plotGroup").selectAll(".colorScaleBar").remove();

            d3.selectAll<SVGGElement, string>(this.bindto + " .plotGroup .dimension")
                .each(dim => {
                    if (this.columns[dim].continuous && this.refColumnDim === this.columns[dim].dim) {
                        this.columns[dim].continuous!.drawColorScale();
                    }
                });
        }

        public drawContinuousAxes() {
            d3.select(this.bindto + " .plotGroup")
                .selectAll<SVGGElement, string>(".dimension").append("g")
                .attr("class", "axisGroup")
                .each(dim => {
                    if (this.columns[dim].continuous) {
                        this.columns[dim].continuous!.drawAxe();
                    }
                });
        }

        public drawCategoricals() {
            d3.select(this.bindto + " .plotGroup")
                .selectAll<SVGGElement, string>(".dimension")
                .filter(dim => this.columns[dim].categorical !== null)
                .append("g")
                .attr("class", "catGroup")
                .each(dim => {
                    this.columns[dim].categorical!.drawCategories();
                });
        }

        private updateSelectedRows() {
            this.selectedRows.clear();
            this.sampleData.forEach((row, i) => {
                const isKept = this.dimensions.every(dim => {
                    return this.columns[dim].isKept(row[dim]);
                });

                if (isKept) {
                    this.selectedRows.add(i);
                }
            });
        }

        public buildPlotArea() {
            this.drawBackGroundPath();
            this.drawForeGroundPath();
            this.drawEditedTrace();

            d3.select(this.bindto + " .plotGroup").selectAll(".dimension").remove();

            // Add a group element for each dimension.
            const dimensionGroup = d3.select(this.bindto + " .columns")
                .selectAll(".dimension")
                .data(this.visibleDimensions)
                .enter().append("g")
                .classed("dimension", true)
                .classed("input", dim => this.columns[dim].isInput())
                .classed("output", dim => this.columns[dim].isOutput())
                .attr("transform", dim => {
                        const x = this.xScaleVisibleDimension(dim);
                        const yRefRowOffset = this.yRefRowOffset(dim);
                        return `translate(${x}, ${yRefRowOffset})`;
                    });

            this.drawContinuousAxes();
            this.drawCategoricals();

            this.drawMainHistograms();
            this.drawSelectedHistograms();

            // Add and store a multibrush for each continious axis.
            dimensionGroup.append("g")
                .filter(dim => this.columns[dim].continuous !== null)
                .attr("class", dim => MultiBrush.multiBrushClass(this.columns[dim].colIndex))
                .each(dim => {
                    this.columns[dim].continuous!.initMultiBrush();
                });

            this.columnHeaders = new ColumnHeaders(this);
            this.drawContinuousCS();
        }

        private drawBackGroundPath() {
            d3.select(this.bindto + " .background").selectAll("path").remove();
        
            d3.select(this.bindto + " .background").selectAll("path")
                .data(this.sampleData)
                .enter().append("path")
                .attr("d", (_row, i) => this.path(i))
                .attr("stroke", (row) => this.rowColor(row))
                .style("display", (_row, i) => this.selectedRows.has(i) ? "none" : null);
        }
        
        private drawForeGroundPath() {
            // eslint-disable-next-line @typescript-eslint/no-this-alias
            const thisPlot = this;

            d3.select(this.bindto + " .foreground").selectAll("path").remove();
        
            d3.select(this.bindto + " .foreground")
                .selectAll<SVGPathElement, Row>("path")
                .data<Row>(this.sampleData)
                .enter().append("path")
                .attr("d", (_row, i) => this.path(i))
                .attr("stroke", (row) => this.rowColor(row))
                .attr("stroke-width", 1)
                .style("display", (_row, i) => this.selectedRows.has(i) ? null : "none")
                .on("mouseover", (_event, row) => {
                    const clickedRowIndex = thisPlot.sampleData.indexOf(row);
                    this.drawHighlightedTrace(clickedRowIndex);
                    this.showTraceTooltip(row, clickedRowIndex, this.traceTooltipLocation());
                    this.sendHlPointEvent(clickedRowIndex);
                })
                .on("mouseout", () => {
                    this.drawHighlightedTrace(null);
                    d3.select(this.bindto + " .ppTooltip").style("display", "none");
                    this.sendHlPointEvent(null);
                })
                .on("click", function(_event, row) {
                    const clickedRowIndex = thisPlot.sampleData.indexOf(row);
                    if (thisPlot.editionMode !== ParallelPlot.EDITION_OFF) {
                        thisPlot.editedRowIndex =
                            thisPlot.editedRowIndex === clickedRowIndex
                                ? null
                                : clickedRowIndex;
                        thisPlot.drawEditedTrace();
                    }
                    thisPlot.sendClickEvent(clickedRowIndex);
                });
        }

        path(rowIndex: number) {
            // Each visible categorical needs an additional node (to allow 'both' arrange method)
            let nodeCount = this.visibleDimensions.length;
            this.visibleDimensions.forEach(dim => {
                if (this.columns[dim].categorical !== null) {
                    nodeCount = nodeCount + 1;
                }
            });

            const lineData = new Array(nodeCount);
            let nodeIndex = 0;
            this.visibleDimensions.forEach(dim => {
                const x = this.xScaleVisibleDimension(dim) as number;
                const xCatOffset = this.xCatOffset(dim);
                const y = this.yTraceValue(dim, rowIndex);
                const yRefRowOffset = this.yRefRowOffset(dim);
                    
                lineData[nodeIndex] = [x - xCatOffset, y + yRefRowOffset];
                nodeIndex = nodeIndex + 1;

                if (this.columns[dim].categorical !== null) {
                    const yOut = this.arrangeMethod === ParallelPlot.ARRANGE_FROM_BOTH 
                        ? this.columns[dim].categorical!.yTraceValueOut(rowIndex)
                        : y;
                    lineData[nodeIndex] = [x + xCatOffset, yOut + yRefRowOffset];
                    nodeIndex = nodeIndex + 1;
                }
            });

            return ParallelPlot.line(lineData);
        }

        editedPath(editedDim: string, position: number) {
            return ParallelPlot.line(this.visibleDimensions.map<Point>((dim) => {
                const x = this.xScaleVisibleDimension(dim) as number;
                const xCatOffset = this.xCatOffset(dim);
                const y = (dim === editedDim || this.editedRowIndex === null)
                    ? position
                    : this.yTraceValue(dim, this.editedRowIndex);
                const yRefRowOffset = this.yRefRowOffset(dim);
                return [x + xCatOffset, y + yRefRowOffset];
            })
            );
        }

        public drawHighlightedTrace(rowIndex: number | null) {
            if (this.hlPointIndex !== rowIndex) {
                this.hlPointIndex = rowIndex;
                if (rowIndex === null || this.editedPointDim !== null) {
                    d3.select(this.bindto + " .subHlTrace")
                        .style("display", "none");
                    d3.select(this.bindto + " .hlTrace")
                        .style("display", "none");
                }
                else {
                    const row = this.sampleData[rowIndex];
                    const stroke = this.rowColor(row);
                    const greyStroke = ParallelPlot.greyStroke(stroke);
                    d3.select(this.bindto + " .subHlTrace")
                        .attr("d", this.path(rowIndex) as string)
                        .attr("stroke", greyStroke)
                        .style("display", null);
                    d3.select(this.bindto + " .hlTrace")
                        .attr("d", this.path(rowIndex) as string)
                        .attr("stroke", stroke)
                        .style("display", null);
                }
                
                d3.select(this.bindto + " .foreground")
                    .attr("opacity", rowIndex === null && this.editedRowIndex === null ? 0.8 : 0.6);
            }
        }

        public initTraceTooltip() {
            const rowIndex = 0;
            const coords = this.traceTooltipLocation();

            const tooltipTitle = this.rowLabels
                ? this.rowLabels[rowIndex]
                : "Point " + (rowIndex + 1);
            d3.select(this.bindto + " .ppTooltip").remove();
            const ppDiv = d3.select(this.bindto + " .ppDiv")
            const tooltip = ppDiv.append("div")
                .attr("class", "ppTooltip")
                .style("display", "none")
                .style("left", coords[0] + "px")
                .style("top", coords[1] + "px");
            tooltip.append("div")
                .attr("class", "pointIndex title")
                .text(tooltipTitle);

            tooltip.append("div").selectAll("xNameDiv").data(this.visibleDimensions)
                .enter()
                .append("div")
                .attr("class", "xNameDiv")
                .html(dim => {
                    const column = this.columns[dim];
                    return `<span class="xName">${column.labelText()}</span>: <span class="xValue">${column.formatedRowValue(this.sampleData[rowIndex])}</span>`;
                });
        }

        public showTraceTooltip(row: Row, rowIndex: number, coords: [number, number]) {
            const tooltipTitle = this.rowLabels
                ? this.rowLabels[rowIndex]
                : "Point " + (rowIndex + 1);
            const tooltip = d3.select(this.bindto + " .ppTooltip")
                .style("display", "block")
                .style("left", coords[0] + "px")
                .style("top", coords[1] + "px");
            tooltip.select(".pointIndex.title")
                .text(tooltipTitle);

            const xNameDiv = tooltip.selectAll<HTMLDivElement, string>(".xNameDiv")
            xNameDiv.select<HTMLDivElement>(".xName")
                .text(dim => {
                    return this.columns[dim].labelText();
                });
            xNameDiv.select<HTMLDivElement>(".xValue")
                .text(dim => {
                    return this.columns[dim].formatedRowValue(row);
                });
        }

        private traceTooltipLocation() {
            const ppDivNode = d3.select<HTMLElement, unknown>(this.bindto + " .ppDiv").node();
            const parentBounds = (ppDivNode === null) ? null : ppDivNode.getBoundingClientRect();
            const xParent = (parentBounds === null) ? 0 : parentBounds.x;
            const yParent = (parentBounds === null) ? 0 : parentBounds.y;

            const plotGroup = d3.select<HTMLElement, unknown>(this.bindto + " .plotGroup").node();
            const elementBounds = (plotGroup === null) ? null : plotGroup.getBoundingClientRect();
            const xRect = (elementBounds === null) ? 0 : elementBounds.x;
            const yRect = (elementBounds === null) ? 0 : elementBounds.y;
            const wRect = (elementBounds === null) ? 0 : elementBounds.width;

            return [xRect - xParent + wRect + 5, yRect - yParent + 20] as [number, number];
        }

        private static greyStroke(stroke: string) {
            const strokeColor = d3.color(stroke);
            let greyStroke = d3.rgb(0, 0, 0);
            if (strokeColor) {
                const rgb = strokeColor.rgb();
                const greyComp = Math.round(((rgb.r ^ 255) + (rgb.g ^ 255) + (rgb.b ^ 255)) / 3);
                greyStroke = d3.rgb(greyComp, greyComp, greyComp);
            }
            return greyStroke.hex();
        }

        // eslint-disable-next-line max-lines-per-function
        private drawEditedTrace() {
            const editedRowIndex = this.editedRowIndex;
            if (editedRowIndex === null) {
                d3.select(this.bindto + " .subEditedTrace")
                    .style("display", "none");
                d3.select(this.bindto + " .editedTrace")
                    .style("display", "none");
            }
            else {
                const row = this.sampleData[editedRowIndex];
                const stroke = this.selectedRows.has(editedRowIndex) ? this.rowColor(row) : "#FFFFFF";
                const greyStroke = ParallelPlot.greyStroke(stroke);
                d3.select(this.bindto + " .subEditedTrace")
                    .attr("d", this.path(editedRowIndex) as string)
                    .attr("stroke", greyStroke)
                    .style("display", null);
                d3.select(this.bindto + " .editedTrace")
                    .attr("d", this.path(editedRowIndex) as string)
                    .attr("stroke", stroke)
                    .style("display", null);
            }

            d3.select(this.bindto + " .foreground")
                .attr("opacity", this.editedRowIndex === null ? 0.8 : 0.6);

            const visibleInputDims = this.visibleDimensions.filter(dim => this.columns[dim].isInput());
            d3.select(this.bindto + " .editionCircles")
                .style("display", () => this.editedRowIndex === null ? "none" : null)
                .selectAll<SVGGElement, string>(".gEditionCircle")
                .data(visibleInputDims)
                .join(
                    enter => {
                        const gEditionCircle = enter.append("g")
                            .attr("class", "gEditionCircle");
                        gEditionCircle.append("circle")
                            .attr("r", 4)
                            .call(d3.drag<SVGCircleElement, string>()
                                .on("start", (_event, dim) => {
                                    this.editedPointDim = dim;
                                    this.drawEditedTrace();
                                })
                                .on("drag", (event, dim) => {
                                    this.pointDrag(event.y, dim);
                                })
                                .on("end", (event, dim) => {
                                    this.pointDragEnd(event.y, dim);
                                })
                            )
                        return gEditionCircle;
                    },
                    update => update,
                    exit => exit.remove()
                )
                .attr("transform", (dim) => {
                    const x = this.xScaleVisibleDimension(dim) as number;
                    const xCatOffset = this.xCatOffset(dim);
                    const yRefRowOffset = this.yRefRowOffset(dim);
                    return `translate(${x + xCatOffset}, ${yRefRowOffset})`;
                })
                .select<SVGCircleElement>("circle")
                .attr("cx", 0)
                .filter(dim => dim !== this.editedPointDim)
                .attr("cy", (dim) => {
                    if (this.editedRowIndex === null) {
                        return 0;
                    }
                    return this.yTraceValue(dim, this.editedRowIndex);
                });
        }

        pointDrag(y: number, draggedDim: string) {
            if (this.editionMode === ParallelPlot.EDITION_ON_DRAG_END) {
                this.dragEditionPoint(draggedDim, y);
            }
            else if (this.editionMode === ParallelPlot.EDITION_ON_DRAG) {
                this.dragEditionPoint(draggedDim, y);
                this.askForPointEdition(draggedDim, y);
            }
        }

        pointDragEnd(y: number, draggedDim: string) {
            if (this.editionMode !== ParallelPlot.EDITION_OFF) {
                this.askForPointEdition(draggedDim, y);
                this.editedPointDim = null;
            }

        }

        private dragEditionPoint(draggedDim: string, position: number) {
            if (this.editedRowIndex === null) {
                console.error("dragEditionPoint is called but editedRowIndex is null")
                return;
            }

            d3.select(this.bindto + " .editionCircles")
                .selectAll<SVGCircleElement, string>("circle")
                .filter(dim => dim === draggedDim)
                .attr("cy", () => position);

            d3.select(this.bindto + " .foreground")
                .selectAll("path")
                .filter((_row, i) => i === this.editedRowIndex)
                .attr("d", this.editedPath(draggedDim, position) as string);

            d3.select(this.bindto + " .subEditedTrace")
                .attr("d", this.editedPath(draggedDim, position) as string);
            d3.select(this.bindto + " .editedTrace")
                .attr("d", this.editedPath(draggedDim, position) as string);
        }

        private askForPointEdition(draggedDim: string, y: number) {
            if (this.editedRowIndex === null) {
                console.error("dragEditedTrace is called but editedRowIndex is null")
                return;
            }

            if (this.columns[draggedDim].categorical) {
                const newValue = this.columns[draggedDim].categorical!.invertYValue(y);
                const categories = this.columns[draggedDim].categorical!.categories;
                const catIndex = Math.round(newValue);
                if (catIndex >= 0 && catIndex < categories.length) {
                    this.sendPointEditionEvent(draggedDim, this.editedRowIndex, categories[catIndex]);
                }
            }
            if (this.columns[draggedDim].continuous) {
                const newValue = this.columns[draggedDim].continuous!.y().invert(y);
                this.sendPointEditionEvent(draggedDim, this.editedRowIndex, newValue);
            }
        }

        private drawSelectedTraces() {
            d3.select(this.bindto + " .foreground")
                .selectAll("path")
                .style("display", (_d, i) => this.selectedRows.has(i) ? null : "none");

            d3.select(this.bindto + " .background")
                .selectAll("path")
                .style("display", (_d, i) => this.selectedRows.has(i) ? "none" : null);

            d3.select(this.bindto + " .plotGroup").selectAll(".histogram")
                .style("display", () => {
                    return this.selectedRows.size > this.sampleData.length / 10.0
                        ? null
                        : "none";
                });
        }

        setContCutoff(brushSelections: BrushSelection[], dim: string, adjusting: boolean) {
            if (this.columns[dim].continuous) {
                const contCutoffs = brushSelections
                    .map(interval => interval.map(this.columns[dim].continuous!.y().invert))
                    .map(interval => {
                        return interval.sort(function(a, b) {
                            return b - a;
                        });
                    }) as ContCutoff[];

                if (contCutoffs === null || contCutoffs.length === 0) {
                    this.columns[dim].continuous!.contCutoffs = null;
                } else {
                    this.columns[dim].continuous!.contCutoffs = contCutoffs;
                }
            }
            else {
                throw new Error("'setContCutoff' failed for:" + dim);
            }

            this.applyColumnCutoffs(dim, adjusting);
        }

        applyColumnCutoffs(updatedDim: string, adjusting: boolean) {
            this.updateSelectedRows();
            this.drawSelectedTraces();
            this.drawSelectedHistograms();
            
            this.sendCutoffEvent(updatedDim, adjusting);
        }

        sendCutoffEvent(updatedDim: string, adjusting: boolean) {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, { 
                    type: ParallelPlot.CUTOFF_EVENT,
                    value: { updatedDim: updatedDim, adjusting: adjusting, cutoffs: this.getValue(ParallelPlot.CO_ATTR_TYPE), selectedTraces: this.getValue(ParallelPlot.ST_ATTR_TYPE) }
                }
            );
        }

        sendClickEvent(rowIndex: number) {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, { 
                    type: ParallelPlot.ROW_CLICK_EVENT,
                    value: { rowIndex: rowIndex }
                }
            );
        }

        sendHlPointEvent(rowIndex: number | null) {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, { 
                    type: ParallelPlot.HL_ROW_EVENT,
                    value: { rowIndex: rowIndex }
                }
            );
        }

        sendPointEditionEvent(dim: string, rowIndex: number, newValue: number | string) {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, {
                    type: ParallelPlot.EDITION_EVENT,
                    value: { dim: dim, rowIndex: rowIndex, newValue: newValue }
                }
            );
        }

        sendInvertedAxeEvent(invertedAxes: boolean[] | Record<string, boolean>) {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, {
                    type: ParallelPlot.INVERTED_AXE_EVENT,
                    value: { invertedAxes: invertedAxes }
                }
            );
        }

        sendRefColumnDimEvent() {
            this.dispatch.call(
                ParallelPlot.PLOT_EVENT,
                undefined, {
                    type: ParallelPlot.REF_COLUMN_DIM_EVENT,
                    value: { refColumnDim: this.refColumnDim }
                }
            );
        }

        public highlightRow(rowIndex: number | null) {
            this.drawHighlightedTrace(rowIndex);
        }

        public changeRow(rowIndex: number, newValues: Row) {
            const changedRow: Row = this.sampleData[rowIndex];
            Object.keys(this.sampleData[0]).forEach((dim: string) => {
                this.columns[dim].unvalid();
            });
            Object.keys(newValues).forEach((dim) => {
                if (this.columns[dim].categorical) {
                    const categories = this.columns[dim].categorical!.categories;
                    changedRow[dim] = categories.indexOf(newValues[dim].toString());
                }
                else {
                    changedRow[dim] = +newValues[dim];
                }
            });
            this.sampleData[rowIndex] = changedRow;

            const isKept = this.dimensions.every((dim) => {
                return this.columns[dim].isKept(this.sampleData[rowIndex][dim]);
            });

            if (isKept) {
                this.selectedRows.add(rowIndex);
            }
            else {
                this.selectedRows.delete(rowIndex);
            }

            this.unvalidateColumnInit();
            this.buildPlotArea();
            this.style.applyCssRules();
        }

        public drawMainHistograms() {
            d3.select(this.bindto + " .plotGroup")
                .selectAll<SVGGElement, string>(".dimension")
                .each(dim => this.columns[dim].drawMainHistogram());
        }
        
        public drawSelectedHistograms() {
            const selected = this.sampleData.filter((_row, i) =>
                this.selectedRows.has(i)
            );

            d3.select(this.bindto + " .plotGroup")
                .selectAll<SVGGElement, string>(".dimension")
                .each(dim => this.columns[dim].drawSelectedHistogram(selected));
        }
        
        // eslint-disable-next-line max-lines-per-function
        getPlotConfig(): PPConfig {
            const allDimensions = Object.keys(this.sampleData[0] as Row);

            const categorical = allDimensions.map(dim => {
                if (this.columns[dim]) {
                    return this.columns[dim].categorical
                        ? this.columns[dim].categorical!.categories
                        : null;
                }
                return null;
            });
            const inputColumns = allDimensions.map(dim => this.columns[dim] && this.columns[dim].ioType === Column.INPUT);

            const keptColumns = allDimensions.map(dim => this.dimensions.includes(dim));

            const histoVisibility = allDimensions.map(dim => this.columns[dim]
                ? this.columns[dim].histoVisible
                : false
            );

            const invertedAxes = allDimensions.map(dim => {
                if (this.columns[dim]) {
                    return this.columns[dim].continuous
                        ? this.columns[dim].continuous!.invertedAxe
                        : false
                }
                return false;
            });

            const cutoffs = allDimensions.map(dim => this.columns[dim]
                ? this.columns[dim].getCutoffs()
                : null
            );

            const columnLabels = allDimensions.map(dim => this.columns[dim] && this.columns[dim].label
                ? this.columns[dim].label
                : dim
            );

            return {
                data: [] as unknown as d3.DSVRowArray<string>,
                rowLabels: this.rowLabels,
                categorical: categorical,
                categoriesRep: this.catEquallySpacedLines ? ParallelPlot.CATEGORIES_REP_LIST[0] : ParallelPlot.CATEGORIES_REP_LIST[1],
                arrangeMethod: this.arrangeMethod,
                inputColumns: inputColumns,
                keptColumns: keptColumns,
                histoVisibility: histoVisibility,
                invertedAxes: invertedAxes,
                cutoffs: cutoffs,
                refRowIndex: this.refRowIndex,
                refColumnDim: this.refColumnDim,
                rotateTitle: this.rotateTitle,
                columnLabels: columnLabels,
                categoricalCS: this.categoricalCsId,
                continuousCS: this.continuousCsId,
                editionMode: this.continuousCsId,
                controlWidgets: this.useControlWidgets,
                cssRules: this.style.cssRules,
                sliderPosition: {
                    dimCount: this.visibleDimCount,
                    startingDimIndex: this.startingDimIndex
                }
            };
        }
    }
