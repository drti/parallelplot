import * as d3 from "d3";

    export class Style {

        bindto: string;
        
        cssRules: Record<string, string | string[]> | null | undefined;

        constructor(bindto: string) {
            this.bindto = bindto;
        }

        public applyCssRules() {
            if (this.cssRules) {
                for (const [selector, declarations] of Object.entries(this.cssRules)) {
                    const selection = d3.select(this.bindto).selectAll(selector);

                    const applyDeclaration = (declaration: string) => {
                        const splitDeclaration = declaration.split(":");
                        if (splitDeclaration.length === 2) {
                            selection.style(splitDeclaration[0], splitDeclaration[1]);
                        }
                        else {
                            console.error("Invalid CSS declaration:", declaration);
                        }
                    }
                    if (Array.isArray(declarations)) {
                        declarations.forEach(applyDeclaration);
                    }
                    if (typeof declarations === "string") {
                        applyDeclaration(declarations);
                    }
                }
            }
        }

    }